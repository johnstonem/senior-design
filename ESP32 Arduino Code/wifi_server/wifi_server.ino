/*********
 * Mix of https://randomnerdtutorials.com/esp32-access-point-ap-web-server/
 * and https://wokwi.com/projects/320964045035274834
 *********/

/*********** BUTTON INTERUPTS ******/
struct Button {
	const uint8_t PIN;
	uint32_t numberKeyPresses;
	bool pressed;
};

Button button1 = {18, 0, false};
Button button2 = {19, 0, false};

void IRAM_ATTR count1() {
  button1.numberKeyPresses++;
}

void IRAM_ATTR count2() {
  button2.numberKeyPresses++;
}

/************ WIFI MODULE **********/
// Load Wi-Fi library
#include <WiFi.h>
#include <WebServer.h>
#include <WiFiClient.h>

// Load format library
#include <format>

// Replace with your network credentials
const char* ssid     = "Bitch";
const char* password = "12345678";

// Set web server port number to 80
WebServer server(80);

// Variable to store the HTTP request
String header;

// Auxiliar variables to store the current output state
String output26State = "off";
String output27State = "off";

// Assign output variables to GPIO pins
const int output26 = 26;
const int output27 = 27;

int i = 0;

// Index.html base
String html = "";

// Scoreboard base
int score1[7] = {0,0,0,0,0,0,0};
int score2[7] = {0,0,0,0,0,0,0};

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void handleRoot() { 
  String message = "hello from esp32!";
  server.send(200, "text/plain", message); 
}

void handleCount(){
  /*
  
  OLD MESSAGE CODE
  
  message += "<html>";
  message += "<head>";
  message += "<title>Refresh Page Example</title>";
  message += "<meta http-equiv=\"refresh\" content=\"1\">";
  message += "</head>";
  message += "<body>";
  message += "<h1>Refreshing Page</h1>";
  message += "<p>This page will refresh after 1 second.</p>";
  message += "<p>Current count:";
  message += i;
  message += "</p>";
  message += "<p>B1 count:";
  message += button1.numberKeyPresses;
  message += "</p>";
  message += "<p>B2 count:";
  message += button2.numberKeyPresses;
  message += "</p>";
  message += "</body>";
  message += "</html>";
  */

  String message = std::format(
    html, 
    "Tennis Scoreboard Tracking",     // Title
    "<meta http-equiv=\"refresh\" content=\"1\">",     // Refresh content
    "Tennis Scoreboard Tracking",     // Header
    score1[0],     // Court1 Score1
    score2[0],     // Court1 Score2
    score1[1],     // Court2 Score1
    score2[1],     // Court2 Score2
    score1[2],     // Court3 Score1
    score2[2],     // Court3 Score2
    score1[3],     // Court4 Score1
    score2[3],     // Court4 Score2
    score1[4],     // Court5 Score1
    score2[4],     // Court5 Score2
    score1[5],     // Court6 Score1
    score2[5],     // Court6 Score2
    score1[6],     // Court7 Score1
    score2[6],     // Court7 Score2
  )

  server.send(200, "text/html", message); 
}

void turnOff26(){ digitalWrite(output26,0); }
void turnOn26(){ digitalWrite(output26,1); }
void turnOff27(){ digitalWrite(output27,0); }
void turnOn27(){ digitalWrite(output27,1); }


/**** SETUP ******/
void setup() {
  Serial.begin(115200);
  // Initialize the output variables as outputs
  pinMode(output26, OUTPUT);
  pinMode(output27, OUTPUT);
  pinMode(button1.PIN, INPUT_PULLUP);
  pinMode(button2.PIN, INPUT_PULLUP);

  // Set outputs to LOW
  digitalWrite(output26, LOW);
  digitalWrite(output27, LOW);

  // Connect to Wi-Fi network with SSID and password
  Serial.print("Setting AP (Access Point)…");
  // Remove the password parameter, if you want the AP (Access Point) to be open
  // Channel set to 6. Make sure to use the same channel for your mesh and for you other
  // network (STATION_SSID)
  WiFi.softAP(ssid, password, 6);

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);
  
  /** Define the server connections! **/
  // server.on("/", handleRoot);
  server.on("/26/0", turnOff26);
  server.on("/26/1", turnOn26);
  server.on("/27/0", turnOff27);
  server.on("/27/1", turnOn27);
  server.on("/count", handleCount);
  server.onNotFound(handleNotFound);
  
  server.begin();

  attachInterrupt(button1.PIN, count1, FALLING);
  attachInterrupt(button2.PIN, count2, FALLING);

  // Save the current HTML for the web server, unformatted
  html += "
    <!DOCTYPE html>
    <html>
    <head>
        <title>{}</title>
        <style>
            body {
                font-family: 'Courier New', monospace;
            }

            .court {
                border: 2px solid #333;
                padding: 20px;
                margin: 20px;
                float: left;
                width: 300px;
                text-align: center;
                background-color: #f5f5f5;
                box-shadow: 5px 5px 15px rgba(0,0,0,0.2);
            }

            h2 {
                font-size: 24px;
                margin-bottom: 10px;
            }

            button {
                font-size: 18px;
                padding: 10px 20px;
                margin: 10px;
                background-color: #333;
                color: #fff;
                border: none;
                cursor: pointer;
                border-radius: 5px;
            }

            span {
                font-size: 48px;
                display: block;
                margin-bottom: 20px;
            }
        </style>

    <!-- Refresh Content goes here -->
        {}
    </head>
    <body>

    <h1 style=\"text-align: center;\">{}</h1>

    <div id=\"courts\">
        <div class=\"court\" id=\"court1\">
            <h2>Court 1</h2>
            <button onclick=\"incrementScore('court1', 'player1')\">Player 1</button>
            <span id=\"score1\">{}</span>
            <button onclick=\"incrementScore('court1', 'player2')\">Player 2</button>
            <span id=\"score2\">{}</span>
        </div>
      
        <div class=\"court\" id=\"court2\">
            <h2>Court 2</h2>
            <button onclick=\"incrementScore('court1', 'player3')\">Player 1</button>
            <span id=\"score3\">{}</span>
            <button onclick=\"incrementScore('court1', 'player4')\">Player 2</button>
            <span id=\"score4\">{}</span>
        </div>
        
        <div class=\"court\" id=\"court3\">
            <h2>Court 3</h2>
            <button onclick=\"incrementScore('court1', 'player5')\">Player 1</button>
            <span id=\"score5\">{}</span>
            <button onclick=\"incrementScore('court1', 'player6')\">Player 2</button>
            <span id=\"score6\">{}</span>
        </div>
          
        <div class=\"court\" id=\"court4\">
            <h2>Court 4</h2>
            <button onclick=\"incrementScore('court1', 'player7')\">Player 1</button>
            <span id=\"score7\">{}</span>
            <button onclick=\"incrementScore('court1', 'player8')\">Player 2</button>
            <span id=\"score8\">{}</span>
        </div>
            
        <div class=\"court\" id=\"court5\">
            <h2>Court 5</h2>
            <button onclick=\"incrementScore('court1', 'player9')\">Player 1</button>
            <span id=\"score9\">{}</span>
            <button onclick=\"incrementScore('court1', 'playerA')\">Player 2</button>
            <span id=\"scoreA\">{}</span>
        </div>
              
        <div class=\"court\" id=\"court6\">
            <h2>Court 6</h2>
            <button onclick=\"incrementScore('court1', 'playerB')\">Player 1</button>
            <span id=\"scoreB\">{}</span>
            <button onclick=\"incrementScore('court1', 'playerC')\">Player 2</button>
            <span id=\"scoreC\">{}</span>
        </div>
                
        <div class=\"court\" id=\"court7\">
            <h2>Court 7</h2>
            <button onclick=\"incrementScore('court1', 'playerD')\">Player 1</button>
            <span id=\"scoreD\">{}</span>
            <button onclick=\"incrementScore('court1', 'playerE')\">Player 2</button>
            <span id=\"scoreE\">{}</span>
        </div>
    </div>
                
    <button onclick=\"copyToClipboard('Court 1: Player 1 - ' + document.getElementById('score1').innerHTML + ', Player 2 - ' + document.getElementById('score2').innerHTML)\">Copy Scores</button>

    <script>
        function incrementScore(courtId, player) {
            var scoreStr = 'score' + player.slice(-1);
            var scoreElement = document.getElementById(scoreStr);
            var currentScore = parseInt(scoreElement.innerHTML);
            if (isNaN(currentScore)) {
                scoreElement.innerHTML = 0;
            } else {
                scoreElement.innerHTML = currentScore + 1;
            }
        }
      
      function copyToClipboard(text) {
            var textarea = document.createElement(\"textarea\");
            textarea.value = text;
            document.body.appendChild(textarea);
            textarea.select();
            document.execCommand(\"copy\");
            document.body.removeChild(textarea);
            alert(\"Scores copied to clipboard!\");
        }
    </script>
    </body>
    </html>";
}

void loop(){
  server.handleClient();
  delay(2);//allow the cpu to switch to other tasks
  i++;
}