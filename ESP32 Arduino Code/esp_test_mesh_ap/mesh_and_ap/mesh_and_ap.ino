#include "painlessMesh.h"
#include <WiFi.h>

#define   MESH_PREFIX     "whateverYouLike"
#define   MESH_PASSWORD   "somethingSneaky"
#define   MESH_PORT       5555

void ISR_BTN1_PRESSED(void);
void ISR_BTN2_PRESSED(void);

#define BTN1 19 // BTN1 on pin19 with a 3.3k pullup to 5V
#define BTN2 18 // BTN2 on pin18 with a 3.3k pullup to 5V
#define Y_LIGHT 26 // Yellow light on pin 26
#define G_LIGHT 27 // Green light on pin 27

#define BOARD_ID 0

// BTN1 ISR
void ISR_BTN1_PRESSED(void) {
  scoreboard[BOARD_ID].sethome[0]+=1;
  sendMessage();
}

// BTN2 ISR
void ISR_BTN2_PRESSED(void) {
  scoreboard[BOARD_ID].setaway[0]+=1;
}


// Replace with your network credentials
const char* ssid     = "ESP32AP";
const char* password = "12345678";


Scheduler userScheduler; // to control your personal task
painlessMesh  mesh;
// User stub
void sendMessage() ; // Prototype so PlatformIO doesn't complain

void sendMessage() {
  String msg = "";
  msg += BOARD_ID + "&";
  msg += scoreboard[BOARD_ID].sethome + "&";
  msg += scoreboard[BOARD_ID].setaway + "&";
  msg += scoreboard[BOARD_ID].gamehome + "&";
  msg += scoreboard[BOARD_ID].gameaway;
  
  mesh.sendBroadcast(msg);
}

// Needed for painless library
void receivedCallback( uint32_t from, String &msg ) {
  Serial.printf("startHere: Received from %u msg=%s\n", from, msg.c_str());
  String id_str = msg.substring(0,1);
  uint32_t id = id_str[0]-'0'
  
  scoreboard[id].sethome = msg.substring(2,3);
  scoreboard[id].setaway = msg.substring(4,5);
  scoreboard[id].gamehome = msg.substring(6,8);
  scoreboard[id].gameaway = msg.substring(9,11);
}

void newConnectionCallback(uint32_t nodeId) {
    Serial.printf("--> startHere: New Connection, nodeId = %u\n", nodeId);
}

void changedConnectionCallback() {
  Serial.printf("Changed connections\n");
}

void nodeTimeAdjustedCallback(int32_t offset) {
    Serial.printf("Adjusted time %u. Offset = %d\n", mesh.getNodeTime(),offset);
}

// Set web server port number to 80
WiFiServer server(80);

// Variable to store the HTTP request
String header;

// Index.html base
String html = "";

// Score base
struct Scoreboard {
  String sethome;
  String setaway;
  String gamehome;
  String gameaway;
}
Scoreboard scoreboards[7] = {{"0","0","0","0"},{"0","0","0","0"},{"0","0","0","0"},{"0","0","0","0"},{"0","0","0","0"},{"0","0","0","0"},{"0","0","0","0"}};

void setup() {
  Serial.begin(115200);
    mesh.setDebugMsgTypes( ERROR | STARTUP );  // set before init() so that you can see startup messages

  mesh.init( MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT );
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);

  userScheduler.addTask( taskSendMessage );
  taskSendMessage.enable();


  // Connect to Wi-Fi network with SSID and password
  Serial.print("Setting AP (Access Point)…");
  // Remove the password parameter, if you want the AP (Access Point) to be open
  WiFi.softAP(ssid, password);

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);

  // Button setup
  // both buttons inputs
  pinMode(BTN1, INPUT_PULLUP);
  pinMode(BTN2, INPUT_PULLUP);
  // both lights outputs
  pinMode(Y_LIGHT, OUTPUT);
  pinMode(G_LIGHT, OUTPUT);
  // Attach ISRs to respective buttons
  attachInterrupt(digitalPinToInterrupt(BTN1), ISR_BTN1_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN2), ISR_BTN2_PRESSED, FALLING);

  html += "
    <!DOCTYPE html>
    <html>
    <head>
        <title>{}</title>
        <style>
            body {
                font-family: 'Courier New', monospace;
            }

            .court {
                border: 2px solid #333;
                padding: 20px;
                margin: 20px;
                float: left;
                width: 300px;
                text-align: center;
                background-color: #f5f5f5;
                box-shadow: 5px 5px 15px rgba(0,0,0,0.2);
            }

            h2 {
                font-size: 24px;
                margin-bottom: 10px;
            }

            button {
                font-size: 18px;
                padding: 10px 20px;
                margin: 10px;
                background-color: #333;
                color: #fff;
                border: none;
                cursor: pointer;
                border-radius: 5px;
            }

            span {
                font-size: 48px;
                display: block;
                margin-bottom: 20px;
            }
        </style>

    <!-- Refresh Content goes here -->
        {}
    </head>
    <body>

    <h1 style=\"text-align: center;\">{}</h1>

    <div id=\"courts\">
        <div class=\"court\" id=\"court1\">
            <h2>Court 1</h2>
            <button onclick=\"incrementScore('court1', 'player1')\">Player 1</button>
            <span id=\"score1\">{}</span>
            <button onclick=\"incrementScore('court1', 'player2')\">Player 2</button>
            <span id=\"score2\">{}</span>
        </div>
      
        <div class=\"court\" id=\"court2\">
            <h2>Court 2</h2>
            <button onclick=\"incrementScore('court1', 'player3')\">Player 1</button>
            <span id=\"score3\">{}</span>
            <button onclick=\"incrementScore('court1', 'player4')\">Player 2</button>
            <span id=\"score4\">{}</span>
        </div>
        
        <div class=\"court\" id=\"court3\">
            <h2>Court 3</h2>
            <button onclick=\"incrementScore('court1', 'player5')\">Player 1</button>
            <span id=\"score5\">{}</span>
            <button onclick=\"incrementScore('court1', 'player6')\">Player 2</button>
            <span id=\"score6\">{}</span>
        </div>
          
        <div class=\"court\" id=\"court4\">
            <h2>Court 4</h2>
            <button onclick=\"incrementScore('court1', 'player7')\">Player 1</button>
            <span id=\"score7\">{}</span>
            <button onclick=\"incrementScore('court1', 'player8')\">Player 2</button>
            <span id=\"score8\">{}</span>
        </div>
            
        <div class=\"court\" id=\"court5\">
            <h2>Court 5</h2>
            <button onclick=\"incrementScore('court1', 'player9')\">Player 1</button>
            <span id=\"score9\">{}</span>
            <button onclick=\"incrementScore('court1', 'playerA')\">Player 2</button>
            <span id=\"scoreA\">{}</span>
        </div>
              
        <div class=\"court\" id=\"court6\">
            <h2>Court 6</h2>
            <button onclick=\"incrementScore('court1', 'playerB')\">Player 1</button>
            <span id=\"scoreB\">{}</span>
            <button onclick=\"incrementScore('court1', 'playerC')\">Player 2</button>
            <span id=\"scoreC\">{}</span>
        </div>
                
        <div class=\"court\" id=\"court7\">
            <h2>Court 7</h2>
            <button onclick=\"incrementScore('court1', 'playerD')\">Player 1</button>
            <span id=\"scoreD\">{}</span>
            <button onclick=\"incrementScore('court1', 'playerE')\">Player 2</button>
            <span id=\"scoreE\">{}</span>
        </div>
    </div>
                
    <button onclick=\"copyToClipboard('Court 1: Player 1 - ' + document.getElementById('score1').innerHTML + ', Player 2 - ' + document.getElementById('score2').innerHTML)\">Copy Scores</button>

    <script>
        function incrementScore(courtId, player) {
            var scoreStr = 'score' + player.slice(-1);
            var scoreElement = document.getElementById(scoreStr);
            var currentScore = parseInt(scoreElement.innerHTML);
            if (isNaN(currentScore)) {
                scoreElement.innerHTML = 0;
            } else {
                scoreElement.innerHTML = currentScore + 1;
            }
        }
      
      function copyToClipboard(text) {
            var textarea = document.createElement(\"textarea\");
            textarea.value = text;
            document.body.appendChild(textarea);
            textarea.select();
            document.execCommand(\"copy\");
            document.body.removeChild(textarea);
            alert(\"Scores copied to clipboard!\");
        }
    </script>
    </body>
    </html>";
  
  server.begin();
}

void loop(){
  WiFiClient client = server.available();   // Listen for incoming clients

  if (client) {                             // If a new client connects,
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
 
          if (currentLine.length() == 0) {
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();

            // Display the HTML web page
            String message = std::format(
              html, 
              "Tennis Scoreboard Tracking",     // Title
              "<meta http-equiv=\"refresh\" content=\"1\">",     // Refresh content
              "Tennis Scoreboard Tracking",     // HTML Header
              scoreboard[0].sethome,
              scoreboard[0].setaway,
              scoreboard[1].sethome,
              scoreboard[1].setaway,
              scoreboard[2].sethome,
              scoreboard[2].setaway,
              scoreboard[3].sethome,
              scoreboard[3].setaway,
              scoreboard[4].sethome,
              scoreboard[4].setaway,
              scoreboard[5].sethome,
              scoreboard[5].setaway,
              scoreboard[6].sethome,
              scoreboard[6].setaway              
            );

            client.println(message);
            /*
            OLD DISPLAY CODE

            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            
            client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
            //client.println(".button { background-color: #4CAF50; border: none; color: white; padding: 16px 40px;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".button2 {background-color: #555555;}</style></head>");
            
            // Web Page Heading
            client.println("<body><h1>ESP32 Web Server</h1>");
            

            client.println("</body></html>");
            */

            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}
