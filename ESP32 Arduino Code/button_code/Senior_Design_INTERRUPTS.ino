/*
 * Simple Program to demonstrate using buttons to switch lights on and off with interrupts
 * Created by Tyler Togstad 11/9/23
 */
void ISR_BTN1_PRESSED(void);
void ISR_BTN2_PRESSED(void);

#define BTN1 19 // BTN1 on pin19 with a 3.3k pullup to 5V
#define BTN2 18 // BTN2 on pin18 with a 3.3k pullup to 5V
#define Y_LIGHT 26 // Yellow light on pin 26
#define G_LIGHT 27 // Green light on pin 27

void setup() {
  // both buttons inputs
  pinMode(BTN1, INPUT_PULLUP);
  pinMode(BTN2, INPUT_PULLUP);
  // both lights outputs
  pinMode(Y_LIGHT, OUTPUT);
  pinMode(G_LIGHT, OUTPUT);
  // Attach ISRs to respective buttons
  attachInterrupt(digitalPinToInterrupt(BTN1), ISR_BTN1_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN2), ISR_BTN2_PRESSED, FALLING);
}
// some state vars to swap lights on and off
int ledYState = LOW;
int ledGState = LOW;

void loop() {}

// BTN1 ISR
void ISR_BTN1_PRESSED(void) {
  ledYState = !(ledYState);
  digitalWrite(Y_LIGHT, ledYState);
}

// BTN2 ISR
void ISR_BTN2_PRESSED(void) {
  ledGState = !(ledGState);
  digitalWrite(G_LIGHT, ledGState);
}