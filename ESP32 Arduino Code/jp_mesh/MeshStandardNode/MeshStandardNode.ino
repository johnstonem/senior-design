//************************************************************
// this is a simple example that uses the painlessMesh library
//
// 1. sends a silly message to every node on the mesh at a random time between 1 and 5 seconds
// 2. prints anything it receives to Serial.print
//
//
//************************************************************
#include "painlessMesh.h"

#define   MESH_PREFIX     "Tennis Mesh"
#define   MESH_PASSWORD   "12345678"
#define   MESH_PORT       5555

void ISR_BTN1_PRESSED(void);
void ISR_BTN2_PRESSED(void);

#define BTN1 19 // BTN1 on pin19 with a 3.3k pullup to 5V
#define BTN2 18 // BTN2 on pin18 with a 3.3k pullup to 5V
#define Y_LIGHT 26 // Yellow light on pin 26
#define G_LIGHT 27 // Green light on pin 27

#define BOARD_ID 1

Scheduler userScheduler; // to control your personal task
painlessMesh mesh;

struct Scoreboard {
  int sethome;
  int setaway;
  int gamehome;
  int gameaway;
} scoreboard = {0,0,0,0};

void sendMessage() ; // Prototype so PlatformIO doesn't complain
Task taskSendMessage( TASK_SECOND * 1, TASK_FOREVER, &sendMessage );
// Send Message ISR
void sendMessage() {
  String msg = "0000000";
  msg[0] += BOARD_ID;
  msg[1] += scoreboard.sethome;
  msg[2] += scoreboard.setaway;
  msg[3] += scoreboard.gamehome/10;
  msg[4] += scoreboard.gamehome%10;
  msg[5] += scoreboard.gameaway/10;
  msg[6] += scoreboard.gameaway%10;

  mesh.sendBroadcast(msg);
  taskSendMessage.disable();
}

bool pressed = false;

// BTN1 ISR
void ISR_BTN1_PRESSED(void) {
  // pressed = true;
  scoreboard.sethome+=1;
  digitalWrite(Y_LIGHT, (scoreboard.sethome) % 2);
  taskSendMessage.enable();
}

// BTN2 ISR
void ISR_BTN2_PRESSED(void) {
  // pressed = true;
  scoreboard.setaway+=1;
  digitalWrite(G_LIGHT, (scoreboard.setaway) % 2);
  taskSendMessage.enable();
}

// SETUP
void setup() {
  Serial.begin(115200);

  mesh.setDebugMsgTypes( ERROR | STARTUP );  // set before init() so that you can see startup messages
  mesh.init( MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT);
  userScheduler.addTask( taskSendMessage );

  // Button setup
  // both buttons inputs
  pinMode(BTN1, INPUT_PULLUP);
  pinMode(BTN2, INPUT_PULLUP);

  // both lights outputs
  pinMode(Y_LIGHT, OUTPUT);
  pinMode(G_LIGHT, OUTPUT);
  
  // Attach ISRs to respective buttons
  attachInterrupt(digitalPinToInterrupt(BTN1), ISR_BTN1_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN2), ISR_BTN2_PRESSED, FALLING);
}

void loop() {
  // it will run the user scheduler as well
  mesh.update();
  // if(pressed){
  //   pressed = false;
  //   Serial.println("Pressed button");
  // }
}
