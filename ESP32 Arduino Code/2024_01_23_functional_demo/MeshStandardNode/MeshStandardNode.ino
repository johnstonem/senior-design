//************************************************************
// this is a simple example that uses the painlessMesh library
//
// 1. sends a silly message to every node on the mesh at a random time between 1 and 5 seconds
// 2. prints anything it receives to Serial.print
//
//
//************************************************************
#include "painlessMesh.h"

#define   MESH_PREFIX     "Tennis Mesh"
#define   MESH_PASSWORD   "12345678"
#define   MESH_PORT       5555

void ISR_BTN1_PRESSED(void);
void ISR_BTN2_PRESSED(void);
void ISR_BTN3_PRESSED(void);
void ISR_BTN4_PRESSED(void);
void ISR_BTN5_PRESSED(void);
void ISR_BTN6_PRESSED(void);
void ISR_BTN7_PRESSED(void);

#define BTN1 19 // BTN1 on pin19 with a 3.3k pullup to 5V
#define BTN2 18 // BTN2 on pin18 with a 3.3k pullup to 5V
#define BTN3 17 // BTN2 on pin18 with a 3.3k pullup to 5V
#define BTN4 16 // BTN2 on pin18 with a 3.3k pullup to 5V
#define BTN5 15 // BTN2 on pin18 with a 3.3k pullup to 5V
#define BTN6 14 // BTN2 on pin18 with a 3.3k pullup to 5V
#define BTN7 13 // BTN2 on pin18 with a 3.3k pullup to 5V

#define Y_LIGHT 26 // Yellow light on pin 26
#define G_LIGHT 27 // Green light on pin 27

#define BOARD_ID 1

Scheduler userScheduler; // to control your personal task
painlessMesh mesh;

struct Scoreboard {
  int home_score;    //home score
  int away_score;    //away score
  int home_sets;
  int away_sets;
  int gamehome;
  int gameaway;
} scoreboard = {0,0,0,0,0,0};

void sendMessage() ; // Prototype so PlatformIO doesn't complain
Task taskSendMessage( TASK_SECOND * 1, TASK_FOREVER, &sendMessage );
// Send Message ISR
void sendMessage() {
  String msg = "0000000";
  msg[0] += BOARD_ID;
  msg[1] += scoreboard.home_score;
  msg[2] += scoreboard.away_score;
  msg[3] += scoreboard.gamehome/10;
  msg[4] += scoreboard.gamehome%10;
  msg[5] += scoreboard.gameaway/10;
  msg[6] += scoreboard.gameaway%10;

  mesh.sendBroadcast(msg);
  taskSendMessage.disable();
}

bool pressed = false;

// BTN1 ISR
//Increment home score
void ISR_BTN1_PRESSED(void) {
  // pressed = true;
  scoreboard.home_score+=1;
  digitalWrite(Y_LIGHT, (scoreboard.home_score) % 2);
  taskSendMessage.enable();
}

// BTN2 ISR
//Increment away score
void ISR_BTN2_PRESSED(void) {
  // pressed = true;
  scoreboard.away_score+=1;
  digitalWrite(G_LIGHT, (scoreboard.away_score) % 2);
  taskSendMessage.enable();
}

// BTN3 ISR
//Decrement home score
void ISR_BTN3_PRESSED(void) {
  // pressed = true;
  scoreboard.home_score-=1;
  digitalWrite(G_LIGHT, (scoreboard.home_score) % 2);
  taskSendMessage.enable();
}

// BTN4 ISR
//Decrement away score
void ISR_BTN4_PRESSED(void) {
  // pressed = true;
  scoreboard.away_score-=1;
  digitalWrite(G_LIGHT, (scoreboard.away_score) % 2);
  taskSendMessage.enable();
}

// BTN5 ISR
//New Set Button
void ISR_BTN5_PRESSED(void) {
  // pressed = true;
  //If home > away, icrement home sets won else increment away sets won
  if (scoreboard.home_score > scoreboard.away_score) {
    scoreboard.home_sets+=1;
  } else if (scoreboard.away_score > scoreboard.home_score) {
    scoreboard.away_sets+=1;
  }

  //Set scores back to zero for new set
  if (scoreboard.home_score != scoreboard.away_score) {
    scoreboard.home_score=0;
    scoreboard.away_score=0;
  }

  taskSendMessage.enable();
}

// BTN6 ISR
//Switch Sides 
void ISR_BTN6_PRESSED(void) {
  // pressed = true;
  //Use temp variable for the home score and sets
  int home_score_temp = scoreboard.home_score;
  int home_sets_temp = scoreboard.home_sets;
  //Switch home and away scores and sets
  scoreboard.home_score = scoreboard.away_score;
  scoreboard.home_sets = scoreboard.away_sets;
  scoreboard.away_score = home_score_temp;
  scoreboard.away_sets = home_sets_temp;

  taskSendMessage.enable();
}

// BTN7 ISR
//New Match/reset Button (Potentially can send updates to a CSV about Match information in future)
void ISR_BTN7_PRESSED(void) {
  //Reset score and sets to zero
  scoreboard.home_score = 0;
  scoreboard.home_sets = 0;
  scoreboard.away_score = 0;
  scoreboard.away_score = 0;
  taskSendMessage.enable();
}

// SETUP
void setup() {
  Serial.begin(115200);

  mesh.setDebugMsgTypes( ERROR | STARTUP );  // set before init() so that you can see startup messages
  mesh.init( MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT);
  userScheduler.addTask( taskSendMessage );

  // Button setup
  // both buttons inputs
  pinMode(BTN1, INPUT_PULLUP);
  pinMode(BTN2, INPUT_PULLUP);
  pinMode(BTN3, INPUT_PULLUP);
  pinMode(BTN4, INPUT_PULLUP);
  pinMode(BTN5, INPUT_PULLUP);
  pinMode(BTN6, INPUT_PULLUP);
  pinMode(BTN7, INPUT_PULLUP);


  // both lights outputs
  pinMode(Y_LIGHT, OUTPUT);
  pinMode(G_LIGHT, OUTPUT);
  
  // Attach ISRs to respective buttons
  attachInterrupt(digitalPinToInterrupt(BTN1), ISR_BTN1_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN2), ISR_BTN2_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN3), ISR_BTN3_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN4), ISR_BTN4_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN5), ISR_BTN5_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN6), ISR_BTN6_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN7), ISR_BTN7_PRESSED, FALLING);
}

void loop() {
  // it will run the user scheduler as well
  mesh.update();
  // if(pressed){
  //   pressed = false;
  //   Serial.println("Pressed button");
  // }
}
