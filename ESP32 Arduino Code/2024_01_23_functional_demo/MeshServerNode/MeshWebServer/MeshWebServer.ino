//************************************************************
// this is a simple example that uses the painlessMesh library to
// connect to a another network and broadcast message from a webpage to the edges of the mesh network.
// This sketch can be extended further using all the abilities of the AsyncWebserver library (WS, events, ...)
// for more details
// https://gitlab.com/painlessMesh/painlessMesh/wikis/bridge-between-mesh-and-another-network
// for more details about my version
// https://gitlab.com/Assassynv__V/painlessMesh
// and for more details about the AsyncWebserver library
// https://github.com/me-no-dev/ESPAsyncWebServer
//************************************************************

#include "IPAddress.h"
#include "painlessMesh.h"

#ifdef ESP8266
#include "Hash.h"
#include <ESPAsyncTCP.h>
#else
#include <AsyncTCP.h>
#include <ESPmDNS.h>
#endif
#include <ESPAsyncWebSrv.h>


#define   MESH_PREFIX     "Tennis Mesh"
#define   MESH_PASSWORD   "12345678"
#define   MESH_PORT       5555

void ISR_BTN1_PRESSED(void);
void ISR_BTN2_PRESSED(void);
void ISR_BTN3_PRESSED(void);
void ISR_BTN4_PRESSED(void);
void ISR_BTN5_PRESSED(void);
void ISR_BTN6_PRESSED(void);
void ISR_BTN7_PRESSED(void);

#define BTN1 19 // BTN1 on pin19 with a 3.3k pullup to 5V
#define BTN2 18 // BTN2 on pin18 with a 3.3k pullup to 5V
#define BTN3 18 // BTN2 on pin18 with a 3.3k pullup to 5V
#define BTN4 18 // BTN2 on pin18 with a 3.3k pullup to 5V
#define BTN5 18 // BTN2 on pin18 with a 3.3k pullup to 5V
#define BTN6 18 // BTN2 on pin18 with a 3.3k pullup to 5V
#define BTN7 18 // BTN2 on pin18 with a 3.3k pullup to 5V

#define Y_LIGHT 26 // Yellow light on pin 26
#define G_LIGHT 27 // Green light on pin 27

#define BOARD_ID 0

// Prototype
IPAddress getlocalIP();
void sendMessage();
void receivedCallback( const uint32_t &from, const String &msg );

String queue[20];
int q_index = -1;

// Score base
struct Scoreboard {
  int home_score = 0;
  int home_sets = 0;
  int away_score = 0;
  int away_sets = 0;
  int gamehome = 0;
  int gameaway = 0;
};
Scoreboard scoreboard[7];

bool pressed = false;

// BTN1 ISR
//Increment home score
void ISR_BTN1_PRESSED(void) {
  // pressed = true;
  scoreboard[BOARD_ID].home_score+=1;
  digitalWrite(Y_LIGHT, (scoreboard[BOARD_ID].home_score) % 2);
}

// BTN2 ISR
//Increment away score
void ISR_BTN2_PRESSED(void) {
  // pressed = true;
  scoreboard[BOARD_ID].away_score+=1;
  digitalWrite(G_LIGHT, (scoreboard[BOARD_ID].away_score) % 2);
}

// BTN3 ISR
//Decrement home score
void ISR_BTN3_PRESSED(void) {
  // pressed = true;
  scoreboard[BOARD_ID].home_score-=1;
  digitalWrite(G_LIGHT, (scoreboard[BOARD_ID].home_score) % 2);
}

// BTN4 ISR
//Decrement away score
void ISR_BTN4_PRESSED(void) {
  // pressed = true;
  scoreboard[BOARD_ID].away_score-=1;
  digitalWrite(G_LIGHT, (scoreboard[BOARD_ID].away_score) % 2);
}

// BTN5 ISR
//New Set Button
void ISR_BTN5_PRESSED(void) {
  // pressed = true;
  //If home > away, icrement home sets won else increment away sets won
  if (scoreboard[BOARD_ID].home_score > scoreboard[BOARD_ID].away_score) {
    scoreboard[BOARD_ID].home_sets+=1;
  } else if (scoreboard[BOARD_ID].away_score > scoreboard[BOARD_ID].home_score) {
    scoreboard[BOARD_ID].away_sets+=1;
  }

  //Set scores back to zero for new set
  if (scoreboard[BOARD_ID].home_score != scoreboard[BOARD_ID].away_score) {
    scoreboard[BOARD_ID].home_score=0;
    scoreboard[BOARD_ID].away_score=0;
  }
}

// BTN6 ISR
//Switch Sides 
void ISR_BTN6_PRESSED(void) {
  // pressed = true;
  //Use temp variable for the home score and sets
  int home_score_temp = scoreboard[BOARD_ID].home_score;
  int home_sets_temp = scoreboard[BOARD_ID].home_sets;
  //Switch home and away scores and sets
  scoreboard[BOARD_ID].home_score = scoreboard[BOARD_ID].away_score;
  scoreboard[BOARD_ID].home_sets = scoreboard[BOARD_ID].away_sets;
  scoreboard[BOARD_ID].away_score = home_score_temp;
  scoreboard[BOARD_ID].away_sets = home_sets_temp;
}

// BTN7 ISR
//New Match/reset Button (Potentially can send updates to a CSV about Match information in future)
void ISR_BTN7_PRESSED(void) {
  //Reset score and sets to zero
  scoreboard[BOARD_ID].home_score = 0;
  scoreboard[BOARD_ID].home_sets = 0;
  scoreboard[BOARD_ID].away_score = 0;
  scoreboard[BOARD_ID].away_score = 0;
}

painlessMesh  mesh;
AsyncWebServer server(80);
IPAddress myIP(0,0,0,0);
IPAddress myAPIP(0,0,0,0);

char html[] = "<!DOCTYPE html>"
  "<html>"
  "<head>"
      "<title>%s</title>"
      "<style>"
          "body {"
              "font-family: 'Courier New', monospace;"
          "}"
          ".court {"
              "border: 2px solid #333;"
              "padding: 20px;"
              "margin: 20px;"
              "float: left;"
              "width: 300px;"
              "text-align: center;"
              "background-color: #f5f5f5;"
              "box-shadow: 5px 5px 15px rgba(0,0,0,0.2);"
          "}"
          "h2 {"
              "font-size: 24px;"
              "margin-bottom: 10px;"
          "}"
          "button {"
              "font-size: 18px;"
              "padding: 10px 20px;"
              "margin: 10px;"
              "background-color: #333;"
              "color: #fff;"
              "border: none;"
              "cursor: pointer;"
              "border-radius: 5px;"
          "}"
          "span {"
              "font-size: 48px;"
              "display: block;"
              "margin-bottom: 20px;"
          "}"
      "</style>"
  "<!-- Refresh Content goes here -->"
      "%s"
  "</head>"
  "<body>"
  "<h1 style=\"text-align: center;\">%s</h1>"
  "<div id=\"courts\">"
      "<div class=\"court\" id=\"court1\">"
          "<h2>Court 1</h2>"
          "<button onclick=\"incrementScore('court1', 'player1')\">Player 1</button>"
          "<span id=\"score1\">%d</span>"
          "<button onclick=\"incrementScore('court1', 'player2')\">Player 2</button>"
          "<span id=\"score2\">%d</span>"
      "</div>"
      "<div class=\"court\" id=\"court2\">"
          "<h2>Court 2</h2>"
          "<button onclick=\"incrementScore('court1', 'player3')\">Player 1</button>"
          "<span id=\"score3\">%d</span>"
          "<button onclick=\"incrementScore('court1', 'player4')\">Player 2</button>"
          "<span id=\"score4\">%d</span>"
      "</div>"
      "<div class=\"court\" id=\"court3\">"
          "<h2>Court 3</h2>"
          "<button onclick=\"incrementScore('court1', 'player5')\">Player 1</button>"
          "<span id=\"score5\">%d</span>"
          "<button onclick=\"incrementScore('court1', 'player6')\">Player 2</button>"
          "<span id=\"score6\">%d</span>"
      "</div>"
      "<div class=\"court\" id=\"court4\">"
          "<h2>Court 4</h2>"
          "<button onclick=\"incrementScore('court1', 'player7')\">Player 1</button>"
          "<span id=\"score7\">%d</span>"
          "<button onclick=\"incrementScore('court1', 'player8')\">Player 2</button>"
          "<span id=\"score8\">%d</span>"
      "</div>"
      "<div class=\"court\" id=\"court5\">"
          "<h2>Court 5</h2>"
          "<button onclick=\"incrementScore('court1', 'player9')\">Player 1</button>"
          "<span id=\"score9\">%d</span>"
          "<button onclick=\"incrementScore('court1', 'playerA')\">Player 2</button>"
          "<span id=\"scoreA\">%d</span>"
      "</div>"
      "<div class=\"court\" id=\"court6\">"
          "<h2>Court 6</h2>"
          "<button onclick=\"incrementScore('court1', 'playerB')\">Player 1</button>"
          "<span id=\"scoreB\">%d</span>"
          "<button onclick=\"incrementScore('court1', 'playerC')\">Player 2</button>"
          "<span id=\"scoreC\">%d</span>"
      "</div>"
      "<div class=\"court\" id=\"court7\">"
          "<h2>Court 7</h2>"
          "<button onclick=\"incrementScore('court1', 'playerD')\">Player 1</button>"
          "<span id=\"scoreD\">%d</span>"
          "<button onclick=\"incrementScore('court1', 'playerE')\">Player 2</button>"
          "<span id=\"scoreE\">%d</span>"
      "</div>"
  "</div>"
  "<button onclick=\"copyToClipboard('Court 1: Player 1 - ' + document.getElementById('score1').innerHTML + ', Player 2 - ' + document.getElementById('score2').innerHTML)\">Copy Scores</button>"
  "<script>"
      "function incrementScore(courtId, player) {"
          "var scoreStr = 'score' + player.slice(-1);"
          "var scoreElement = document.getElementById(scoreStr);"
          "var currentScore = parseInt(scoreElement.innerHTML);"
          "if (isNaN(currentScore)) {"
              "scoreElement.innerHTML = 0;"
          "} else {"
              "scoreElement.innerHTML = currentScore + 1;"
          "}"
      "}"
    "function copyToClipboard(text) {"
          "var textarea = document.createElement(\"textarea\");"
          "textarea.value = text;"
          "document.body.appendChild(textarea);"
          "textarea.select();"
          "document.execCommand(\"copy\");"
          "document.body.removeChild(textarea);"
          "alert(\"Scores copied to clipboard!\");"
      "}"
  "</script>"
  "</body>"
  "</html>";
char message[5000];


void receivedCallback( const uint32_t &from, const String &msg ) {
  queue[++q_index] = msg;
}

void processCall(){
  if(q_index < 0) return;
  Serial.println("Recieved");

  String msg = queue[q_index--];
  uint32_t id = msg[0]-'0';
  scoreboard[id].home_score = msg[1]-'0';
  scoreboard[id].away_score = msg[2]-'0';
  scoreboard[id].gamehome = 10*(msg[3]-'0') + (msg[4]-'0');
  scoreboard[id].gameaway = 10*(msg[3]-'0') + (msg[4]-'0');
}

void setup() {
  Serial.begin(115200);

  // Channel set to 6. Make sure to use the same channel for your mesh and for you other
  // network (STATION_SSID)
  mesh.init( MESH_PREFIX, MESH_PASSWORD, MESH_PORT, WIFI_AP_STA);
  mesh.onReceive(&receivedCallback);

  // Bridge node, should (in most cases) be a root node. See [the wiki](https://gitlab.com/painlessMesh/painlessMesh/wikis/Possible-challenges-in-mesh-formation) for some background
  mesh.setRoot(true);
  // This node and all other nodes should ideally know the mesh contains a root, so call this on all nodes
  mesh.setContainsRoot(true);

  myAPIP = IPAddress(mesh.getAPIP());

  if (MDNS.begin("tennis")) {
    Serial.println("MDNS Responder Started: Server: tennis.local");
  }

  // Button setup
  // both buttons inputs
  pinMode(BTN1, INPUT_PULLUP);
  pinMode(BTN2, INPUT_PULLUP);
  pinMode(BTN3, INPUT_PULLUP);
  pinMode(BTN4, INPUT_PULLUP);
  pinMode(BTN5, INPUT_PULLUP);
  pinMode(BTN6, INPUT_PULLUP);
  pinMode(BTN7, INPUT_PULLUP);

  // // both lights outputs
  pinMode(Y_LIGHT, OUTPUT);
  pinMode(G_LIGHT, OUTPUT);

  // Attach ISRs to respective buttons
  attachInterrupt(digitalPinToInterrupt(BTN1), ISR_BTN1_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN2), ISR_BTN2_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN3), ISR_BTN3_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN4), ISR_BTN4_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN5), ISR_BTN5_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN6), ISR_BTN6_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN7), ISR_BTN7_PRESSED, FALLING);

  //Async webserver
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/html", message);
  });
  server.begin();
}

void loop() {
  mesh.update();
  if(myIP != getlocalIP()){
    myIP = getlocalIP();
    Serial.println("My IP is " + myIP.toString());
  }
  if(pressed){
    pressed = false;
    Serial.println("Pressed button");
  }
  processCall();

  sprintf(message, html,
            "Tennis Scoreboard Tracking",     // Title
            "<meta http-equiv=\"refresh\" content=\"1\">",     // Refresh content
            "Tennis Scoreboard Tracking",     // HTML Header
            scoreboard[0].home_score,
            scoreboard[0].away_score,
            scoreboard[1].home_score,
            scoreboard[1].away_score,
            scoreboard[2].home_score,
            scoreboard[2].away_score,
            scoreboard[3].home_score,
            scoreboard[3].away_score,
            scoreboard[4].home_score,
            scoreboard[4].away_score,
            scoreboard[5].home_score,
            scoreboard[5].away_score,
            scoreboard[6].home_score,
            scoreboard[6].away_score
          );
}

IPAddress getlocalIP() {
  return IPAddress(mesh.getStationIP());
}
