/*
  Button

  Turns on and off a light emitting diode(LED) connected to digital pin 13,
  when pressing a pushbutton attached to pin 2.

  The circuit:
  - LED attached from pin 13 to ground through 220 ohm resistor
  - pushbutton attached to pin 2 from +5V
  - 10K resistor attached to pin 2 from ground

  - Note: on most Arduinos there is already an LED on the board
    attached to pin 13.

  created 2005
  by DojoDave <http://www.0j0.org>
  modified 30 Aug 2011
  by Tom Igoe

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Button
*/

// constants won't change. They're used here to set pin numbers:
const int BTN1 = 39;  // the number of the pushbutton pin
bool tylerIsBackSeatProgramming = false;

void ISR_BTN1_PRESSED(void) {
  tylerIsBackSeatProgramming = true;
}

void setup() {
  // initialize the pushbutton pin as an input:
  pinMode(BTN1, INPUT);
  pinMode(12, OUTPUT);
  pinMode(14, OUTPUT);
  pinMode(27, OUTPUT);
  pinMode(26, OUTPUT);

  pinMode(22, OUTPUT);
  digitalWrite(22, LOW);

  attachInterrupt(digitalPinToInterrupt(BTN1), ISR_BTN1_PRESSED, FALLING);
  Serial.begin(115200);
}

void loop() {
  if(tylerIsBackSeatProgramming){
    static int button_count = 0;
    Serial.println("Fuck Charles!!");
    Serial.println("abcdefghijklmnopqrstuvwxyz!!");
    button_count = ++button_count % 11;
    Serial.println(button_count);

    tylerIsBackSeatProgramming = false;
    digitalWrite(12, (button_count&0b0001)>>0);
    digitalWrite(14, (button_count&0b0010)>>1);
    digitalWrite(27, (button_count&0b0100)>>2);
    digitalWrite(26, (button_count&0b1000)>>3);

  }
}