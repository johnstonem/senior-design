#ifndef __SCORES_HPP__
#define __SCORES_HPP__

struct Scoreboard {
  int home_score;    //home score
  int away_score;    //away score
  int home_sets;
  int away_sets;
  int gamehome;
  int gameaway;
};

#endif