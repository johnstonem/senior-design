#include "Arduino.h"
#include "IPAddress.h"
#include <AsyncTCP.h>
#include <ESPmDNS.h>
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>
#include "painlessMesh.h"
#include "Mesh.hpp"
#include "Scores.hpp"
#include "IO.hpp"

extern painlessMesh mesh;

// Pull scoreboard
extern Scoreboard scoreboard[7];

// setup web stuff
AsyncWebServer server(80);
IPAddress myIP(0,0,0,0);
IPAddress myAPIP(0,0,0,0);

// Create a WebSocket object
AsyncWebSocket ws("/ws");

// Get statistics about games and return JSON object
String getStats(){
  // Json Variable to Hold json data
  JsonDocument json_var;
  for(int i = 0; i < 7; i++){
      String label = "score";
      json_var[label+(i+1)+"H"] = String(scoreboard[i].home_score);
      json_var[label+(i+1)+"A"] = String(scoreboard[i].away_score);
      label = "set";
      json_var[label+(i+1)+"H"] = String(scoreboard[i].home_sets);
      json_var[label+(i+1)+"A"] = String(scoreboard[i].away_sets);
      // TODO: Maybe add names???
  }
  String jsonString = "";
  serializeJson(json_var, jsonString);
  Serial.println("Setting Stats: "+jsonString);
  return jsonString;
}

void notifyClients(String json_var) {
  ws.textAll(json_var);
}

void updateNamesInStructs(){
  Serial.println("Updating Struct names");
  File file = SPIFFS.open("/names.csv", FILE_READ);
  if(!file){
      Serial.println("- failed to open file for writing");
      return;
  }
  String label = "name";
  for(int i = 1; i <= 7; i++){
    scoreboard[i-1].name_home = file.readStringUntil(',');
    scoreboard[i-1].name_away = file.readStringUntil(',');
  }
  file.close();
}

void setNames(String names){
  Serial.println("Setting names: "+names);
  File file = SPIFFS.open("/names.csv", FILE_WRITE);
  file.print(names);
  file.close();
}

String getNames(void){
  File file = SPIFFS.open("/names.csv", FILE_READ);
  if(!file){
      Serial.println("- failed to open file for writing");
      return "[]";
  }
  String label = "name";
  // Json Variable to Hold json data
  JsonDocument json_var;
  for(int i = 1; i <= 7; i++){
    json_var[label+i+"H"] = file.readStringUntil(',');
    json_var[label+i+"A"] = file.readStringUntil(',');
  }
  file.close();
  String jsonString = "";
  serializeJson(json_var, jsonString);
  return jsonString;
}

void handleWebSocketMessage(void *arg, uint8_t *data, size_t len) {
  AwsFrameInfo *info = (AwsFrameInfo*)arg;
  if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT) {
    char* message = (char*)malloc(200*sizeof(char));
    strncpy(message, (char*)data, len); 
    message[len]=0;
    
    Serial.println("Got Message: "+String(message));
    // Check if the message is "getjson_var"
    if (strcmp(message, "getStats") == 0) {
      // if it is, send current sensor json_var
      notifyClients(getStats());
    } else if (strcmp(message, "getNames") == 0) {
      // trying to update the names!
      notifyClients(getNames());
    } else {
      // trying to update the names!
      // We get a , separated, alternating (H/A) list of names!
      setNames(message);
      updateNamesInStructs();
    }
    free(message);
  }
}

void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len) {
  // Serial.println(type);
  switch (type) {
    case WS_EVT_CONNECT:
      Serial.printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
      break;
    case WS_EVT_DISCONNECT:
      Serial.printf("WebSocket client #%u disconnected\n", client->id());
      break;
    case WS_EVT_DATA:
      handleWebSocketMessage(arg, data, len);
      break;
    case WS_EVT_PONG:
    case WS_EVT_ERROR:
      break;
  }
}

void init_WebSocket() {
  ws.onEvent(onEvent);
  server.addHandler(&ws);
  Serial.println("Web Socket Started");
}

void init_WIFI(void){
    myAPIP = IPAddress(mesh.getAPIP());
    if (MDNS.begin("tennis")) {
        Serial.println("MDNS Responder Started: Server: tennis.local");
    }
}

void init_SPIFFS() {
  if (!SPIFFS.begin(true)) {
    Serial.println("An error has occurred while mounting SPIFFS");
  }
  Serial.println("SPIFFS mounted successfully.");
}

// Setup the server and SPIFF stuff
void Server_Setup(void){
    init_SPIFFS();
    init_WIFI();
    init_WebSocket();

    //Async webserver
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(SPIFFS, "/index.html", "text/html");
    });
    
    // host the SPIFFS content!
    server.serveStatic("/", SPIFFS, "/");
    server.begin();

    Serial.println("Set up Server.");
}

void write_scores(void){
    File file = SPIFFS.open("/scores.csv", FILE_WRITE);
    if(!file){
        Serial.println("- failed to open file for writing");
        return;
    }
    file.println("nameHome,setHome,scoreHome,nameAway,setAway,scoreAway");
    for(int i = 0; i < 7; i++){
      file.print(scoreboard[i].name_home+",");
      file.print(String(scoreboard[i].home_sets)+",");
      file.print(String(scoreboard[i].home_score)+',');
      file.print(scoreboard[i].name_away+",");
      file.print(String(scoreboard[i].away_sets)+",");
      file.println(String(scoreboard[i].away_score));
    }
    file.close();
}

unsigned long lastTime = 0, timerDelay = 3000;
void Server_Update(void){
    if ((millis() - lastTime) > timerDelay) {
        // This is just to test that it's updating the web server!!
        scoreboard[1].home_score++;
        scoreboard[1].away_sets = ++scoreboard[1].away_sets % 3;

        // actual things to do every 3000 seconds
        write_scores();
        getNames();
        lastTime = millis();
    }
    ws.cleanupClients();
}
