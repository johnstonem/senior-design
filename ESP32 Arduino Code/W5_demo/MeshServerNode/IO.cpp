#include "IO.hpp"
#include "Scores.hpp"
#include "Arduino.h"
#include "Mesh.hpp"

// Pull scoreboard[BOARD_ID]
extern Scoreboard scoreboard[7];


// boolean flag of whether left or right is home
bool leftIsHome = true;

int l_bits[] = {SEG1_BIT1, SEG1_BIT2, SEG1_BIT3, SEG1_BIT4};
int r_bits[] = {SEG2_BIT1, SEG2_BIT2, SEG2_BIT3, SEG2_BIT4};
// updates all of the pin statuses
void update_pin_out(void){
  // :)
  digitalWrite(L_HOME,leftIsHome);
  digitalWrite(R_HOME,!leftIsHome);
  if(leftIsHome){
    digitalWrite(L_MUL, scoreboard[BOARD_ID].home_score >= 10);
    digitalWrite(L_SET_LED_1, scoreboard[BOARD_ID].home_sets > 0);
    digitalWrite(L_SET_LED_2, scoreboard[BOARD_ID].home_sets > 1);
    for(int i = 0; i < 4; i++){
      digitalWrite(l_bits[i], ((1<<i)&(scoreboard[BOARD_ID].home_score%10))>0);
    }

    digitalWrite(R_MUL, scoreboard[BOARD_ID].away_score >= 10);
    digitalWrite(R_SET_LED_1, scoreboard[BOARD_ID].away_sets > 0);
    digitalWrite(R_SET_LED_2, scoreboard[BOARD_ID].away_sets > 1);
    for(int i = 0; i < 4; i++){
      digitalWrite(r_bits[i], ((1<<i)&(scoreboard[BOARD_ID].away_score%10))>0);
    }

  } else {
    digitalWrite(L_MUL, scoreboard[BOARD_ID].away_score >= 10);
    digitalWrite(L_SET_LED_1, scoreboard[BOARD_ID].away_sets > 0);
    digitalWrite(L_SET_LED_2, scoreboard[BOARD_ID].away_sets > 1);
    for(int i = 0; i < 4; i++){
      digitalWrite(l_bits[i], ((1<<i)&(scoreboard[BOARD_ID].away_score%10))>0);
    }

    digitalWrite(R_MUL, scoreboard[BOARD_ID].home_score >= 10);
    digitalWrite(R_SET_LED_1, scoreboard[BOARD_ID].home_sets > 0);
    digitalWrite(R_SET_LED_2, scoreboard[BOARD_ID].home_sets > 1);
    for(int i = 0; i < 4; i++){
      digitalWrite(r_bits[i], ((1<<i)&(scoreboard[BOARD_ID].home_score%10))>0);
    }
  }
}

// BTN1 ISR
//Increment left score
void ISR_BTN1_PRESSED(void) {
  if(leftIsHome) scoreboard[BOARD_ID].home_score = min(scoreboard[BOARD_ID].home_score+1, 19);
  else scoreboard[BOARD_ID].away_score = min(scoreboard[BOARD_ID].away_score+1, 19);
}

// BTN2 ISR
//Increment right score
void ISR_BTN2_PRESSED(void) {
  if(leftIsHome) scoreboard[BOARD_ID].away_score = min(scoreboard[BOARD_ID].away_score+1, 19);
  else scoreboard[BOARD_ID].home_score = min(scoreboard[BOARD_ID].home_score+1, 19);
}

// BTN3 ISR
//Decrement left score
void ISR_BTN3_PRESSED(void) {
  if(leftIsHome) scoreboard[BOARD_ID].home_score = max(scoreboard[BOARD_ID].home_score-1,0);
  else scoreboard[BOARD_ID].away_score = max(scoreboard[BOARD_ID].away_score-1,0);
}

// BTN4 ISR
//Decrement right score
void ISR_BTN4_PRESSED(void) {
  if(leftIsHome) scoreboard[BOARD_ID].away_score = max(scoreboard[BOARD_ID].away_score-1,0);
  else scoreboard[BOARD_ID].home_score = max(scoreboard[BOARD_ID].home_score-1,0);
}

// BTN5 ISR
//New Set Button
void ISR_BTN5_PRESSED(void) {
  // pressed = true;
  //If home > away, increment home sets won else increment away sets won
  if (scoreboard[BOARD_ID].home_score > scoreboard[BOARD_ID].away_score) {
    scoreboard[BOARD_ID].home_sets+=1;
  } else if (scoreboard[BOARD_ID].away_score > scoreboard[BOARD_ID].home_score) {
    scoreboard[BOARD_ID].away_sets+=1;
  }

  //Set scores back to zero for new set
  if (scoreboard[BOARD_ID].home_score != scoreboard[BOARD_ID].away_score) {
    scoreboard[BOARD_ID].home_score=0;
    scoreboard[BOARD_ID].away_score=0;
  }
}

// BTN6 ISR
//Switch Sides 
void ISR_BTN6_PRESSED(void) {
  // change whether left is home or not
  leftIsHome = !leftIsHome;

  // no updates to the scores, just the sides
}

void reset_board(void){
  //Reset score and sets to zero
  scoreboard[BOARD_ID].home_score = 0;
  scoreboard[BOARD_ID].home_sets = 0;
  scoreboard[BOARD_ID].away_score = 0;
  scoreboard[BOARD_ID].away_score = 0;
  bool leftIsHome = true;
}

// BTN7 ISR
//New Match/reset Button (Potentially can send updates to a CSV about Match information in future)
void ISR_BTN7_PRESSED(void) {
  reset_board();
}

// define setup for the GPIO pins
void IO_Setup(void){
  // Button setup
  // both buttons inputs
  pinMode(BTN1, INPUT);
  pinMode(BTN2, INPUT);
  pinMode(BTN3, INPUT);
  pinMode(BTN4, INPUT);
  pinMode(BTN5, INPUT);
  pinMode(BTN6, INPUT);
  pinMode(BTN7, INPUT);
  
  // Attach ISRs to respective buttons
  attachInterrupt(digitalPinToInterrupt(BTN1), ISR_BTN1_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN2), ISR_BTN2_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN3), ISR_BTN3_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN4), ISR_BTN4_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN5), ISR_BTN5_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN6), ISR_BTN6_PRESSED, FALLING);
  attachInterrupt(digitalPinToInterrupt(BTN7), ISR_BTN7_PRESSED, FALLING);

  // set up outputs
  pinMode(SEG1_BIT1, OUTPUT);
  pinMode(SEG1_BIT2, OUTPUT);
  pinMode(SEG1_BIT3, OUTPUT);
  pinMode(SEG1_BIT4, OUTPUT);

  pinMode(SEG2_BIT1, OUTPUT);
  pinMode(SEG2_BIT2, OUTPUT);
  pinMode(SEG2_BIT3, OUTPUT);
  pinMode(SEG2_BIT4, OUTPUT);

  pinMode(L_MUL, OUTPUT);
  pinMode(L_SET_LED_1, OUTPUT);
  pinMode(L_SET_LED_2, OUTPUT);
  pinMode(L_HOME, OUTPUT);

  pinMode(R_MUL, OUTPUT);
  pinMode(R_SET_LED_1, OUTPUT);
  pinMode(R_SET_LED_2, OUTPUT);
  pinMode(R_HOME, OUTPUT);

  // reset the board
  reset_board();
}








