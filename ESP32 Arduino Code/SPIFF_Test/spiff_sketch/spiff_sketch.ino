/*********
  Rui Santos
  Complete project details at https://randomnerdtutorials.com  
*********/

#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>
#include "SPIFFS.h"
#include <ArduinoJson.h>
#include <ESPmDNS.h>
#include "painlessMesh.h"


#define   MESH_PREFIX     "ASSEATER"
#define   MESH_PASSWORD   "penis123"
#define   MESH_PORT       5555
painlessMesh  mesh;

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// Create a WebSocket object
AsyncWebSocket ws("/ws");

// Json Variable to Hold Sensor Readings
JsonDocument readings;

// Timer variables
unsigned long lastTime = 0;
unsigned long timerDelay = 30000;

// Get Sensor Readings and return JSON object
String getSensorReadings(){
  static int temperature = 0;
  static int humidity = 0;
  static int pressure = 0;
  temperature++;
  humidity++;
  pressure++;
  readings["temperature"] = String(temperature);
  readings["humidity"] =  String(humidity);
  readings["pressure"] = String(pressure);
  String jsonString = "";
  serializeJson(readings, jsonString);
  return jsonString;
}

// Initialize SPIFFS
void initSPIFFS() {
  if (!SPIFFS.begin(true)) {
    Serial.println("An error has occurred while mounting SPIFFS");
  }
  Serial.println("SPIFFS mounted successfully");
}

// Initialize WiFi
void initWiFi() {
  mesh.init( MESH_PREFIX, MESH_PASSWORD, MESH_PORT, WIFI_AP_STA);

  // Bridge node, should (in most cases) be a root node. See [the wiki](https://gitlab.com/painlessMesh/painlessMesh/wikis/Possible-challenges-in-mesh-formation) for some background
  mesh.setRoot(true);
  // This node and all other nodes should ideally know the mesh contains a root, so call this on all nodes
  mesh.setContainsRoot(true);

  if (MDNS.begin("tennis")) {
    Serial.println("MDNS Responder Started: Server: tennis.local");
  }
}

void notifyClients(String sensorReadings) {
  ws.textAll(sensorReadings);
}

void handleWebSocketMessage(void *arg, uint8_t *data, size_t len) {
  AwsFrameInfo *info = (AwsFrameInfo*)arg;
  
  Serial.println("HANDLING WEB SOCKET MESSAGE");
  if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT) {
    data[len] = 0;
    String message = (char*)data;
    // Check if the message is "getReadings"
    if (strcmp((char*)data, "getReadings") == 0) {
      //if it is, send current sensor readings
      Serial.println("GETTING READINGS");
      String sensorReadings = getSensorReadings();
      Serial.print(sensorReadings);
      notifyClients(sensorReadings);
    } else Serial.println("Non-GETSENSOR Message.");
  }
}

void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len) {
  switch (type) {
    case WS_EVT_CONNECT:
      Serial.printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
      break;
    case WS_EVT_DISCONNECT:
      Serial.printf("WebSocket client #%u disconnected\n", client->id());
      break;
    case WS_EVT_DATA:
      handleWebSocketMessage(arg, data, len);
      break;
    case WS_EVT_PONG:
    case WS_EVT_ERROR:
      break;
  }
}

void initWebSocket() {
  ws.onEvent(onEvent);
  server.addHandler(&ws);
}

void writeSetupFiles(){
  File file;

  Serial.println("WRITING HTML");
  String html = "<!DOCTYPE html><html>    <head>        <title>ESP IOT DASHBOARD</title>        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">        <link rel=\"icon\" type=\"image/png\" href=\"favicon.png\">        <link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">    </head>    <body>        <div class=\"topnav\">            <h1>SENSOR READINGS (WEBSOCKET)</h1>        </div>        <div class=\"content\">            <div class=\"card-grid\">                <div class=\"card\">                    <p class=\"card-title\"><i class=\"fas fa-thermometer-threequarters\" style=\"color:#059e8a;\"></i> Temperature</p>                    <p class=\"reading\"><span id=\"temperature\"></span> &deg;C</p>                </div>                <div class=\"card\">                    <p class=\"card-title\"> Humidity</p>                    <p class=\"reading\"><span id=\"humidity\"></span> &percnt;</p>                </div>                <div class=\"card\">                    <p class=\"card-title\"> Pressure</p>                    <p class=\"reading\"><span id=\"pressure\"></span> hpa</p>                </div>            </div>        </div>        <script src=\"script.js\"></script>    </body></html>";
  file = SPIFFS.open("/index.html", "w");
  file.print(html);
  file.close();

  Serial.println("WRITING CSS");
  String css = "html {    font-family: Arial, Helvetica, sans-serif;    display: inline-block;    text-align: center;}h1 {    font-size: 1.8rem;    color: white;}.topnav {    overflow: hidden;    background-color: #0A1128;}body {    margin: 0;}.content {    padding: 50px;}.card-grid {    max-width: 800px;    margin: 0 auto;    display: grid;    grid-gap: 2rem;    grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));}.card {    background-color: white;    box-shadow: 2px 2px 12px 1px rgba(140,140,140,.5);}.card-title {    font-size: 1.2rem;    font-weight: bold;    color: #034078}.reading {    font-size: 1.2rem;    color: #1282A2;}";
  file = SPIFFS.open("/style.css", "w");
  file.print(css);
  file.close();
  
  Serial.println("WRITING JS");
  String js = "var gateway = `ws://${window.location.hostname}/ws`;var websocket; window.addEventListener('load', onload);function onload(event) {    initWebSocket();}function getReadings(){    websocket.send('getReadings');}function initWebSocket() {    console.log('Trying to open a WebSocket connection…');    websocket = new WebSocket(gateway);    websocket.onopen = onOpen;    websocket.onclose = onClose;    websocket.onmessage = onMessage;} function onOpen(event) {    console.log('Connection opened');    getReadings();}function onClose(event) {    console.log('Connection closed');    setTimeout(initWebSocket, 2000);} function onMessage(event) {    console.log(event.data);    var myObj = JSON.parse(event.data);    var keys = Object.keys(myObj);    for (var i = 0; i < keys.length; i++){        var key = keys[i];        document.getElementById(key).innerHTML = myObj[key];    }}";
  file = SPIFFS.open("/script.js", "w");
  file.print(js);
  file.close();
}

void setup() {
  Serial.begin(115200);
  initWiFi();
  initSPIFFS();
  writeSetupFiles();
  initWebSocket();

  // Web Server Root URL
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/index.html", "text/html");
  });
  server.serveStatic("/", SPIFFS, "/");

  // Start server
  server.begin();
}

void loop() {
  if ((millis() - lastTime) > timerDelay) {
    String sensorReadings = getSensorReadings();
    Serial.println("WOOHOO!");
    Serial.println(sensorReadings);
    notifyClients(sensorReadings);

    lastTime = millis();
  }

  mesh.update();
  ws.cleanupClients();
}