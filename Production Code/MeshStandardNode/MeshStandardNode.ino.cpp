//************************************************************
// this is a simple example that uses the painlessMesh library
//
// 1. sends a silly message to every node on the mesh at a random time between 1 and 5 seconds
// 2. prints anything it receives to Serial.print
//
//************************************************************
#include "IO.hpp"
#include "Mesh.hpp"
#include "Scores.hpp"
#include "Arduino.h"

Scoreboard scoreboard = {0,0,0,0,0,0};

// SETUP
void setup() {
  Serial.begin(115200);
  IO_Setup();
  Mesh_Setup();
}

void loop() {
  Mesh_Update();
  update_pin_out();
}
