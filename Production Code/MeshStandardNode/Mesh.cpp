#include "Mesh.hpp"
#include "Arduino.h"
#include "Scores.hpp"
#include "painlessMesh.h"

// Pull scoreboard
extern Scoreboard scoreboard;

Scheduler userScheduler; // to control your personal task
Task taskSendMessage( TASK_SECOND * 1, TASK_FOREVER, &sendMessage ); // the task to send updates
painlessMesh mesh;

// Send Message ISR
void sendMessage() {
  String msg = "0000000";
  msg[0] += BOARD_ID;
  msg[1] += scoreboard.home_score;
  msg[2] += scoreboard.away_score;
  msg[3] += scoreboard.gamehome/10;
  msg[4] += scoreboard.gamehome%10;
  msg[5] += scoreboard.gameaway/10;
  msg[6] += scoreboard.gameaway%10;

  mesh.sendBroadcast(msg);
  // taskSendMessage.setInterval(TASK_SECOND * 1);
  taskSendMessage.disable();
}

void Mesh_Setup(void) {
  mesh.setDebugMsgTypes( ERROR | STARTUP );  // set before init() so that you can see startup messages
  mesh.init( MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT);
  userScheduler.addTask( taskSendMessage );
  // taskSendMessage.enable();
}

void Mesh_Update(void){
  mesh.update();
}
