#ifndef __MESH_HPP__
#define __MESH_HPP__

#define BOARD_ID 1

#define   MESH_PREFIX     "Tennis Mesh"
#define   MESH_PASSWORD   "12345678"
#define   MESH_PORT       5555

void sendMessage() ; // Prototype so PlatformIO doesn't complain
void Mesh_Setup(void);
void Mesh_Update(void);

#endif
