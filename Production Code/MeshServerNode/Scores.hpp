#ifndef __SCORES_HPP__
#define __SCORES_HPP__

#include "Arduino.h"

struct Scoreboard {
  int home_score;    //home score
  int away_score;    //away score
  int home_sets;
  int away_sets;
  int gamehome;
  int gameaway;
  String name_away;
  String name_home;
};

#endif