#include "Mesh.hpp"
#include "Arduino.h"
#include "Scores.hpp"

#include "painlessMesh.h"

// Pull scoreboard
extern Scoreboard scoreboard[7];

painlessMesh mesh;

/** Handling messages within a queue **/
String queue[20];
int q_index = 0;

void receivedCallback( const uint32_t &from, const String &msg ) {
  queue[q_index++] = msg;
}

void processCall(){
  if(q_index == 0) return;
  String msg = queue[--q_index];

  Serial.println("Received"); // print after to avoid interrupt between check and modification
  uint32_t id = msg[0]-'0';
  scoreboard[id].home_score = msg[1]-'0';
  scoreboard[id].away_score = msg[2]-'0';
  scoreboard[id].gamehome = 10*(msg[3]-'0') + (msg[4]-'0');
  scoreboard[id].gameaway = 10*(msg[5]-'0') + (msg[6]-'0');
}

/** SETUP THE MESH NETWORK **/
void Mesh_Setup(void) {
  mesh.setDebugMsgTypes( ERROR | STARTUP );  // set before init() so that you can see startup messages

  // Channel set to 6. Make sure to use the same channel for your mesh and for you other
  // network (STATION_SSID)
  mesh.init( MESH_PREFIX, MESH_PASSWORD, MESH_PORT, WIFI_AP_STA);
  mesh.onReceive(&receivedCallback);

  // Bridge node, should (in most cases) be a root node. See [the wiki](https://gitlab.com/painlessMesh/painlessMesh/wikis/Possible-challenges-in-mesh-formation) for some background
  mesh.setRoot(true);
  // This node and all other nodes should ideally know the mesh contains a root, so call this on all nodes
  mesh.setContainsRoot(true);
}

void Mesh_Update(void){
  mesh.update();
  processCall();
}
