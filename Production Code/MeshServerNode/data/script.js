/* Website code! */
/* Code from the websocket! https://randomnerdtutorials.com/esp32-websocket-server-sensor/ */
var gateway = `ws://${window.location.hostname}/ws`;
var websocket;

// Init web socket when the page loads
window.addEventListener('load', onload);

function onload(event) {
    initWebSocket();
}

function sendNames(){
    var names = "";
    for(var i = 1; i <= 7; i++){
        names += document.getElementById("name"+i+"H").innerHTML+",";
        names += document.getElementById("name"+i+"A").innerHTML+",";
    }
    console.log("Sending names: "+names)
    websocket.send(names);
}

function getNames(){
    websocket.send("getNames");
    console.log('GetNames from websocket!');
}

function getStats(){
    websocket.send("getStats");
    console.log('GetStats from websocket!');
}

function initWebSocket() {
    console.log('Trying to open a WebSocket connection...');
    websocket = new WebSocket(gateway);
    websocket.onopen = onOpen;
    websocket.onclose = onClose;
    websocket.onmessage = onMessage;
    
    // every 1s examine the socket and send more data
    // only if all the existing data was sent out
    setInterval(() => {
        if (websocket.readyState == WebSocket.OPEN && websocket.bufferedAmount == 0) {
            getStats();
        }
    }, 2000);
}

// When websocket is established, call the getStats() function
function onOpen(event) {
    console.log('Connection opened');
    console.log('Trying to get readings');
    getStats();
    console.log('Trying to get names');
    getNames();
}

function onClose(event) {
    console.log('Connection closed');
    setTimeout(initWebSocket, 2000);
}

// Function that receives the message from the ESP32 with the readings
function onMessage(event) {
    console.log(event.data);
    var myObj = JSON.parse(event.data);
    var keys = Object.keys(myObj);

    // TODO: for the sets, add variable circles! Lit up?
    for (var i = 0; i < keys.length; i++){
        var key = keys[i];
        if(key.startsWith("set")){
            document.getElementById(key).innerHTML 
                = '<span class="set won_set"></span>'.repeat(2-parseInt(myObj[key]))
                +'<span class="set"></span>'.repeat(parseInt(myObj[key]));
        } else {
            document.getElementById(key).innerHTML = myObj[key];
        }
    }
}