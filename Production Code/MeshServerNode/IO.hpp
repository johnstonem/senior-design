#ifndef __IO_HPP__
#define __IO_HPP__

/**
 * Buttons are numbered from top right and then clockwise
*/
#define BTN1 36 // BTN1 on pin19 with a 3.3k pullup to 5V
#define BTN2 39 // BTN2 on pin18 with a 3.3k pullup to 5V
#define BTN3 34 // BTN3 on pin17 with a 3.3k pullup to 5V
#define BTN4 35 // BTN4 on pin16 with a 3.3k pullup to 5V
#define BTN5 5 // BTN5 on pin15 with a 3.3k pullup to 5V
#define BTN6 17 // BTN6 on pin14 with a 3.3k pullup to 5V
#define BTN7 16 // BTN7 on pin13 with a 3.3k pullup to 5V

#define SEG1_BIT1 4
#define SEG1_BIT2 2
#define SEG1_BIT3 15
#define SEG1_BIT4 13

#define SEG2_BIT1 12
#define SEG2_BIT2 14
#define SEG2_BIT3 27
#define SEG2_BIT4 26

#define L_MUL 18
#define L_SET_LED_1 32
#define L_SET_LED_2 33
#define L_HOME 25

#define R_MUL 19
#define R_SET_LED_1 22
#define R_SET_LED_2 21
#define R_HOME 23

void update_pin_out(void);
void IO_Setup(void);
void ISR_BTN1_PRESSED(void);

#endif
