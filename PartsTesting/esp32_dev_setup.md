# IDE Setup
[Default Arduino IDE Setup](https://www.arduino.cc/en/software)

[Install ESP32 on Arduino](https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/)

[Setting up Arduino with Git](https://thenewstack.io/tutorial-git-an-arduino-ide-workflow/)

Note: We are setting up using the "ESP32 FM DevKit" sketch board.

# Seting up MicroPython with Arduino (ESP32 specifically)
[Micropython setup](https://docs.arduino.cc/micropython/)

[Micropython ESP32 Setup](https://docs.micropython.org/en/latest/esp32/tutorial/intro.html)

* We will be using the "stable firmware build" of Micropython

* We ran "pip install esptool" to download the latest tool build

# Setting up the ESP32 Mesh Network
[Painless ESP32 Mesh Tutorial](https://randomnerdtutorials.com/esp-mesh-esp32-esp8266-painlessmesh/)

