# Senior Design

This is the Gitlab Repository for the Senior Design project.
Details are pending on the exact focus / description.

The details on the Senior Design Schedule for the fall is [here](https://gitlab.com/msoe.edu/eecs/seniordesign/ce/-/wikis/Fall-Semester)
:)

## Members

- Ryan Beatty
- John Paul Bunn
- Mitchell Johnstone
- Charles Schaefer
- Tyler Togstad

---------------------------