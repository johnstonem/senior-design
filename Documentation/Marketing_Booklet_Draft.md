# Automated Tennis Score Tracking

## Team Members:
- Ryan Beatty (CE)
- John Paul Bunn (CE, CS)
- Mitchell Johnstone (CE, CS)
- Charles Schaefer (CE)
- Tyler Togstad (CE)

## Faculty Advisors:
- Dr. Durant

## Description:
In collaboration with the local Brown Deer High School tennis teams, the senior design project focuses on developing a tennis scoreboard system. The system consists of individual scoreboards with custom-designed PCBs connected via a mesh network using ESP32s, allowing for seamless score communication between scoreboards. Additionally, the scoreboards hosts a website that aggregates the sent data, providing coaches with a comprehensive view of ongoing games. This digital solution aims to reduce human error in scoring by providing a reliable, automated scoring system that tracks the games' histories, ensuring a tailored and efficient solution for tennis scorekeeping. This project showcases our expertise in electronics, hardware, networking, and software development