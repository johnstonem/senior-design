# System Requirements Specification
## Automated Tennis Scoring Project
Senior Design 2023

Members: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Advisor: Dr. Durant

Date Submitted: 10/27/2023

Based on the [IEEE Std. 1233](https://www2.seas.gwu.edu/~mlancast/cs254/IEE_STD_1233-_Requirements_Spec.pdf)

## Table of contents
1. [Introduction](#1-introduction)
2. [General System Description](#2-general-system-description)
3. [System capabilities, conditions, and constraints](#3-system-capabilities-conditions-and-constraints)
4. [System interfaces](#4-system-interfaces)

## List of Figures

## 1. Introduction
The Brown Deer Tennis coach, Jim Matousek, requested an update to the tennis scoring system used for the team's tennis matches. Before, the team had an analog scoreboard system that was posted at every court which would keep track of the current game status. The students playing the matches would keep track of the scores, and then the coach would collect the results afterwards. This put a strain on the student by making them remember the results. As well, the coach was unable to keep up with the state of every court.

The proposed system will replace the outdated flip-card scoring. 

### 1.1 System purpose
The system primarily is a means of enhancing the coach's observation over multiple concurrent tennis matches and recording the results of the individual matches while displaying the state of distinct tennis games. 

Currently the players are responsible for knowing the current score of the game, but the coach must collect final results to log to an external site, [tennisreporting.com](https://tennisreporting.com/), as it has become an interscholastic mandated app for normalizing reporting. In addition, the coach can't keep up with every court when he's constantly moving between games.

The system must provide a means of concurrently viewing the state of all games for the coach to stay up to date with each game. 
The system must assist in recording the final score for each match, so the coach can log it and the students don't have to remember.
Lastly, the system must still display the current score to the players and spectators.

### 1.2 System scope
Our senior design project will produce a tennis scoring system. 

The tennis coach needs an electronic replacement of the traditional tennis score systems. To maintain the visibility of the current score, the system will provide an LED display of the current state of the game. By providing communication between systems posted at each court, the system should be able to collect the current state of all games happening and provide a user interface so the coach can stay up-to-date with the matches without having to travel to all games. To reduce to the load on students, the scores will be stored so the coach can log it without having to ask them.

By providing all above implementations, the coach will have a centralized location to view information about all the games, the students won't have to remember each game's score for future logging, and the current game score will be displayed more vividly to all spectators / players.

### 1.3 Definitions, acronyms, and abbreviations
- MSOE: Milwaukee School of Engineering

### 1.4 References
- [tennisreporting.com](https://tennisreporting.com/)

### 1.5 System overview
The system produced will be a scoreboard that can be posted at each court. An LED display will be incorporated in the design to display the current game score, with distinct LED displays on two halves of one side to display the score for the home and away players respectively.

Each unit will communicate with the others, providing concurrency of information across courts. 

The units will also provide a simple interface for the coach to connect to that will contain all relevant data on each game played during the tournament.

## 2. General System Description
The system will generally consist of a few separate components. The three main components are the scoreboards, the main hub device, and the connection between the devices. The scoreboards will keep score of the current set and show how many sets each side has won. There will be different buttons on the scoreboard which are intended to be operated by the athletes. These buttons will control the scoreboard displays (Score, Sets Won). The second component of the system is the central hub device. This device is used to keep track of the scores and possibly be viewable by the coach. Currently, there are different methods for implementing this component; however, there is no ultimate decision to how this idea will be implemented in the final rendition of the system. The final component of the product is a non-physical component which is the wireless connection between the prior two components. Some ideas for the wireless connection include bluetooth, a single wifi access point, or a mesh network among the scoreboards and central hub device.

### 2.1 System Context
_Scoreboard User Interface:_

Allows tennis players and operators to interact with the scoreboard for score updates and match management.

_Wireless Communication Interface:_

The wireless communication enables scoreboards to synchronize game scores and transmit data to the central monitoring system.

_Mobile Device Interface:_

This will provides remote access to match scores, allowing users to monitor games from their smartphones or tablets.

### 2.2 System Modes and States
_Game in Progress:_

The system should accurately update and display game 
scores as long as a tennis match is in progress on a 
court.

_End of Set:_

When a set concludes, the system must transition to a 
new set and reset the set scores while maintaining 
game scores.

_Match Completion:_

Upon the conclusion of a tennis match, the system 
should display final match scores and potentially 
indicate the winner.

_Charging_

When the portable battery runs out of power, the device must recharge. The device must be plugged to recharge the battery for further function.

### 2.3 Major system capabilities
_User Interface and Interaction:_
- Button interfaces for tennis players to update scores.
    - Buttons to increase and decrease the game score for each player.
    - Buttons to increase and decrease the set score for each player.
    - Buttons to end the match.
- LED display for showing current game status.
- User-friendly design for spectators and scoreboard operators.

_Scoring and Game Management:_
- Real-time score tracking and updates.
- Accurate display of sets and match status.

_Wireless Communication:_
- Establishing and maintaining wireless connections with data synchronization.

_Power and Battery Management:_
- Rechargeable battery
- Must produce a 1A current for up to 8 hours.

_Data Display and Accessibility:_
- Visibility and readability of the LED display.
- Integration with a mobile device for remote data access.

### 2.4 Major system constraints
_Hardware Constraints:_

The system must operate within the limitations of the 
hardware components, including the LED display, 
buttons, and wireless communication modules.

_Power Constraints:_

The system is constrained by the battery capacity and 
must operate efficiently to maximize battery life 
during matches.

_Safety Compliance:_

This system will be interacting with humans, so it 
also must be safe to interact with. The system must 
comply with relevant regulations, such as 
electromagnetic interference (EMI) standards and 
safety certifications.

### 2.5 User characteristics
_Tennis Players:_

- Function: They use the scoreboard to update and view the current score of their tennis game.
- Location: At each tennis court where the scoreboard is installed.
- Type of Device: The scoreboard has physical buttons for tennis players to press.
- Number: Typically, there will be two tennis players per court.

_Spectators:_

- Function: Spectators watch the tennis matches and may also refer to the scoreboard to check the current game status.
- Location: At the tennis court where the matches are taking place.
- Type of Device: No specific device, although they may use the electronic scoreboard for reference.
- Number: The number of spectators can vary but may include several individuals per court.⌈

_Coach:_

- Function: They use a mobile app to wirelessly connect to the scoreboard system and access the current game status of all courts.
- Location: Anywhere with network access.
- Type of Device: Smartphones or tablets with the mobile app installed.
- Number: one coach per team.

### 2.6 Assumptions and dependencies

_Battery Performance:_ 

The system's operation depends on the reliable performance of rechargeable batteries. Any issues with battery degradation or failure can impact system uptime.

_User Input:_

The system depends on tennis players pressing the correct buttons to update scores. Incorrect inputs can lead to inaccurate score display. As the system is unable to determine the correctness of the score, it is up to the users to catch mistakes and remedy them.

_Mobile Access Functionality:_

The communication to the device used for remote monitoring depends on the compatibility with the scoreboard system and the reliability of mobile networks.

### 2.7 Operational scenarios

_Example: Updating Set Score_

Scenario: A tennis match was won by one player.
Use Case: After the match concludes, the player presses the corresponding button on the scoreboard. The system updates the score, displaying "1-0".

_Example: Changing Sets_

Scenario: A tennis match between two players is progressing to a new set.
Use Case: After the first set concludes, the scoreboard updates to the next set automatically. The system resets the set scores to "0-0" while keeping track of the game scores.

_Example: Remote Monitoring via Mobile App_

Scenario: Coach Matousek is not present at the court but wants to monitor a player's progress.
Use Case: Coach Matousek uses the mobile app to connect to the scoreboard system wirelessly. He can see real-time updates of the set and match scores on his smartphone, even when he's not physically present at the court.

_Example: Mistake in Scoring_

Scenario: A player on the field scores a point, but presses the incorrect button (decreasing instead of increasing). The mistake is caught.
Use Case: The scoreboard will display the incorrect score until a correction is made. The system is allowed to be adjusted up and down at any point, so when the mistake is found, the correct adjustment is made by the tennis player (increase to fix the mistake), and then the correct input is inputted (increase again).

## 3. System capabilities, conditions, and constraints
NOTE—System behavior, exception handling, manufacturability, and deployment should be covered under each capability, condition, and constraint.
### 3.1 Physical
#### 3.1.1 Construction - Tyler
The device will be a water tight unit where dirt and debris will not be able to easily enter it. It will have a durable exterior shell and will be built to withstand a decent amount of bumping round. The chassis will be held together using screws, standoffs, and a wiring harness between the different boards contained within it. The battery will be secured to the chassis as well. 
#### 3.1.2 Durability
The product is not required to be considerably durable. Tennis matches are played in fairly nice weather when compared to other sports. The devices should be moderately lightweight and be able to take a hit from a tennis ball. The devices could also be scratch-resistant and have the potential to withstand being dropped from a short distance. 
#### 3.1.3 Adaptability - Tyler
There will be little adaptability within the system. More boards for the displays can be assembled, but they will not be off-the-shelf parts. The most adaptable part of the system would be the battery, since the chassis will have some extra room for a potentially larger battery. 
#### 3.1.4 Environmental conditions
The system will be used during tennis matches held outdoors, so the system will be exposed to natural environment conditions, such as wind and the sun. The system will have to be wind-resistant and visible through sun or shade. Tennis matches will not be played during rain-weather or more extreme natural conditions, so the system need not be waterproof.

During transportation, the system will feel the effects of vehicle riding or hand carrying, which will include some motion. During operation in tennis matches the system will not be moved.

The tennis matches will be watched by spectators, so there is a potential for some signal interference from spectator electronic devices.

### 3.2 System performance characteristics
The devices should be visible from a moderate distance with quick device-to-device communication. The utmost use of our product will be about eight hours on a given day, such as in large tournament events. The full utilization of the device should be maximum eight hours. 

### 3.3 System security
Wireless communication is reliant on passcodes, which should not be exposed to the customer. Accessing the data stored in the device should require an additional passcode to prevent data exposure. Sensitive data ("individual athlete" data) should only be accessible to the customer. A log of access attempts should be maintained to track malicious behavior.

A distributed log of local data should be maintained in the case that data is breached and modified.

### 3.4 Information management
The device will locally maintain a registry of data, storing a history of device interactions, such as changes to the score. This data should be replicated to maintain reliability.

### 3.5 System operations
The system operations of the electronic tennis scoreboards include initializing the system, starting games, updating scores via physical buttons, transitioning between sets, resetting sets, and displaying match results. The system maintains wireless communication for real-time data synchronization and displays the scores visibly using an LED display. Remote monitoring via a mobile device allows coaches to access all games simultaneously. 

#### 3.5.1 System human factors
The main human error could come from the physical interaction with the board. Because the tennis players or a designated person will update the game progress when a set is won, there will be significant dependence on their input being correct.

Because the interface for the score display is visible to all parties, there should be reliability in the error, as people will be able to fact check the scores. However, the system's accuracy still depends on the accuracy of the input via the push-buttons.

#### 3.5.2 System maintainability
Once the product is developed and handed off to the client (Jim Matousek), the expectation is that the product is maintainable by him with minimal experience needed. 

Only commonly used and well stocked parts should be used in the design so that replacements to the system are easily accessible. Each product should have a bill of materials with links to order so that the replacement parts are easily ordered.

In the case of a fault in the PCB or electrical components, a stored schematic should be maintained so that a replacement can be ordered and assembled. A spare should be ordered and constructed so at least 1 board is able to be replaced with minimal down-time of the overall system.

#### 3.5.3 System reliability
Maintenance involved is the charging of the rechargable battery, and should last approximately 8 hours at minimum. 

Repair should, on average, take about one hour.

The system should provide consistent and dependable system-output, data maintainability, and results.

### 3.6 Policy and regulation - 
The only policies and regulation from IEEE and the FCC we have to take into consideration are in regards to our intentianal emitting device. Currently the plan is to use the 802.11B Wifi standard on a 
2.4ghz band to communicate between devices. According to IEEE the 802.11b standard was created shortly after 802.11 went obsolete in 1999. This standard has a maximum theoretical data rate of 11mb/s on 
a 2.4ghz band. 

We will be using a point to multipoint connection on a 2.4 ghz band, and the FCC has rules and regulations for any emitting device. They provided a table for limits we can't exceed for the maximum power of 
our intential radiotor, although the absalute power limit for our device is 1 watt, or 30dBm (We will not be getting anywhere close to this value). The EIRP limit remains constant at 4 watts or 36dbm for each 
power range, but the maximum antenna gain does vary on our power range. The gain ranges from 6dBi at 1 watt to 24dBi at 16mW.


## 4. System interfaces - 





concept drawings and proposed user interfaces
