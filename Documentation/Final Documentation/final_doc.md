# Tennis Scoring System
_CPE4901 Senior Design_

## Documentation Date -  5/3/2024

## Team
- Charles Schaefer - Computer Engineering
- Tyler Togstad - Computer Engineering
- Ryan Beatty - Computer Engineering
- Mitchell Johnstone - Computer Engineering/Computer Science
- John Paul Bunn - Computer Engineering/Computer Science

## Abstract
The automated tennis scoring system project aims to replace the existing manual scoring system at Brown Deer High School with electronic scoreboards, enhancing the accuracy and efficiency of score tracking. The system will feature real-time score updates, user-friendly interfaces, and durable construction suitable for outdoor use. Additionally, it will enable remote monitoring of match scores and integration with an external site for match result logging. This report provides a detailed overview of the project's objectives, design, testing, and budget considerations.

## Table of Contents
- [Abstract](#abstract)
- [Introduction](#introduction)
- [Problem statement / objectives](#problem-statement--objectives)
- [Ethical considerations](#ethical-considerations)
- [Requirements / specifications](#requirements--specifications)
    - [Functional requirements](#functional-requirements)
    - [Non-functional requirements](#non-functional-requirements)
- [Design documentation](#design-documentation)
- [Testing of software and hardware](#testing-of-software-and-hardware)
- [Budget](#budget)
    - [Engineering effort](#engineering-effort)
    - [BoM/cost of parts](#bomcost-of-parts)
- [User's manual](#users-manual)
- [Appendices](#appendices)
    - [Key pages from key data sheets](#key-pages-from-key-data-sheets)
    - [Overview of structure of repository](#overview-of-structure-of-repository)
    - [Configuration of build tools](#configuration-of-build-tools)

## Introduction
The report details the development of an automated tennis scoring system to replace the manual scoring system at Brown Deer High School. The system includes electronic scoreboards with real-time communication capabilities, user-friendly interfaces, and durability suitable for outdoor use.

The report is structured into several sections, including the abstract, introduction, problem statement/objectives, requirements/specifications, design documentation, testing, budget, user's manual, and appendices.

- The abstract provides a concise summary of the project, outlining its objectives and scope. The introduction expands on this, discussing the current inefficiencies of the manual scoring system and the goals of the automated system.

- The problem statement/objectives section outlines the specific objectives of the project, such as developing an automated scoring system, enabling real-time communication, and enhancing visibility and clarity of score displays.

- The requirements/specifications section details the functional and non-functional requirements of the system, including user interface, scoring and game management, wireless communication, and system security.

- The design documentation provides an in-depth look at the hardware and software design of the system, including alternative designs considered and diagrams of the system components.

- The testing section discusses the testing methodologies used to validate the system's performance, ensuring functionality and reliability.

- The budget section outlines the cost of parts for the system, including a breakdown of costs for the prototype and first production run.

- The user's manual provides detailed instructions on how to operate the system, aimed at players, coaches, and spectators. 

- The appendices include additional information, such as an overview of the repository structure.

## Problem statement / objectives
The current tennis scoring system at Brown Deer High School is inefficient and outdated, relying on analog scoreboards and manual score tracking. This system places a burden on both players, who must remember scores, and the coach, who struggles to keep track of multiple matches simultaneously. The lack of a centralized scoring system leads to inconsistencies in score reporting and increases the likelihood of errors. Additionally, the reliance on manual score tracking detracts from the overall spectator experience, as it can be challenging for spectators to accurately follow the progression of a match.

The objectives of this project then follow:

1. Develop an Automated Scoring System: Our primary objective is to develop an automated tennis scoring system that replaces the existing flip-card scoring with electronic scoreboards at each court. These scoreboards will provide real-time score updates and eliminate the need for manual score tracking.
2. Enable Real-time Communication: The system will enable real-time communication between scoreboards, allowing for a centralized view of all match scores for the coach. This will enhance the coach's ability to observe and manage multiple matches simultaneously.
3. User-friendly Interface: The system will feature a user-friendly interface for tennis players to update scores and for spectators to view the current game status. This will improve the overall tennis match experience for both players and spectators.
4. Enhance Visibility and Clarity: The system will enhance the visibility and clarity of score displays for players and spectators. This will improve the overall spectator experience and make it easier for players to track the progress of a match.
5. Ensure Durability and Adaptability: The system will be designed to be durable and adaptable to varying environmental conditions. It will be able to withstand impact from tennis balls and be resistant to water and dust.
6. Maintain System Security: The system will maintain security by implementing access controls and encryption methods to protect user data. This will ensure that only authorized individuals have access to sensitive information.
7. Enable Remote Monitoring: The system will enable remote monitoring of match scores via a mobile app for the coach. This will allow the coach to stay informed about match progress even when not physically present at the court.
8. Integrate with External Site: The system will 
integrate with an external site, tennisreporting.com, to 
facilitate the logging of match results as required by 
interscholastic mandates. This will streamline the 
reporting process and ensure compliance with regulations.

Of these objectives, the fully completed ones are objectives 1, 2, 5, and 7 reliably and to our understanding. Objectives 3 and 4 are a bit subjective, but we believe we've met the standard that the prior solution had. Objectives 6 and 8 are not yet implemented, as there is very little security other than maintaining a secure password for the network, and there is no current integration with tennisreporting.com.

## Ethical considerations
Our electronic tennis scoring system for Brown Deer High School is designed with a strong focus on ethical considerations, ensuring that the development and deployment of the system adhere to the highest standards of integrity and responsibility. 

- “to hold paramount the safety, health, and welfare of the public, to strive to comply with ethical design and sustainable development practices, to protect the privacy of others, and to disclose promptly factors that might endanger the public or the environment;” - 7.8 IEEE Code of Ethics, point II-1

Privacy is a significant ethical concern addressed by our system. We have implemented stringent access controls and encryption methods to protect users' data. Only authorized individuals will have access to sensitive information, ensuring that privacy is maintained at all times. Additionally, our system is designed to comply with relevant data protection regulations to further safeguard users' privacy.

Safety is another critical ethical issue that our system effectively addresses. We conduct thorough safety validations and hazard testing throughout the development process to identify and mitigate any potential risks. This ensures that our technology poses no harm to users or the environment, aligning with our commitment to prioritize the safety and well-being of the public.

- “to seek, accept, and offer honest criticism of technical work, to acknowledge and correct errors, to be honest and realistic in stating claims or estimates based on available data, and to credit properly the contributions of others;” - 7.8 IEEE Code of Ethics, point II-5

Transparency and accountability are core principles guiding our development process. We maintain detailed documentation of all decisions and actions taken, ensuring that our process is transparent and accountable. This includes promptly identifying and disclosing any potential risks to the public or the environment, fostering a responsible and transparent development approach.

- “to treat all persons fairly and with respect, and to not engage in discrimination based on characteristics such as race, religion, gender, disability, age, national origin, sexual orientation, gender identity, or gender expression;” - 7.8 IEEE Code of Ethics, point II-7

Furthermore, our system is designed to be fair and inclusive. We do not discriminate based on characteristics such as race, religion, gender, or disability. Our diverse team ensures that different perspectives are considered in the development process, leading to a more inclusive and equitable system. Additionally, the system is designed to be accessible and user-friendly for individuals of all backgrounds, further supporting inclusivity.

In conclusion, our electronic tennis scoring system for Brown Deer High School exemplifies our commitment to ethical principles. By prioritizing privacy, safety, transparency, accountability, fairness, and inclusivity, we ensure that our system is developed and deployed in an ethical and responsible manner, benefiting both the school and its users.

## Requirements / specifications
### Functional requirements
The system for the electronic tennis scoreboard has several key modes and states:

- Game in Progress: Accurate updating and display of game scores during a match.
- End of Set: Transitioning to a new set, resetting set scores while maintaining game scores.
- Match Completion: Displaying final match scores and potentially indicating the winner.
- Charging: Recharging the portable battery when it runs out of power. 
- Power and Battery Management: Rechargeable 
battery producing 1A current for up to 8 hours.

As for the working functional requirements, we have completed all of the requirements listed. There may need to be additional updates to the functionality in the future; however, each of the requirements are implemented within the current iteration of the system.

### Non-functional requirements
- User Interface and Interaction: Button interfaces 
for players to update scores, LED display for 
showing game status, and user-friendly design.
- Scoring and Game Management: Real-time score 
tracking and accurate display of sets and match 
status.
- Wireless Communication: Establishing and 
maintaining wireless connections for data 
synchronization.
- Data Display and Accessibility: Visible and 
readable LED display, integration with mobile 
devices for remote access.

User characteristics include the need for the device to be water-tight, durable, moderately lightweight, and able to withstand typical tennis match conditions. The system will operate outdoors and may experience signal interference from spectator electronic devices.

System security involves using passcodes for wireless communication and data access, with sensitive data accessible only to authorized users. A log of access attempts should be maintained.

Information management includes locally storing a registry of data to track interactions, with data replication for reliability.

Human factors such as input accuracy and system maintainability by the client are important considerations. The system should be easily maintainable with commonly used and well-stocked parts, and spare parts should be available to minimize downtime.

As for the non-functional requirements, the current version of the system implements all of the requirements but needs work on the readable LED display portion. Better diffusion film should be used and placed further from the LED to create more visible indicators.

## Design documentation 

In-depth documentation for the hardware and software is provided in the below documents:
- [Hardware Documentation](./Hardware.md)
- [Software Documentation](../Engineering%20Documents/software_eng_doc.md)

Overall block diagram: 
![Block Diagram](block_diagram.png)

3D printed enclosure diagram:
![3D printed enclosure](enclosure.png)

### Alternative Designs
#### Analog/Physical Scoreboard
- Description: Physical cards with numbers to represent sets and games won.
- Pros: Simple, no technology needed, easy to understand, mobile.
- Cons: Outdated, not easily viewable from a distance.
- Value: Simple and easy to maintain, but does not address issues of human error and visibility.

#### Digital Scoreboard
- Description: Similar to analog but in digital form, showing more information.
- Pros: Simple, can provide more information, better visibility, easy to understand.
- Cons: Requires power source, can be costly.
- Value: Modern and customizable but can be more expensive and may not address all visibility issues.

#### Mobile/Phone Scoreboard App
- Description: Mobile app for tracking scores and communicating.
- Pros: Mobile, easy to use, good for communication.
- Cons: Not viewable to large crowds, designed for individual use.
- Value: Convenient for individuals but may not be suitable for large crowds or tournaments.

#### 8th Scoreboard (No HUB)
- Description: Each scoreboard acts as a hub, allowing coaches to view scores.
- Pros: Only one PCB and software needed, easy production and replacement.
- Cons: More complex construction, limited information display, increased overall price.

### HUB Display Options
- Display: Central hub for receiving and displaying scores.
- Onboard Screen: Displays scores from all courts but limited by size and power consumption.
- Connected Phone App: Displays scores on a phone, providing more information but requiring complex technology.

## Testing of software and hardware
### Software
- Each segment of the software was tested seperately and then added to the project. This includes:
    - Mesh System
    - Web Server
    - SPIFFS
    - Socket Connections
- Each was tested on the developed MCU board as well as the prototyped breadboard. 
- All components work properly and have been tested thoroughly.

### Hardware
- Initial prototyping and concepts proved effective with esp32 development boards and generic seven segment displays. 
- Further hardware testing was done on the MCU and display boards which includes: 
    - MCU Board
        - Debounce Chip Footprint Tolerance
        - Shorts to ground under LDO
        - Incorrect Footprint for ESP32
        - Untented Vias and Pads from many reworks
    - Display Board
        - Incorrect footprint for decoder
        - Faults at voltage levels larger than 6V
            - The decoder chip would not operate properly above the input voltage of 6V. Changing the voltage to 5V fixes the issue but is not meant with the original design. 
- Each board was tested for shorts to ground and other simple issues that would cause critical failure.

## Budget
### Engineering effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--     |--     |--     |--     |--     |
|W1         |2 hrs  |2 hrs  |2 hrs  |8 hrs  |3 hrs  |
|W2         |2 hrs  |4 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W3         |1 hrs  |5 hrs  |3 hrs  |5 hrs  |5 hrs  |
|W4         |3 hrs  |3 hrs  |2 hrs  |6 hrs  |3 hrs  |
|W5         |2 hrs  |1 hr   |       |1 hr   |2 hr   |
|W6         |3 hrs  |3 hrs  |4 hrs  |5 hrs  |3 hrs  |
|W7         |2 hrs  |5 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W8         |2 hrs  |2 hrs  |6 hrs  |4 hrs  |4 hrs  |
|W9         |4 hrs  |5 hrs  |11 hrs |7 hrs  |3 hrs  |
|W10        |3 hrs  |5 hrs  |2 hrs  |3 hrs  |4 hrs  |
|W11        |6 hrs  |4 hrs  |7 hrs  |5 hrs  |5 hrs  |
|W12        |3 hrs  |2 hrs  |3 hrs  |3 hrs  |2 hrs  |
|W13        |<td>Thanksgiving Break</td>
|W14        |5 hrs  |5 hrs  |       |5 hrs  |5 hrs  |
|W15        |3 hrs  |3 hrs  |3 hrs  |3 hors |3 hrs  |
|W1         |4 hrs  |4 hrs  |4 hrs  |8 hrs  |3 hrs  |
|W2         |6 hrs  |4 hrs  |5 hrs  |6 hrs  |6 hrs  |
|W3         |3 hrs  |2 hrs  |5 hrs  |6 hrs  |7 hrs  |
|W4         |1 hr   |15 hrs |6 hrs  |20 hrs |6 hrs  |
|W5         |Sick   |3 hrs  |4 hrs  |4 hrs  |8 hrs  |  
|W6         |Sick   |4 hrs  |       |4 hrs  |4 hrs  |  
|W7         |4 hrs  |8 hrs  |8 hrs  |13 hrs |6 hrs  |
|W8         |6 hrs  |3 hrs  |2 hrs  |10 hrs |2 hrs  |
|W9         |5 hrs  |5 hrs  |6 hrs  |6 hrs  |4 hrs  |
|W10        |6 hrs  |2 hrs  |3 hrs  |8 hrs  |6 hrs  |
|W11        |3 hrs  |3 hrs  |3 hrs  |4 hrs  |3 hrs  |
|W12        |       |       |4 hrs  |       |6 hrs  |
|W13        |2 hrs  |3 hrs  |6 hrs  |4 hrs  |5 hrs  |
|W14        |4 hrs  |6 hrs  |4 hrs  |6 hrs  |7 hrs  |
|W15        |4 hrs  |8 hrs  |4 hrs  |8 hrs  |7 hrs  |
|**Total:** |89 hrs |119 hrs|115 hrs|174 hrs|128 hrs|

### BoM/cost of parts - Tyler
(perhaps in quantity 1 for your prototype and a reasonable first production run such as 500 devices, depending on your market)

Harness Cost
|Qty|Part Description             |Vendor|Vendor Part #                                |Cost Each|Total Cost|
|---|-----------------------------|------|---------------------------------------------|---------|----------|
|1  |Lithium Ion Battery Pack - 10Ah (3A/1A USB Ports)|Sparkfun|Power Bank M108A                             | $21.50  | $21.50   |
|1  |(5 Pack) JacobsParts 5V Fixed Voltage USB Type-A QC 2.0 3.0 DC Trigger Module Board|Amazon|POWTRIG-A-12V-5PK                            | $8.99   | $8.99    |
|1  |omnidirectional antenna      |Mouser|214414-0001                                  | $3.77   | $3.77    |
|1  |antenna extension cable      |Mouser|CSH-SGFB-100-UFFR                            | $5.84   | $5.84    |
|7  |Off-mom pushbutton           |Digikey|PB1973BBLKBLKEF0                             | $2.30   | $16.10   |
|1  |rocker switch                |Digikey|RB141C1100-214                               | $1.35   | $1.35    |
|6  |wire harness holders         |Digikey|3240707                                      | $0.10   | $0.60    |
|1  |1.5 ft ribbon cable          |Digikey|M3DDA-1618J                                  | $5.55   | $5.55    |
|1  |panel mount m to f usb-c     |Digikey|CAB-15455                                    | $9.69   | $9.69    |
|1  |2A mini blade fuse           |Digikey|0297002.WXNV                                 | $0.35   | $0.35    |
|1  |mini blade inline fuse holder|Digikey|01530002H                                    | $2.00   | $2.00    |
|3  |0.25 QC crimp right angle    |Digikey|2-520128-2                                   | $0.41   | $1.23    |
|14 |CONN QC RCPT 18-22AWG 0.187  |Digikey|2-520181-2                                   | $0.22   | $3.08    |
|   |                             |      |                                             |TOTAL:   | $80.05   |


MCU Board Cost
|Qty|Part Description             |Vendor|Vendor Part #                                |Cost Each|Total Cost|
|---|-----------------------------|------|---------------------------------------------|---------|----------|
|1  |MCU PCB                      |JLC PCB|x                                            | $0.50   | $0.50    |
|1  |controller                   |Digikey|ESP32-WROOM-32UE-N4                          | $2.50   | $2.50    |
|1  |USB interface                |Digikey|FT231XS-R                                    | $2.34   | $2.34    |
|1  |V Reg                        |Digikey|NCV1117DT33T5G                               | $0.71   | $0.71    |
|2  |transistor                   |Digikey|MMBT3904_R1_00001                            | $0.14   | $0.28    |
|2  |47 uf cap                    |Digikey|865230445005                                 | $0.33   | $0.66    |
|1  |16-pin shrouded              |Digikey|61201621621                                  | $0.63   | $0.63    |
|1  |IC debouncer                 |Digikey|MAX6818EAP+T                                 | $7.40   | $7.40    |
|1  |0805 Grn LED                 |Digikey|LTST-C171GKT                                 | $0.26   | $0.26    |
|1  |0805 Red LED                 |Digikey|LTST-C171KRKT                                | $0.28   | $0.28    |
|1  |0805 Orng LED                |Digikey|LTST-C170KFKT                                | $0.28   | $0.28    |
|3  |0805 330 ohm                 |Digikey|RMCF0805JT330R                               | $0.10   | $0.30    |
|3  |0805 10K ohm                 |Digikey|RMCF0805FT10K0                               | $0.10   | $0.30    |
|2  |0805 27 ohm                  |Digikey|RMCF0805FT27R0                               | $0.10   | $0.20    |
|2  |0805 47pf cap                |Digikey|C0805C470J5GAC7210                           | $0.10   | $0.20    |
|8  |0805 0.1 uf cap              |Digikey|C0805C104M5RAC7800                           | $0.10   | $0.80    |
|2  |0805 10 uf cap               |Digikey|CL21A106KOQNNNE                              | $0.10   | $0.20    |
|1  |0805 4.7 uf cap              |Digikey|CL21A475KAQNNNE                              | $0.10   | $0.10    |
|9  |2 pos terminal block         |Digikey|TB006-508-02BE                               | $0.73   | $6.57    |
|   |                             |      |                                             |TOTAL:   | $24.51   |


Display Board Cost
|Qty|Part Description             |Vendor|Vendor Part #                                |Cost Each|Total Cost|
|---|-----------------------------|------|---------------------------------------------|---------|----------|
|1  |LED Array PCB                |JLC PCB|x                                            | $4.78   | $4.78    |
|2  |transistor array             |Digikey|ULN2004AD                                    | $1.03   | $2.06    |
|60 |surface mount LED            |Digikey|APT2012SURCK                                 | $0.10   | $6.18    |
|17 |diode array                  |Digikey|1SS308(TE85L,F                               | $0.29   | $4.93    |
|1  |decoder IC                   |Digikey|CD4028BM                                     | $1.06   | $1.06    |
|4  |1K resistor 0805             |Digikey|CRGH0805J1K0                                 | $0.05   | $0.20    |
|14 |330 resistor 0805            |Digikey|ERJ-P06J331V                                 | $0.04   | $0.56    |
|2  |16 pos header                |Digikey|61201621621                                  | $0.63   | $1.26    |
|2  |2 pos terminal block         |Digikey|TB006-508-02BE                               | $0.73   | $1.46    |
|   |                             |      |                                             |TOTAL:   | $22.49   |

Case Cost
|Qty|Part Description             |Vendor|Vendor Part #                                |Cost Each|Total Cost|
|---|-----------------------------|------|---------------------------------------------|---------|----------|
|1  |Clear Scratch- and UV-Resistant Cast Acrylic Sheet 12"x12"x 1/8"|McMaster-Carr|8560K239                                     | $10.00  | $10.00   |
|1  |PolyLite PETG Color: Black Diameter: 1.75mm Weight: 1kg|Polymaker|PB01011 - NOTE, may be cheaper on Amazon     | $21.99  | $21.99   |
|   |                             |      |                                             |TOTAL:   | $31.99   |

## User's manual

The user manual for the board can be found in our [Operations doc](../Engineering%20Documents/operations_doc.md)

## Appendices
### Overview of structure of repository
Each folder in the repo has a separate purpose / content designated to it.

|Folder name|Content|
|-|-|
|Documentation| Contains written material, including content written for all written reports, product owner meetings, tech concept investigations, even this final report document. This folder has the history of our progress, including detailed status reports.|
|ESP32 Arduino Code| This contains a lot of the developed code over the project lifetime. There's demo code, full code bits, test code for specific components.|
|Hardware|KiCad projects with the PCBs and Schematics for both the MCU and the Display boards, along with any needed gerbers for ordering.|
|Orders|Every purchase order that we ordered from this year, with a breakdown of cost and components, like a split up BOM for the entire project.|
|Parts Testing|Folder has some details on some parts that got tested, included LEDs and ESP32s.|
|Production Code|This is the latest code that's pushed to the demo boards. It has the most up to date libraries / firmware, and is used for final compilation / building for the demo.|

For the software specifically, there's almost always 2 separate code versions. This is evident by having 2 separate folders in each code folder:
- MeshServerNode - this hosts the web server that the coach would connect to, and is the root of the mesh. It records all of the other board's scores, listening across the mesh.
- MeshStandardNode - Keeps track of a single score (the one board), and sends messages across the connected mesh. Does not have a web server, but still operates the lights / buttons correctly independently.