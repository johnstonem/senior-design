# Tennis Scoring System - Hardware

## Documentation Date -  4/17/2024

## Team
- Charles Schaefer - Computer Engineering
- Tyler Togstad - Computer Engineering
- Ryan Beatty - Computer Engineering
- Mitchell Johnstone - Computer Engineering/Computer Science
- John Paul Bunn - Computer Engineering/Computer Science

## Table of Contents
- [1. Project Overview](#1-project-overview)
- [2. MCU Board](#2-mcu-board-tss_mcu_v10)
- [3. Display Board](#3-display-board-tss_disp_v3)
- [4. Battery](#4-battery)
- [5. Tennis Scoring System HLA](#5-tennis-scoring-system-hla)

## 1. Project Overview
The tennis scoring system is comprised of these main components: an MCU Board (TSS_MCU_V10), two Display Boards (TSS_DISP_V3), and the wiring harness at the HLA level. 

The MCU board houses the software that manages the mesh network, web server, the logic to drive the display boards, and receives button input from the user. 

The display board is a large seven segment display. The MCU board drives the display board inputs through the I/O header. The display board also has it's own multiplexing circuitry to decode what number it should display. 

The HLA outlines how the device is assembled and describes components that are needed for switching the power, interconnection between devies, safety considerations, antenna connection, and connection for user interface buttons.

## 2. MCU Board TSS_MCU_V10
### 2.1 MCU & I/O
Schematic:
![Schematic](MCU_S.png)

PCB:
![PCB](MCU_P.png)

The heart of the MCU board is the ESP32. It has on-board WiFi that we use to connect all of the scoreboards together in a mesh network. The ESP32-WROOM-32UE module serves our custom hardware solution well because it allows us to place our own omidirectional antenna outside the case for better interconnection between devices. 

In addition to the WiFi, the ESP32 also serves as the driver for the display boards. J2 is a 16 pin (2x8 layout) ribbon cable receptacle that acts as the main I/O header in the middle of the MCU board that goes to one of the display boards. The connected display board reroutes some of the inputs to an alternate configuration on an identical header used to go from display board to display board. This is how we are able to drive both display boards with one I/O header. 

Our custom implementation uses NO (normally open) momentary pushbutton switches (PB1973BBLKBLKEF0). The pushbuttons are connected to J5-J11 through the wiring harness. To avoid switch bounce, we've added a debouce IC at the hardware level. MAX6818EAP+T is the debounce we've used to achieve this. The device has 63kΩ pullup resistors built into each of the inputs, so there's no need to configure any pullups in the software. 

### 2.2 Power and Bulk Decoupling
The ESP32 requires 3.3V for it's supplied power. A QC trigger is able to request and ouptut 5V, which is connected to the MCU with a terminal block. Because the display boards also require 5V to power the LEDs, the MCU has an additional terminal block to connect power to both display boards. U2 is the LDO that is used to step this voltage down to 3.3V. NCP1117 is the LDO we are using. It has been implemented as described in its datasheet. There is a keepout zone under the LDO and connected 5V input/output terminals in the PCB design to avoid any potential shorts to ground due to it heating up and potentially reflowing solder onto a via. An added bonus of using this regulator is that our board will accept an input voltage up to 30V, although the logic on the display board only works up to 5V.

### 2.3 USB Interface
The display board has the capability to be programmed via the USB interface. We are using a USB Mini B port to achieve this. The FT231XS is the USB bridge to connect the device to a programming interface. For a production model, the female USB Mini B would not be installed. The port was used during bring up for the boards to easily program the device. If this board was to go into production, it would be programmed at the functional test level. We have provided TP3 and TP4 as test points to program the device through a flying probe fixture, or a bed of nails fixture.

### 2.4 Reset and Bootloader Control
Referencing the hardware design recommendations from Espressif [(esp-hardware-design-guidelines-en-master-esp32.pdf)](https://docs.espressif.com/projects/esp-hardware-design-guidelines/en/latest/esp32/esp-hardware-design-guidelines-en-master-esp32.pdf), the reset line must have an RC circuit placed on the CHIP_PU pin. In our implementation, we've used the NPN transistors as well as a pushbutton to manage the downloading of the data through USB, as well as the reset controls. This circuit is used to put the ESP32 into download mode. Viewing the development kit we used for prototyping, we can see a similar circuit used in the hardware schematic seen [here](https://dl.espressif.com/dl/schematics/SCH_ESP32-S3-DevKitC-1_V1.1_20220413.pdf).

### 2.5 PCB Guidelines
The PCB for this design is a two layer board. There are no real high frequency concerns for the board, as we are only using low freuency inputs and outputs. The ESP32-WROOM-32UE does not need a keepout for the antenna, as it is leaving the front face of the device.

The PCB has been laid out so that it is easy for a technician to create the wiring harness. All of the button I/O terminal blocks (J5-J11) are placed on the right side of the board. Input power and output power (J3 and J4) are placed in the top left of the board. The temporary programming interface is placed near the bottom left, and the main I/O header is directly in the middle of the board. 

### 2.6 FLA and Power Usage
Observed Full Load Amperage (FLA) for one MCU board is 0.16A with the current 5V input voltage. This value is subject to change based on input voltage, since the regulator will likely draw more current.

## 3. Display Board TSS_DISP_V3
Schematic:
![Schematic](DB_S.png)

PCB:
![PCB](DB_P.png)

The display board is a large, carefully arranged array of LEDs with some supporting circuitry. There are two main headers. The lower header is the input header, which reroutes some signals to the upper header as an output to the other board. Board #1 is directly connected to the MCU board through the lower header, while board #2 is connected to the leftover top connector from board #1 into the bottom header of board #2. The design was made so the same display board could be reused for both sides of the board.

The base circuit for the LEDs is very simple. The chosen LED (APT2012SURCK) has a color of hyper red and has a typical forward voltage of about 1.95V. For the base LED circuit, we have the power source with a 100 Ohm resistor below it. Then, from that node there is a network of four LEDs in parallel. These LEDs are then switched to ground to begin emitting light. 

### 3.1 Transistors (3.3V->5V)
The ESP32 has an nominal output voltage of 3.3V. We wanted to increase the voltage that the LEDs would be seeing, so we opted to add a transistor array (ULN2004A). This allows us to have the ESP32 outputs go high and switch the ground for the desired segment we are looking to turn on. An added bonus of having these transistors in the design, is that we can effectively power the board with an extremely large range of input voltages (up to 30V). 

### 3.2 Power Entry
The display board is equipped with two terminal blocks for power entry. One of them is primarily used for the board itself, while the other may be used to simplify the wiring harness. This second terminal block can be used to avoid having a long power wire going to the second board. The second terminal block does not need to be used, and it may not be added for a production unit.

### 3.3 I/O
Board 1 gets directly connected to the MCU board through the ribbon cable into J1. Board 1's J2 is then connected to board 2's J1. J2 is effectively just a combination of breakouts of the MCU board outputs.

### 3.4 Decoding
Our decoding uses a 4x10 multiplexer (CD4028BM) to decrease the number of required I/O to drive each segment. In combination with the multiplexer, we are also using diode arrays to isolate each number associated with a given input to the multiplexer. Using this circuitry takes the required I/O to drive a single board from seven down to four.

### 3.5 Set & Home LEDs
The set and home LEDs have a dedicated GPIO for turning them on and off. They still get grounded through the transistor, but they are not decoded through the CD4028BM. 

### 3.6 FLA and Power Usage
Observed FLA for one display board is 1.2 A with the current configuration at 5V. Further testing would need to be conducted to see the FLA at higher voltages.

## 4. Battery
The battery chosen for this project is a power bank typically used as a portable phone charger. This type of power source was chosen since it has a large capacity at a variety of voltages. It also has several safeties in place. The battery pack has short circuit protection, an on-board temperature monitoring system, and an on-board battery monitoring system. Additionally, this type of power bank can be purchased at most commercial stores or online, so it would be easy to replace or order more of. Considering all of these features, it comes at the low cost of $21.50. Implementing this into the project provided a low cost battery with many features that is capeable of giving us the desired battery life of 8 hours. Any typical power bank could be used to supply power to our project, so long as it's output voltage is between a 5-30V range and is capeable of supplying at least 300 mA of current, although the total power of the bank will determine the runtime of the board.

To get our supply voltage from the battery, we are using a QC trigger that requests 5V from one of the USB outputs on the power bank. A custom housing and cables are installed on top of the QC trigger to avoid any potential shorts across the pads.

## 5. Tennis Scoring System HLA
![HLA](HLA.png)

The high level assembly of the tennis scoring system combines the MCU board and two display boards in a wiring harness with some additional components. 

### 5.1 Power & Safety
The battery has a M to F USB-C cable (CAB-15455) that will be mounted on the outside of the case that is used to charge the battery. The QC trigger (POWTRIG-A-5V-5PK) is placed into one of the USB-A ports and then goes to terminal 1 of the power switch. The power bank the scoreboard is using will not allow the scoreboard to be on while it is charging. If a charging cable is connected to the battery while the scoreboard is in use, the scoreboard will turn off. In addition to the short circuit protection built into the power bank, an inline 2A fuse is placed between the power output from the battery and the switch. 

### 5.2 Additional Assembly Recommendations
Solid core wire is recommended for all screw terminals. Stranded wire may be used, but ferrules must be applied to bare ends. All wire is inteded to be 22 AWG as the crimps for the assembly are rated for 18-22 AWG wires. All wires should be placed in loom up to the technicians discretion. Zip tie mounting hardware (3240707) should be used in combination with zip ties to secure wire harness. 

Care must be taken when inserting the antenna extension cable (CSH-SGFB-100-UFFR) into the MCU board. The extension cable is mounted to the top panel of the case, and an omnidirectional antenna is then screwed into the male end. 

### 5.3 FLA and Power Usage
Observed FLA for an entire HLA with a score of 8-8 (all segments on) and an input voltage of 5V is 0.4A. This value is subject to change with input voltage. A 2A fast-blow fuse gives enough room for the inrush current of an inital power-up, but also provides additional short circuit protection.
