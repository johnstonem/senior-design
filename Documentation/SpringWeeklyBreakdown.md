# Spring Semester Weekly Task Breakdown
The spring semester has 15 weeks, and then we'll need to have our project wrapped up. This document serves as a planning document for those weeks to help focus and organize our efforts.

## Required Deliverables
The organizers of the senior design projects (Dr. Durant and Dr. Thomas) have some required assignments and deliverables. 

- [Code of Ethics](https://gitlab.com/msoe.edu/eecs/seniordesign/ce/-/wikis/Code-of-Ethics): Due Week 2
- [Wk 5 Presentation](https://gitlab.com/msoe.edu/eecs/seniordesign/ce/-/wikis/Spring-2024-02-Presentation-Schedule): Due **UNKNOWN**
- [Marketing Project Booklet](https://gitlab.com/msoe.edu/eecs/seniordesign/ce/-/wikis/Marketing-Project-Booklet): Due **UNKNOWN**
- [Senior Design Show Registration Form](https://gitlab.com/msoe.edu/eecs/seniordesign/ce/-/wikis/showReqForm.pdf) : Due Friday, April 21, _2023_??
- [Final Report](https://gitlab.com/msoe.edu/eecs/seniordesign/ce/-/wikis/Final-Report): Due **UNKNOWN**
- [Senior Design Show Poster](https://gitlab.com/msoe.edu/eecs/seniordesign/ce/-/wikis/Senior-Design-Show-Poster): Due **UNKNOWN**
- [Week 15 Presentation](https://gitlab.com/msoe.edu/eecs/seniordesign/ce/-/wikis/Spring-2024-05-Presentation-Schedule): Due **UNKNOWN**
- [Senior Design Show](https://www.msoe.edu/academics/senior-project-showcase/): During Friday of Finals Week

## Planned Weekly Tasks
The semester has 15 weeks total. These are being split into three 5 week segments: 
1. Weeks 1-5: Work time for functionality
2. Weeks 6-10: Cleanup and time for stretch goals
3. Weeks 11-15: Wrap up, documentation, presentations.

With this, We can roughly construct weekly tasks:

|Week|Required Tasks|Other Tasks|
|-|-|-|
1||Finalize / order PCBs
2|Complete the Code of Ethics Assignment|Order other components for the boards.
3||Develop additions to web interface.
4|Create draft for next week's presentation.| Have demo for next week presentation. Print Enclosure
5|Give 2nd presentation.|Assemble PCBs with components. 
6||Debugging and cleanup time
7|Create Marketing Booklet|Field test and get feedback from target consumers.
8|Register for Senior Design Show|Add improvements from feedback, clean up interface
9|Begin the Final Report|Further cleanup of product
10|Draft the Final Report|
11|Finalize the Final Report|
12|Create draft of Poster|
13|Finalize poster for presenting.|
14|Create draft for presentation.|
15|Give 3rd presentation.|
Finals|Present our project at the Senior Design Show|