# Tennis Scoring System - Operations Guide

## Documentation Date - 5/2/2024

# Table of Contents
- [Scoreboard Guide](#1-scoreboard-guide---tennis-player-operation)
- [Web Server Guide](#2-web-server-guide)

# 1. Scoreboard Guide - Tennis Player Operation
![Scoreboard Drawing HERE](image.png)

### 1 -  Increment Respective Score Button
- Increments score for its seven segment display.
### 2 - Decrement Respective Score button
- Decrements score for its seven segment display.
### 3 - New Set Button
- Zeros Out Score
- Increments Sets Won LEDs
### 4 - Switch Sides Button
- Swaps Home and away
### 5 - New Match Button
- Zeros Out Score
- Sends Final Data to CSV
### 6 Sets Won Indicators
- LEDs which indicate number of sets won on each side
### 7 Home Side Indicator
- Indicates which side is home 
### 8 +10 Indicator
- If the score is greater than 9, indicate that the score is greater than 9

# 2. Web Server Guide
![alt text](image-3.png)

### 1 Court Number
- This is the indicated tennis court number (corresponding scoreboard #) of the match.

### 2 Player Names
- Player names can be changed by whoever accesses the web server.

### 3 Game Score
- The game score is displayed as a numeral, in this case zero.

### 4 Set Score
- The set score is displayed as circular indicators, each light up based on # of sets won.

