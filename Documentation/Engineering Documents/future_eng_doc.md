# Tennis Scoring System - Future Updates

## Documentation Date -  4/17/2024

## Team
- Charles Schaefer - Computer Engineering
- Tyler Togstad - Computer Engineering
- Ryan Beatty - Computer Engineering
- Mitchell Johnstone - Computer Engineering/Computer Science
- John Paul Bunn - Computer Engineering/Computer Science

## Table of Contents
- [1. Project Overview](#1-project-overview)
- [2. Future of Software](#2-future-of-software)
    - [2.1 Improvements to Web Server](#21-improvements-to-web-server)
    - [2.2 Tennis Score Reporting](#22-tennis-score-reporting)
    - [2.3 Flash Memory](#23-flash-memory)
- [3. Future of Hardware](#3-future-of-hardware)
    - [3.1 Improvements to Display Brightness](#31-improvements-to-display-brightness)
    - [3.2 Better Connectors](#32-better-connectors)
    - [3.3 Changing Board Interconnection](#33-changing-board-interconnection)
    - [3.4 Changes to Voltage Regulator](#34-changes-to-voltage-regulator)
    - [3.5 Changes to Battery](#35-changes-to-battery)
    - [3.6 RGB LEDs](#36-rgb-leds)

## 1. Project Overview

## 2. Future of Software

### 2.1 Improvements to Web Server
The web server is in a solid state where it functions properly; however, changes could be made to make it look more like a polished product. Most of these changes would likely occur in the HTML document that hosts the server.

### 2.2 Tennis Score Reporting
The current iteration of the project does not interact with tennisreporting.com. This may be quite helpful for the coach in the future so that he only needs to do the verification on the reports. 

### 2.3 Flash Memory
In the event of a power failure, the scores are not saved to flash memory and thus will be lost. There is room to implement a feature to save the scores in flash memory in order to preserve the scores. This should be implemented as a fail safe in a further update of the project.

## 3. Future of Hardware

### 3.1 Improvements to Display Brightness
There are several ways to improve the brightness of the display. The easiest way is to increase the voltage given to the display board. This change would require a 5V or 3.3V power supply given to the decoder (change display board schematic & PCB). If this isn't done, the logic level difference is too high, and the IC doesn't recognize the ESP32 inputs. The other way to increase the brightenss is changing the resistors attached to the LEDs to a smaller resistance. The proposed resistance for this was 100 Ohms. 

### 3.2 Better Connectors
The current configuration of the display board and MCU board use terminal blocks at the wire ends to connect them to each board. A different type of connector that houses all of the buttons and power may be better suited for full scale manufacturing, since it will be easier to plug one cable in. This may or may not decrease cost as well, since terminal blocks can be fairly expensive. A good connector choice would be a deutsch style connector or a molex style connector. 

### 3.3 Changing Board Interconnection
The current configuration has one main header leaving the MCU board. Considering the placement of the MCU board within the case, it is probably better to have two separate headers on the MCU board to have a lower cable cost. 

### 3.4 Changes to Voltage Regulator
The MCU board uses an LDO to convert the input voltage to the needed 3.3V. A better slightly more expensive form of voltage regulator could be used to decrease the power consumption of the board. The added cost may or may not make this actually worthwhile. A proposed better regulator would be a switching regulator.

### 3.5 Changes to Battery
The power bank is a good option as it stands. It may be worthwhile to develop a custom battery solution that is well suited for the project. It could potentially save some production costs, but would need have longer engineering and development costs.

### 3.6 RGB LEDs
The coach would like different colored lights for each side and RGB would allow a plethora of colors to be used for the Home and Away side of the scoreboard. (Gotta lithen to the Coath)