# Tennis Scoring System - Software

## Documentation Date -  4/17/2024

## Team
- Charles Schaefer - Computer Engineering
- Tyler Togstad - Computer Engineering
- Ryan Beatty - Computer Engineering
- Mitchell Johnstone - Computer Engineering/Computer Science
- John Paul Bunn - Computer Engineering/Computer Science

## Table of Contents
- [1. Project Overview](#1-project-overview)
- [2. Software Specifications](#2-software-specifications)
    - [2.1 Software IDE](#21-software-ide-visual-studio-code)
    - [2.2 MCU Software](#22-mcu-software)
    - [2.3 ESP32 GPIO Ports](#23-esp32-gpio-ports)
    - [2.4 PlatformIO](#24-platformio)
    - [2.5 SPIFFS](#25-spiffs)
    - [2.6 Painless Mesh](#26-painless-mesh)
    - [2.7 Web Sockets](#27-web-sockets)
    - [2.8 Server vs Standard](#28-server-vs-standard)
    - [2.9 Web Server](#29-web-server)
- [3. Compiling Instructions](#3-compiling-instructions)

## 1. Project Overview
This is the software component of the tennis scoring system. This document will dive further into the details of which software was used and how to use each of the components. All of the components used can be found within the Software Specifications section of the document. This document will also include information on how to flash the software onto the ESP32 boards as well as the general system architecture layout. 

## 2. Software Specifications

### 2.1 Software IDE (Visual Studio Code)
- **Description:** Primary IDE for software development.

- **Functionality:** Visual Studio Code is a multi-purpose text editor designed for software developers, so there's integrations with various programming languages. With extensions, it can be used as an IDE with PlatformIO integration and better file organization.

- **Resources:** Install VS Code [here.](https://code.visualstudio.com/download)

### 2.2 MCU Software
- **Description:** Software to drive MCU board and Display board. 

- **Specifications:** Implements SPIFFS and Sockets

- **Functionality:** The MCU software is the primary software driver within the project. This code drives the two PCBs and is loaded into the flash memory of the ESP32. 

### 2.3 ESP32 GPIO Ports
- **Description:** This project uses all of the GPIO ports on the ESP32 and this section will explain the specific details about each GPIO port.

- **Specifications:** The following table outlines specified functionality of each GPIO port.

    | GPIO Port # | Description | Additional Info |
    | ----------- | ----------- | --------------- |
    | 2           | Right 7-segment, bit 2 | |
    | 4           | Right 7-segment, bit 1 | |
    | 5           | Button 5               | |
    | 12          | Left 7-segment, bit 1  | |
    | 13          | Right 7-segment, bit 4 | |
    | 14          | Left 7-segment, bit 2  | |
    | 15          | Right 7-segment, bit 3 | |
    | 16          | Button 7               | |
    | 17          | Button 6               | |
    | 18          | Left +10 points        | |
    | 19          | Right +10 points       | |
    | 21          | Right Set Score 2      | |
    | 22          | Right Set Score 1      | |
    | 23          | Right Home Indicator   | |
    | 25          | Left Home Indicator    | |
    | 26          | Left 7-segment, bit 4  | |
    | 27          | Left 7-segment, bit 3  | |
    | 32          | Left Set Score 1       | |
    | 33          | Left Set Score 2       | |
    | 34          | Button 3               | Input-Only, No Pull-Up |
    | 35          | Button 4               | Input-Only, No Pull-Up |
    | 36          | Button 1               | Input-Only, No Pull-Up |
    | 39          | Button 2               | Input-Only, No Pull-Up |

- **Functionality:** The ESP32 GPIO ports provide for a simple connection between software and hardware. Additional information can be found about the GPIO ports at the provided link within the resources section below.

- **Resources:** More information on the ESP32 GPIO ports [here.](https://randomnerdtutorials.com/esp32-pinout-reference-gpios/)

### 2.4 PlatformIO
- **Description:** This library bridges the gap between VS Code and the ESP32. The library provides flash programming and additional file organization to be possible.

- **Specifications:** Open source library/extension in VS Code.

- **Resources** Installation guide can be found at the following [tutorial.](https://www.youtube.com/watch?v=5edPOlQQKmo)

### 2.5 SPIFFS
- **Description:** Serial Peripheral Interface Flash File System. This tool allows the ESP32 to store files into flash memory.

- **Specifications:** Method used to store the code for local web server.

- **Functionality:** Stores files within flash memory of ESP32 for use during normal operation.

- **Resources:** Additional information about SPIFFS [here.](https://randomnerdtutorials.com/install-esp32-filesystem-uploader-arduino-ide/)

### 2.6 Painless Mesh
- **Description:** Open Source library for establishing a mesh WiFi system among multiple ESP32s.

- **Specifications:** Open Source mesh network library for ESP32 and ESP8266.

- **Functionality:** Provides the mesh network connection protocol between ESP32 boards and makes for a simple process to connect multiple boards together into a mesh-like WiFI system. 

- **Resources** Additonal information about Painless Mesh [here.](https://randomnerdtutorials.com/esp-mesh-esp32-esp8266-painlessmesh/)

### 2.7 Web Sockets
- **Description:** Web sockets are utilized in order to establish a continous client connection with the locally hosted web server. This is done to avoid constanly refreshing the web server

- **Resources:** Additional information about Web Sockets [here.](https://randomnerdtutorials.com/esp32-websocket-server-sensor/)

### 2.8 Server Vs Standard
- **Description:**
The server node is utilized to host the web server. This code will run on one of the scoreboards and will be the main scoreboard. This scoreboard should be scoreboard #1.

    The standard node is utilized on any scoreboard that is not the server node. 

- **Location:**
    
    Server Code: [/Production Code/MeshServerNode](../../Production%20Code/MeshServerNode/)

    Standard Code: [/Production Code/MeshStandardNode](../../Production%20Code/MeshStandardNode/)

### 2.9 Web Server
- **Description:** 
The web server is the locally hosted web site that the scoreboards upload their scores to. The web server is hosted by one of the scoreboards and its signal is propagated through the mesh network.

- **Example Photo:**

## 3. Compiling Instructions
The PlatformIO Tab is on the left side of the screen (also indicated by the Alien Logo) and is used for flashing the SPIFFS filesystem onto the ESP32. This is required before flashing the main program onto the board. In order to flash the SPIFFS filesystem, you will need to click "Build Filesystem Image", and then "Upload Filesystem Image". 

PlatformIO provides an interface with many buttons located at the bottom of VS Code which can be used to flash the software into the ESP32. After flashing the filesystem, you can then flash the main software with the buttons on the bottom of the IDE.

The serial monitor can also be used to interact with the ESP32 through a serial connection. The serial monitor was mostly used for testing connection and functionality of the ESP32. If bootup is successful, the user should see a new wifi connection available, TennisMesh, that is broadcasted by the ESP32.