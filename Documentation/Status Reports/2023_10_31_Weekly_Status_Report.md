# Weekly Status Report
Course: CPE 4901 004 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 31 OCT 2023

Term: Fall

Week: 9

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
This week we've confirmed that the mesh network will communicate between disjoint nodes, although messages drop when the mesh is incomplete, and that a wifi signal can be extended through the network. 2 LED segments of a display have been completed and are shown to work with our current decoder. We plan to complete the example display to get a power estimate and create a program to interface with the board.

## Status Update
- JP began coding a system for logging the tennis match history.
- Ryan And Tyler got the decoder worker and got 2 segments working.
    - Want to get full 7segment to get an approximation of power consumption
- Tyler did research on housing (different poly-carb) and light scattering.
- Charles researched connection methods. He used 2 boards, one to host a web server and one to propagate the signal through a mesh network.
- Charles, JP, and Mitchell did field testing for the mesh network to find distances and determine how messages are propagated through multiple boards.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|Software R&D|2 hrs|3 hr|||1 hr|
|Connection test|1 hr|1 hr|||1 hr|
|LED soldering|||2 hrs|
|Decoder wiring|||8 hrs|6 hrs|
|Elevator Speech|1 hr|1 hr|1 hr|1 hr|1 hr|

### Weekly Project Effort
|Week       |John Paul  |Charles    |Ryan   |Tyler  |Mitchell   |
|--         |--         |--         |--     |--     |--         |
|W1         |2 hrs      |2 hrs      |2 hrs  |8 hrs  |3 hrs      |
|W2         |2 hrs      |4 hrs      |4 hrs  |6 hrs  |3 hrs      |
|W3         |1 hrs      |5 hrs      |3 hrs  |5 hrs  |5 hrs      |
|W4         |3 hrs      |3 hrs      |2 hrs  |6 hrs  |3 hrs      |
|W5         |2 hrs      |1 hr       |       |1 hr   |2 hr       |
|W6         |3 hrs      |3 hrs      |4 hrs  |5 hrs  |3 hrs      |
|W7         |           |5 hrs      |4 hrs  |6 hrs  |3 hrs      | 
|W8         |           |2 hrs      |6 hrs  |4 hrs  |4 hrs      | 
|W9         |4 hrs      |5 hrs      |11 hrs |7 hrs  |3 hrs      | 
|**Total:** |17 hrs     |30 hrs     |36 hrs |48 hrs |29 hrs     |

## Discussion
After testing LEDs, we found that surface mount LEDs are a better alternative.
- They scatter much better
- They're closer to the board, making the product thinner
- Easy to place and assemble with the oven

2 of the 7 segments were soldered, so we should be able to get an estimate of the power consumption.
The new decoder system was wired up and verified to work, so we're constructing an example board to work with the surface mount parts to get a test environment for display.

We did some connection testing this week for the ESP32, specifically looking at how signals were propagated throughout the mesh network at range. We found that we could communicate between boards consistently, although we had some dropout when the boards were too far away from each other. Specifically, a message could be communicated to all nodes, even when some nodes aren't directly connected. This caused some messages to be dropped when disconnected from the network for too long. Afterwards, we found we could extend the signal of a web server hosted on an ESP32 web server by connecting node from a mesh network to the wifi access point. We need to test this with more boards to verify.

## Plan Update
- Presentation topics
    - Overview of problem - JP
    - Tennis scoring - Charles
    - Our solution - JP
    - Power / Battery - Tyler
    - LED Array / Decoder - Ryan
    - ESP32 / Mesh Network - Mitchell
    - Plan moving forward
- Basic 3D Model for scoreboard housing
- Fully working LED 7 Segment display
- Code
    - Cross communication of updates
    - Basic example working w/ 7 segment.
        - Buttons to increment / decrement?

## Conclusion
Our top priority is to draft a presentation for week 11, and with other development time we'll create demos for tennis scoring code and the display.