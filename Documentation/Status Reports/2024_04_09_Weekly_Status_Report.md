# Weekly Status Report
Course: CPE 4902 002 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 09 April 2024

Term: Spring

Week: 11

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
The project is currently testing a 3D model component against display boards, addressing safety concerns with open wiring, cleaning up schematics, debugging the SPIFFs system, and ordering additional components.

## Status Update
- Charles developed the 3D model for the QC trigger.
- Tyler worked on the documentation for the display board schematic and PCB.
- Ryan and Mitchell updated the 3D model following 
- JP worked more on the simulation, debugging the memory address issue for SPIFFs.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|Schematic & PCB updates||||4 hrs|
|3D modelling||3 hrs|3 hrs||3 hrs|
|Simulation|3 hrs|

### Weekly Project Effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--     |--     |--     |--     |--     |
|W1         |2 hrs  |2 hrs  |2 hrs  |8 hrs  |3 hrs  |
|W2         |2 hrs  |4 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W3         |1 hrs  |5 hrs  |3 hrs  |5 hrs  |5 hrs  |
|W4         |3 hrs  |3 hrs  |2 hrs  |6 hrs  |3 hrs  |
|W5         |2 hrs  |1 hr   |       |1 hr   |2 hr   |
|W6         |3 hrs  |3 hrs  |4 hrs  |5 hrs  |3 hrs  |
|W7         |2 hrs  |5 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W8         |2 hrs  |2 hrs  |6 hrs  |4 hrs  |4 hrs  |
|W9         |4 hrs  |5 hrs  |11 hrs |7 hrs  |3 hrs  |
|W10        |3 hrs  |5 hrs  |2 hrs  |3 hrs  |4 hrs  |
|W11        |6 hrs  |4 hrs  |7 hrs  |5 hrs  |5 hrs  |
|W12        |3 hrs  |2 hrs  |3 hrs  | 3 hrs |2 hrs  |
|W13        |<td>Thanksgiving Break</td>
|W14        |5 hrs  |5 hrs  |       |5 hrs  |5 hrs  |
|W15        |3 hrs  |3 hrs  |3 hrs  |3 hors |3 hrs  |
|W1         |4 hrs  |4 hrs  |4 hrs  |8 hrs  |3 hrs  |
|W2         |6 hrs  |4 hrs  |5 hrs  |6 hrs  |6 hrs  |
|W3         |3 hrs  |2 hrs  |5 hrs  |6 hrs  |7 hrs  |
|W4         |1 hr   |15 hrs |6 hrs  |20 hrs |6 hrs  |
|W5         |Sick   |3 hrs  |4 hrs  |4 hrs  |8 hrs  |  
|W6         |Sick   |4 hrs  |       |4 hrs  |4 hrs  |  
|W7         |4 hrs  |8 hrs  |8 hrs  |13 hrs |6 hrs  |
|W8         |6 hrs  |3 hrs  |2 hrs  |10 hrs |2 hrs  |
|W9         |5 hrs  |5 hrs  |6 hrs  |6 hrs  |4 hrs  |
|W10        |6 hrs  |2 hrs  |3 hrs  |8 hrs  |6 hrs  |
|W11        |3 hrs  |3 hrs  |3 hrs  |4 hrs  |3 hrs  |
|**Total:** |74 hrs |95 hrs |100 hrs|139 hrs|92 hrs |

## Discussion
The current model for the case can be found [here](https://cad.onshape.com/documents/e1a26b3d9c59e44e8335c20b/w/848112aac7653ba4e4257da2/e/7ffceb83b5eebe99adb6bb4b). A component of the 3D model was printed last week and tested against the display boards. The test showed that the model was too wide on the outside but close together on the inside. After planning out some locations of components, the 3D model had some updates, but more are still needed. Specifically, the design was decided to need separate components for the light isolator and the box, so that needs to be reflected in the model.

Currently, the QC trigger has open wiring that could be potentially hazardous. Charles has designed a 3D model to wrap around the wiring to prevent such an event, and hopefully connect to the case. Additionally, some of the schematics were left a bit messy during development, so Tyler spent some time cleaning them up. Otherwise, JP has been working to debug the SPIFFs system on the simulation, which has continued to be an issue since last status report.

More components were ordered in our last purchase order of the project, including some PLA for the case prints. 

## Plan Update
- Finish production on 2nd scoreboard electronics - Tyler & Charles
- Finalized updates to the 3D model - Ryan & Mitchell
- Begin work on the poster presentation - Charles
- Resolve the SPIFFs error - JP

## Conclusion
For the tennis scoreboard project, we plan to finish production on the 2nd scoreboard electronics, finalize updates to the 3D model, begin work on the poster presentation, and resolve the SPIFFs error.