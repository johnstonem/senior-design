# Weekly Status Report
Course: CPE 4902 002 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 19 March 2024

Term: Spring

Week: 8

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
Revisions were made to all boards to address core issues, and components were ordered for both. An extended antenna was ordered for testing after the break, and the MCU board was assembled successfully, showing progress from the last revision.

## Status Update
- Ryan and Tyler made updates to the Display and MCU board, respectively.
- Tyler ordered the revised versions of the MCU board and the display board.
- Charles and Tyler assembled one version of the MCU PCB that works well and is able to have programs flashed onto it.
- Tyler ordered parts for the new versions of the MCU & Display boards.
- Mitchell sent out the finalized Marketing Booklet Memo.
- Mitchell added Set Indicators for the Website
- JP finished implementing the history in the website.
- JP began implementing virtual ESP32s as a demo for multiple boards.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|PCB Revisions|||2 hours|2 hours|
|Assembly||3 hours||8 hours|
|Web Updates|||||2 hours|
|Virtual ESP32 demo|6 hours|

### Weekly Project Effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--     |--     |--     |--     |--     |
|W1         |2 hrs  |2 hrs  |2 hrs  |8 hrs  |3 hrs  |
|W2         |2 hrs  |4 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W3         |1 hrs  |5 hrs  |3 hrs  |5 hrs  |5 hrs  |
|W4         |3 hrs  |3 hrs  |2 hrs  |6 hrs  |3 hrs  |
|W5         |2 hrs  |1 hr   |       |1 hr   |2 hr   |
|W6         |3 hrs  |3 hrs  |4 hrs  |5 hrs  |3 hrs  |
|W7         |2 hrs  |5 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W8         |2 hrs  |2 hrs  |6 hrs  |4 hrs  |4 hrs  |
|W9         |4 hrs  |5 hrs  |11 hrs |7 hrs  |3 hrs  |
|W10        |3 hrs  |5 hrs  |2 hrs  |3 hrs  |4 hrs  |
|W11        |6 hrs  |4 hrs  |7 hrs  |5 hrs  |5 hrs  |
|W12        |3 hrs  |2 hrs  |3 hrs  | 3 hrs |2 hrs  |
|W13        |<td>Thanksgiving Break</td>
|W14        |5 hrs  |5 hrs  |       |5 hrs  |5 hrs  |
|W15        |3 hrs  |3 hrs  |3 hrs  |3 hors |3 hrs  |
|W1         |4 hrs  |4 hrs  |4 hrs  |8 hrs  |3 hrs  |
|W2         |6 hrs  |4 hrs  |5 hrs  |6 hrs  |6 hrs  |
|W3         |3 hrs  |2 hrs  |5 hrs  |6 hrs  |7 hrs  |
|W4         |1 hr   |15 hrs |6 hrs  |20 hrs |6 hrs  |
|W5         |Sick   |3 hrs  |4 hrs  |4 hrs  |8 hrs  |  
|W6         |Sick   |4 hrs  |       |4 hrs  |4 hrs  |  
|W7         |4 hrs  |8 hrs  |8 hrs  |13 hrs |6 hrs  |
|W8         |6 hrs  |3 hrs  |2 hrs  |10 hrs |2 hrs  |
|**Total:** |65 hrs |90 hrs |94 hrs |137 hrs|83 hrs |

## Discussion
Following from last week, all the boards were revised to fix their core issues. This week, they were sent to order, allowing them to be processed during the spring break. More components were also ordered for both boards. 

Notably, we had to order an extended antenna that we will be testing after break. Our ESP32s didn't have a built in antenna, so this was a necessary order.

The MCU board was received and assembled the monday after break. The board is able to power on and be flashed to with our program, which is good progress and an improvement from last revision.

On the software side, it was decided to pursue a virtual demo of our ESP32 mesh network, since we won't be building more than 2 full scoreboards by the senior design show. Instead, the simulation will demonstrate the effectiveness of using a mesh network for tennis courts.

Otherwise, the website has had more progress done on the history of the games and the set indicators.

## Plan Update
- Finish the ESP32 mesh demo - JP
- Assemble the Display board for a test demo - Ryan
- Finalize the CAD of the case - Mitchell / Ryan
- Flash the software to the MCU and debug - Charles / Tyler
- Assemble duplicate MCU boards for mesh testing - Tyler

## Conclusion
The team plans to finish the ESP32 mesh demo and assemble the Display board for a test demo. CAD of the case will be finalized, and software will be flashed to the MCU for debugging. Duplicate MCU boards will also be assembled for mesh testing.