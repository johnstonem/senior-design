# Weekly Status Report
Course: CPE 4901 004

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 5 SEP 2023

Term: Fall

Week: 1

## Executive Summary
We created a git lab environment for the team to use, started on our detailed proposal and development plan document, and started researching items for HW and SW design. We also designated Tuesday at 3 PM as the weekly meeting with our advisor, Dr. Durant.

## Status Update
- Scheduled weekly meeting with advisor
- Created GitLab repo for code/documentation/other shared files
- Began research on technology to use for project
- Tyler – Created this document, HW research, and tennis research (~8 HRS)
- Mitchell - Set up Gitlab Repo, started the Detailed Proposal and Development Plan. (~3 hrs)
- Everyone - Tennis research (~1 hr)

## Discussion
Seven Segment displays are available as large displays but will use a considerable amount of power & typically use 12/24 VDC. Need to either find a driver board or create one to manage this. Decide on communication to driver board (depends on MCU)? And how to power the board with that much power draw?

## Plan Update
Milestones: We started to work on the Detailed Proposal and Development Plan, but want to talk with our contact before finalizing and submitting by 13 September 2023 (performed by everyone). We'd like to have a rough draph hopefully by 8 September 2023. 

Get a rough idea of the microcontroller we want to use to control the scoreboard. Get a rough idea of what programming language(s) we want to use to develop software to control the scoreboard. Understand the customer’s wants and needs through the meeting on Wednesday (performed by everyone except Mitchell). Understand the game of tennis more to provide the best solution possible (performed by everyone).

## Conclusion
In week 2 we will complete and submit the Detailed Proposal and Development Plan by 13 September 2023. We will also continue planning development for the scoreboard and devices we plan to use when creating it. 