# Weekly Status Report
Course: CPE 4901 004 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 24 OCT 2023

Term: Fall

Week: 8

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
We continued on our development plan, specifically looking to implement system components. A web server was set up so an ESP32 could be used as a WIFI endpoint and mobile devices could view statistics about the board. Work on the LED display was done, but the decoder doesn't work with our setup, so a new one is ordered. Light diffusion techniques were discussed to get a smoother display.

## Status Update
- Charles and Mitchell researched into different ways to connect the ESP32s and host an internet server on board.
- Charles, Mitchell, and Tyler tested extending the ESP32 network across multiple boards, but the endpoint didn't extend.
- Ryan and Tyler attempted to use a 7-segment decoder, but it failed to operate correctly, so we're ordering a new decoder. They also researched into new techniques for diffusing the light from the LED to make it look pretty.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|Connection Development||1 hr|||3 hrs|
|Connection test||1 hr|||1 hr|
|LED Construction|||5 hrs|4 hrs||
|SyRS|||1 hr|

### Weekly Project Effort
|Week       |John Paul  |Charles    |Ryan   |Tyler  |Mitchell   |
|--         |--         |--         |--     |--     |--         |
|W1         |2 hrs      |2 hrs      |2 hrs  |8 hrs  |3 hrs      |
|W2         |2 hrs      |4 hrs      |4 hrs  |6 hrs  |3 hrs      |
|W3         |1 hrs      |5 hrs      |3 hrs  |5 hrs  |5 hrs      |
|W4         |3 hrs      |3 hrs      |2 hrs  |6 hrs  |3 hrs      |
|W5         |2 hrs      |1 hr       |       |1 hr   |2 hr       |
|W6         |3 hrs      |3 hrs      |4 hrs  |5 hrs  |3 hrs      |
|W7         |           |5 hrs      |4 hrs  |6 hrs  |3 hrs      | 
|W8         |           |2 hrs      |6 hrs  |4 hrs  |4 hrs      | 
|**Total:** |13 hrs     |25 hrs     |25 hrs |41 hrs |26 hrs     |

## Discussion
The decoder that was being tested proved to be unreliable, so another one was ordered. The LEDs that were originally our favorite have hot spots in the display, so we need to test out alternatives or workarounds os the display looks nicer. This includes
- Testing new LEDs.
- Using a diffusion gradient.
The SyRS document was improved, including coach Matousek's feedback, and specific FCC requirements, and visual designs.

A mesh network had previously been implemented, so we know we can communicate between boards. We set up a web server so that a mobile device can connect to the board network and see current updates on a mobile page. We tried seeing if we can extend the network through other ESP32s, but it didn't work, so we need to find the correct configuration.

## Plan Update
We plan to continue the following goals:
- Test ESP32 Mesh Network with a 3rd board
- Minimum Viable Product of board
    - [x] Buttons on GPIO for incrementing / resetting
        - [x] Breadboard components
    - [x] WIFI page that displays info
    - [ ] Demonstration of cross-communication & updating
- Display
    - [ ] Create a mockup of the LED display
    - [ ] Get feedback from team / Durant / coach 
    - [ ] Define GPIO / power requirements
    - [ ] Finish KiCad Display Schematic
    - [ ] Document (take photos of) LED issue and potential fixes
- Updates to SyRS based on Feedback
- Elevator Speech
- Begin Compiling Presentation

## Conclusion
The connection between ESP32s has been tested, but needs further testing for larger networks and signal extension. 
The LED array is being updated to use a better looking display, and when the new decoder is in, we should be able to make a proof of concept.