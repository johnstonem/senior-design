# Weekly Status Report
Course: CPE 4901 004 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 17 OCT 2023

Term: Fall

Week: 7

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
A mesh network for ESP32 communication was tested and verified as a viable method with sufficient range. Hardware LED array components were hooked up. supplying power to the LED array should be okay, still working on the decoder for communication.

## Status Update
- Charles and Tyler tested wireless communication, both bluetooth and wifi, through a simple script and found we can connect to phones easily and with large range (200-300ft).
- Mitchell, Charles, and JP tested a mesh network with 2 ESP32 modules, found a connection range outside of about 200ft between the microprocessors. 
- Tyler and Ryan researched hardware components, including getting a working transistor array and testing out a 7 segment decoder.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|SyRS||1 hr||1 hr|1 hr|
|Mesh connection testing|2 hrs|2 hrs|||2 hrs|
|Wireless connection testing|1 hr|2 hr||1 hr||
|Hardware Research|||4 hr|4 hr||

### Weekly Project Effort
|Week       |John Paul  |Charles    |Ryan   |Tyler  |Mitchell   |
|--         |--         |--         |--     |--     |--         |
|W1         |2 hrs      |2 hrs      |2 hrs  |8 hrs  |3 hrs      |
|W2         |2 hrs      |4 hrs      |4 hrs  |6 hrs  |3 hrs      |
|W3         |1 hrs      |5 hrs      |3 hrs  |5 hrs  |5 hrs      |
|W4         |3 hrs      |3 hrs      |2 hrs  |6 hrs  |3 hrs      |
|W5         |2 hrs      |1 hr       |       |1 hr   |2 hr       |
|W6         |3 hrs      |3 hrs      |4 hrs  |5 hrs  |3 hrs      |
|W7         |           |5 hrs      |4 hrs  |6 hrs  |3 hrs      | 
|**Total:** |13 hrs     |23 hrs     |19 hrs |37 hrs |22 hrs     |

## Discussion
After testing the range of the mesh network, we found that that in-board antenna is able to communicate between the boards or to the phone.
With a range of about 200 ft, the boards should be able to transmit cross-court,although further testing will have to be done to see if the enclosure will disrupt the signal.
If the signal is blocked too much or won't extend far enough, we must test either sticking the antenna outside of the enclosure or extending the signal range with another antenna.

To support the LED array, a darlington transistor array was ordered to support the signal energy needed. The component was successfully integrated to power the LED array.
A decoder component was also ordered but is proving difficult to configure. Another may need to be ordered / tested.

## Plan Update
Roughly the next 2 weeks of development:
- Test ESP32 Mesh Network with a 3rd board
- Minimum Viable Product of board
    - Buttons on GPIO for incrementing / resetting
        - Breadboard components
    - WIFI page that displays info
    - Demonstration of cross-communication & updating
- Display
    - Create a mockup of the LED display
    - Get feedback from team / Durant / coach 
    - Define GPIO / power requirements
    - Start KiCad Schematic
- Order - A new relay? 
- Updates to SyRS based on Feedback
- Elevator Speech
- Begin Compiling Presentation

## Conclusion
Building upon the progress of the last 2 weeks, the LED array will be further developed, both in terms of UI and functionality, and a more apt example of ESP32 mesh communication will be done to demonstrate our intention.