# Weekly Status Report
Course: CPE 4901 004 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 3 OCT 2023

Term: Fall

Week: 5

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
The Fall Career Fair and exams took away a lot of focus from this week so not a lot got done. The Technology and Concept Report was revised and submitted on Sept 29th. We planned out what the next 1~2 weeks should look like to get back on track for development.

## Status Update
- Mitchell revised and submitted the Technology and Concept Report.
- Charles began some preliminary research for scoreboard schematic components.
- Tyler research the Mesh Network idea for the ESP32 systems and potential development.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|Technology Report Revisions|1 hr||||2 hrs|
|Schematic research||1 hr||||
|Mesh Network||||1 hr||

### Weekly Project Effort
|Week|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|W1|2 hrs|2 hrs|2 hrs|8 hrs|3 hrs|
|W2|2 hrs|4 hrs|4 hrs|6 hrs|3 hrs|
|W3|1 hrs|5 hrs|3 hrs|5 hrs|5 hrs|
|W4|3 hrs|3 hrs|2 hrs|6 hrs|3 hrs|
|W5|2 hrs|1 hr||1 hr|2 hr|
|**Total:**|10 hrs|15 hrs|11 hrs|26 hrs|16 hrs|

## Discussion
Week 5 had a lot of unexpected events that took priority over the development process. The Career Fair, both prep and the events, took a lot of time away from development. As well, being 1/3 way through the semester, some midterms were given this past week. As such, the only main accomplishment was making the revisions and submission of the technology and concept report. We're hoping to get back into the prototyping phase to grind out some uncertainties in the product. 

## Plan Update
We have drafted a few 2 week projects. Deliverables are due on OCT 16:
- Prototyping with ESP32 Communication System - JP & Charles
    - Mesh network
        - Does a phone connection have seamless transition between ESP32s?
        - How far do the ESP32s connect?
        - How far can a phone connect to an ESP32?
    - iPhone communication
        - Can we communicate via bluetooth well?
        - Can we communicate via wifi well?
    - Stretch goals: test the HC-12 antennas, Update Scoreboard Schematic.
- Developing the Custom LED board and power options - Ryan & Tyler
    - Email some EE faculty to get some input on battery statistics
    - Schematic of the Custom LED 7 segment display
    - Prototype of LED board with statistics pulled from it for comparison
    - Schematic for Power relays and 
- Writing up the SyRS Report - Mitchell & everyone when time permits.
    - Draft, following the IEEE-1233 standard.

## Conclusion
The key priority in the next week is to begin the prototyping to determine concrete implementation details and begin the SyRS for documentation. 