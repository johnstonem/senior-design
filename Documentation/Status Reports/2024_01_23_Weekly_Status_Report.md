# Weekly Status Report
Course: CPE 4902 002 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 23 January 2024

Term: Spring

Week: 1

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
We returned from break with the goal of developing and ordering the custom PCB's as soon as possible. We have 2 boards we're developing, which will be interfaced via a ribbon cable to reduce the amount of surface area per board. The demo is being updated from the before-break version to include more buttons and the supporting web interface.

## Status Update
- Tyler finished the initial draft of control board schematic. It still needs to be reviewed, and a PCB needs to be developed, but the basic logical connections are there.
- Ryan is still working on draft of display board schematic. It partially depended on the control board (and vice versa), and will also need a thorough review after completion.
- Charles expanded the demo to use all planned 7 buttons. The button IO is present, but not the tennis logic behind them.
- Mitchell finished the Code of Ethics and started planning out more issues / development.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|Demo Code and Hardware|1 hr|4 hrs|
|Control board schematic||||8 hrs|
|Display board schematic|||4 hrs|
|Code of Ethics|3 hrs||||3 hrs|

### Weekly Project Effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--        |--      |--     |--     |--       |
|W1         |2 hrs     |2 hrs   |2 hrs  |8 hrs  |3 hrs    |
|W2         |2 hrs     |4 hrs   |4 hrs  |6 hrs  |3 hrs    |
|W3         |1 hrs     |5 hrs   |3 hrs  |5 hrs  |5 hrs    |
|W4         |3 hrs     |3 hrs   |2 hrs  |6 hrs  |3 hrs    |
|W5         |2 hrs     |1 hr    |       |1 hr   |2 hr     |
|W6         |3 hrs     |3 hrs   |4 hrs  |5 hrs  |3 hrs    |
|W7         |2 hrs     |5 hrs   |4 hrs  |6 hrs  |3 hrs    |
|W8         |2 hrs     |2 hrs   |6 hrs  |4 hrs  |4 hrs    |
|W9         |4 hrs     |5 hrs   |11 hrs |7 hrs  |3 hrs    |
|W10        |3 hrs     |5 hrs   |2 hrs  |3 hrs  |4 hrs    |
|W11        |6 hrs     |4 hrs   |7 hrs  |5 hrs  |5 hrs    |
|W12        |3 hrs     |2 hrs   |3 hrs  | 3 hrs |2 hrs    |
|W14        |5 hrs     |5 hrs   |  |5 hrs  |5 hrs    |
|W1         |4 hrs     |4 hrs   |4 hrs  |8 hrs  |3 hrs  |
|**Total:** |42 hrs    |50 hrs  |52 hrs |69 hrs |48 hrs   |

## Discussion
This is the first week back from break, so we had to get back into "school mode". Not much development was done over break, but the cutoff deadline for work is rapidly approaching, so we want to finish up our PCBs as soon as possible to begin ordering and constructing the hardware.

This development includes a lot of Kicad work for the logical schematic and the PCB layout. We got experience last semester with this process (Embedded Systems Fabrication), so we feel confident in the work. It is still a risky situation, since it's one of the largest purchases we'll be making, but if the boards look good then we should be set in terms of hardware. The Schematic and PCB layout will be reviewed by others who didn't create them to get fresh eyes on it.

Otherwise, on the software side, we want to have a solid demo for the week 5 presentation. We'd like to include using the two boards again, hopefully hooked up to a portable battery, and with more button functionality on the web. We will be making additions to the web interface as well as the C logic to make the demo more akin to a real game.

## Plan Update
- Finish the control board schematic / PCB - *Tyler*
- Finish the display board schematic / PCB - *Ryan*
- Review the schematics / PCBs - *Mitchell, John Paul, Charles*
- Set up and test WebSockets / SPIFF on the ESP32s - *Charles*
- Make updates to the website display (listed in issue on Gitlab) - *John Paul*
- Finish 3D model of case - *Mitchell*

## Conclusion
We are planning to finish (and hopefully order) PCBs by the end of the week, and will continue to work on the supporting software in the meantime.