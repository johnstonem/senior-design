# Weekly Status Report
Course: CPE 4901 004 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 14 NOV 2023

Term: Fall

Week: 11

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
After giving our presentation, we developed the web interface development, did PCB component research, and met with Mr. Matousek, who signed off on our current design work. Challenges involve troubleshooting wifi server issues and addressing power resistor requirements for a battery test.

## Status Update
- We all attended the first 3 presentations on Wednesday, 11/8. We then presented at 2:20pm.
- Charles found that the demo code didn't work and needs to do further testing to fix the wifi server on the mesh network.
- Mitchell worked on the UX drawings and started the 3D model, and met with Mr. Matousek to get some feedback.
- JP designed the web interface and wrote code the display.
- Ryan did parts research on the PCB components. He needs overall dimensions to place traces and design the PCB layout.
- Tyler made a demo program to use Buttons to toggle LEDs
- Tyler started a battery test, needs power resistors to withstand battery output.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|Battery Testing||||2 hrs|
|Software|4 hrs|2 hrs||1 hr|
|Presentation Prep & Delivery|2 hrs|2 hrs|2 hrs|2 hrs|2 hrs|
|Case Design|||||2 hr|
|PO Meeting|||||1 hr|
|PCB Parts Research|||2 hrs|
|Parts testing|||3 hrs|

### Weekly Project Effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--        |--      |--     |--     |--       |
|W1         |2 hrs     |2 hrs   |2 hrs  |8 hrs  |3 hrs    |
|W2         |2 hrs     |4 hrs   |4 hrs  |6 hrs  |3 hrs    |
|W3         |1 hrs     |5 hrs   |3 hrs  |5 hrs  |5 hrs    |
|W4         |3 hrs     |3 hrs   |2 hrs  |6 hrs  |3 hrs    |
|W5         |2 hrs     |1 hr    |       |1 hr   |2 hr     |
|W6         |3 hrs     |3 hrs   |4 hrs  |5 hrs  |3 hrs    |
|W7         |2 hrs     |5 hrs   |4 hrs  |6 hrs  |3 hrs    |
|W8         |2 hrs     |2 hrs   |6 hrs  |4 hrs  |4 hrs    |
|W9         |4 hrs     |5 hrs   |11 hrs |7 hrs  |3 hrs    |
|W10        |3 hrs     |5 hrs   |2 hrs  |3 hrs  |4 hrs    |
|W10        |6 hrs     |4 hrs   |7 hrs  |5 hrs  |5 hrs    |
|**Total:** |30 hrs    |39 hrs  |45 hrs |53 hrs |38 hrs   |

## Discussion
We all attended last weeks presentations, and successfully gave our own. 

Charles encountered a hurdle with the demo code; it seems the wifi server on the mesh network isn't cooperating. He'll be looking into other solutions or fixes to the current.

JP designed a basic web interface and coded the display to work with a web server on an ESP32. We should be able to do some testing with this to see output of current stats. 

To move forward on the display work, Mitchell needs to finish a 3D model of the scoreboard so that Ryan has dimensions to place components. After meeting with Mr. Matousek, he signed off on our current designs, so we know the right way to move forward.

Last weeks test with the battery resulted in some burnt resistors. Tyler needs to get stronger resistors to actually test the capacity of the battery.

## Plan Update
- Case / Display - *Mitchell*
    - Create Basic 3D Model for scoreboard housing
    - Get dimensions of board / 7 segment / LEDs to Ryan
- Battery burn / charge test - *Ryan & Tyler*
    - Upon arrival, test battery capacity
    - Additionally, test to ensure the 12V adapter functions
    - Test overall power loss due to regulator
- Networking - *Charles*
    - Find and decide on a development plan for integrating the network(s).
- Tennis Functionality - *JP*
    - Buttons that increment / decrement.
        - Hook up to interrupts that update.
    - Array to hold the current games.
    - Array to hold the current sets.
    - Print game status to console.
- Full Demo Next Week (Start 11/21)
    - Integrate the components
        - Mesh communication
        - Update web server
        - Tennis Code

## Conclusion
This week's work will move us towards a demo, which we will be compiling next week. We're finding results on the battery and network to move forward.