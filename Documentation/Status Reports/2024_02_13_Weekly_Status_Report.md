# Weekly Status Report
Course: CPE 4902 002 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 13 February 2024

Term: Spring

Week: 4

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
This week's focus has been on developing a tangible demo to show the audience during the spring update (February 14th, 2024),
as well as a professional slide deck that communicates our updates. 
This included a lot of effort put behind the demo (hardware and software) and wrapping up the PCB design for review.

## Status Update
- Mitchell, Charles and Ryan worked to create the presentation slide deck
- Everyone practice the presentation for the spring update.
- Tyler and Charles put in a lot of hours in creating the demo
- Ryan completed the Display PCB and began 3D modeling light isolators
- Mitchell and Charles worked to update the current software for the ESP32s.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|Demo Construction||10 hrs||20 hrs|
|Final Display PCB|||3 hrs|
|3D modeling - Light isolators|||2 hrs|
|Demo C code||4 hrs|||3 hrs|
|Presentation|1 hr|1 hr|1 hr|1 hr|3 hrs|

### Weekly Project Effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--     |--     |--     |--     |--     |
|W1         |2 hrs  |2 hrs  |2 hrs  |8 hrs  |3 hrs  |
|W2         |2 hrs  |4 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W3         |1 hrs  |5 hrs  |3 hrs  |5 hrs  |5 hrs  |
|W4         |3 hrs  |3 hrs  |2 hrs  |6 hrs  |3 hrs  |
|W5         |2 hrs  |1 hr   |       |1 hr   |2 hr   |
|W6         |3 hrs  |3 hrs  |4 hrs  |5 hrs  |3 hrs  |
|W7         |2 hrs  |5 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W8         |2 hrs  |2 hrs  |6 hrs  |4 hrs  |4 hrs  |
|W9         |4 hrs  |5 hrs  |11 hrs |7 hrs  |3 hrs  |
|W10        |3 hrs  |5 hrs  |2 hrs  |3 hrs  |4 hrs  |
|W11        |6 hrs  |4 hrs  |7 hrs  |5 hrs  |5 hrs  |
|W12        |3 hrs  |2 hrs  |3 hrs  | 3 hrs |2 hrs  |
|W13        |<td>Thanksgiving Break</td>
|W14        |5 hrs  |5 hrs  |       |5 hrs  |5 hrs  |
|W15        |3 hrs  |3 hrs  |3 hrs  |3 hors |3 hrs  |
|W1         |4 hrs  |4 hrs  |4 hrs  |8 hrs  |3 hrs  |
|W2         |6 hrs  |4 hrs  |5 hrs  |6 hrs  |6 hrs  |
|W3         |3 hrs  |2 hrs  |5 hrs  |6 hrs  |7 hrs  |
|W4         |1 hr   |15 hrs  |6 hrs  |20 hrs |6 hrs  |
|**Total:** |55 hrs |72 hrs  |76 hrs |106 hrs |63 hrs |

## Discussion
We focussed a lot this week on constructing a demo and finishing up the PCB display board design. 
As it is a demo, it has some of the functionality templated and a rough shape, 
The demo has a fully functioning display with some example button functions implemented, including
- Increasing & decreasing scores
- Switching sides
- Zero-ing out scores to reset

We also set up the presentation slide deck to be able to present an update and the demo on 2/14/2024.
This includes time spent crafting the deck and practicing for speaking.

Lastly the PCBs have been finished in the design phase and are currently under review by Mitchell, Tyler, and Charles.
Once the PCBs have had a final review, both designs will be ordered at the same time to save on shipping.

## Plan Update
- Present the demo and slideshow update.
- Order boards
- Finish Web Socket and SPIFF code for seamless integration
- 3D model the light isolators
- 3D model the case to match the PCB specifications

## Conclusion
We mostly aim to practice for the presentation. Then we plan to order the PCBs and prepare for their arrival, including remaining software development.
