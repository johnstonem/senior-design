# Weekly Status Report
Course: CPE 4901 004 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 07 NOV 2023

Term: Fall

Week: 10

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
This week we mostly focused on getting a draft of our presentation to prepare for the Fall presentation, and researched feasibility of mesh network extending wifi access point.

## Status Update
- Charles researched bridge networks, which combine separate networks into one. Through testing, it was found that the web server could likely be brought onto the mesh network through this. Further validation needs to be done.
- Mitchell made some drafts of the product scoreboards. He got initial feedback from peers, and will send it to the 
- Ryan wired up a majority of the display, testing it today/tomorrow.
- Tyler researched battery functioning and put in a parts order for the battery and extra ESP32s for development.
- Every added slides to the slide deck for the presentation one Wednesday.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|Battery Research||||2 hrs|
|Display Wiring|||2 hrs|
|System Drawings|||||1 hr|
|Network Design|2 hrs|3 hrs|
|Presentation|1 hr|1 hr||1 hr|2 hrs

### Weekly Project Effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--        |--      |--     |--     |--       |
|W1         |2 hrs     |2 hrs   |2 hrs  |8 hrs  |3 hrs    |
|W2         |2 hrs     |4 hrs   |4 hrs  |6 hrs  |3 hrs    |
|W3         |1 hrs     |5 hrs   |3 hrs  |5 hrs  |5 hrs    |
|W4         |3 hrs     |3 hrs   |2 hrs  |6 hrs  |3 hrs    |
|W5         |2 hrs     |1 hr    |       |1 hr   |2 hr     |
|W6         |3 hrs     |3 hrs   |4 hrs  |5 hrs  |3 hrs    |
|W7         |2 hrs     |5 hrs   |4 hrs  |6 hrs  |3 hrs    |
|W8         |2 hrs     |2 hrs   |6 hrs  |4 hrs  |4 hrs    |
|W9         |4 hrs     |5 hrs   |11 hrs |7 hrs  |3 hrs    |
|W10        |3 hrs     |5 hrs   |2 hrs  | 3 hrs      |4 hrs    |
|**Total:** |24 hrs    |35 hrs  |38 hrs |48 hrs |33 hrs   |

## Discussion
Further review of the ESP32 Mesh Network solutions most likely requires a bridge network. We'll need to have a board dedicated to hosting a web server to display to the coach, boards making up the Mesh Network to communicate cross court, and a bridge node to connect the two. We've tested some software connections, although more testing is needed for validation that it'll work as expected.

Otherwise, we finalized the LEDs we chose to work with (surface mount, good scatter), and using that we ballpark-ed the power requirements. A battery was found that had 10 Ah at 1A/12V, so we ordered it and will do some practical testing to find the actual battery limits.

We drafted a basic slide deck for our presentation on wednesday, as well as some drawings for the general user interface, which we sent out to the coach and Dr. Durant for feedback.

## Plan Update
- Finish / deliver Presentation - *Everyone*
- Case / Display - *Mitchell*
    - Draft Drawings, send to coach matousek to get feedback.
    - Create Basic 3D Model for scoreboard housing
- Fully working LED 7 Segment display - *Ryan*
    - Order by end of week (11/12)
- Battery burn / charge test 
    - Upon arrival, test battery capacity
    - Additionally, test to ensure the 12V adapter functions
    - Test overall power loss due to regulator
- Code
    - Cross communication of updates - *Charles*
    - Web Server - *JP*
    - Basic example working w/ 7 segment. - *Tyler*
        - Buttons to increment / decrement?

## Conclusion
Our top priority is to prepare for and deliver the presentation this Wednesday. We have a few slides to clean up, and want to dry run it. Otherwise, we want to develop a basic prototype that uses ESP32's with buttons and a display to see the basic functionality and be able to demo it.