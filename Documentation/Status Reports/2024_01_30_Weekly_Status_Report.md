# Weekly Status Report
Course: CPE 4902 002 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 30 January 2024

Term: Spring

Week: 2

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
Week 2 was mostly dedicated to hardware design and review of both boards and the assigned memo for the Code of Ethics. PCBs are nearly ready to be ordered in the upcoming week, after the designs are reviewed. 

## Status Update
- Mitchell and Charles independently reviewed the Control Board Schematic, which lead to minor adjustments to the file to best match documentation. [Current Draft](/Hardware/Senior_Design_MCU_Board_V4/Senior_Design_MCU_Board.kicad_sch)
- Mitchell and Charles walked through a SPIFFs walkthrough and integrated it with the mesh code, but failed to get the WebSocket code to work fully. [Code here](/ESP32%20Arduino%20Code/SPIFF_Test/)
- Tyler finished the current draft the control board PCB. Charles has reviewed it, and Mitchell needs to review. Current active revision is V5. Continued work will be related to changes recommended from other teammates as well as assisting with Ryan's development of the display board.(/Hardware/Senior_Design_MCU_Board_V4/Senior_Design_MCU_Board.kicad_pcb)
- Ryan finished the current draft of the display board schematic, which needs to be reviewed for logical consistency.
- Ryan is finishing the display board PCB.
- Ryan, Tyler, Charles, and JP finished their Code of Ethics assignment.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|SPIFFs / Websocket Code||2 hrs|||3 hrs|
|Control Board PCB||||TYLER HRS|
|Control Board Schematic Review||CHARLES HRS|||2 hrs|
|Display Board PCB|||RYAN HRS|
|Website Updates|JP HOURS|
|Code of Ethics|JP HRS|CH HRS|RY HRS|TY HRS|
|3D Modeling|||||1 hr|

### Weekly Project Effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--        |--      |--     |--     |--       |
|W1         |2 hrs     |2 hrs   |2 hrs  |8 hrs  |3 hrs    |
|W2         |2 hrs     |4 hrs   |4 hrs  |6 hrs  |3 hrs    |
|W3         |1 hrs     |5 hrs   |3 hrs  |5 hrs  |5 hrs    |
|W4         |3 hrs     |3 hrs   |2 hrs  |6 hrs  |3 hrs    |
|W5         |2 hrs     |1 hr    |       |1 hr   |2 hr     |
|W6         |3 hrs     |3 hrs   |4 hrs  |5 hrs  |3 hrs    |
|W7         |2 hrs     |5 hrs   |4 hrs  |6 hrs  |3 hrs    |
|W8         |2 hrs     |2 hrs   |6 hrs  |4 hrs  |4 hrs    |
|W9         |4 hrs     |5 hrs   |11 hrs |7 hrs  |3 hrs    |
|W10        |3 hrs     |5 hrs   |2 hrs  |3 hrs  |4 hrs    |
|W11        |6 hrs     |4 hrs   |7 hrs  |5 hrs  |5 hrs    |
|W12        |3 hrs     |2 hrs   |3 hrs  | 3 hrs |2 hrs    |
|W13        |<td colspan=2>Thanksgiving Break |
|W14        |5 hrs     |5 hrs   |       |5 hrs  |5 hrs    |
|W15        |3 hrs     |3 hrs   |3 hrs  |3 hors |3 hrs    |
|W1         |4 hrs     |4 hrs   |4 hrs  |8 hrs  |3 hrs    |
|W2         |          |        |       |6 hrs       |6 hrs    |
|**Total:** |45 hrs    |53 hrs  |55 hrs |72 hrs |57 hrs   |

## Discussion
The code of Ethics was assigned to be completed individually and submitted by the end of week 2. Mitchell completed it during week 1 and Charles, Ryan, Tyler, and JP have submitted the document to Dr. Durant during this week.

On the hardware side, we are pushing to get done with our design of the PCBs so we can order them soon. The control board had it's schematic designed by Tyler, and it was reviewed by Charles and Mitchell with minor adjustments. The PCB was also designed and reviewed, though another check is probably needed to be safe. The display board's schematic was completed by Ryan, and he is working to finish the PCB. After the design is complete, it will be reviewed by Mitchell, Tyler, and Charles.

On the software side, Mitchell and Charles implemented using SPIFFs, which is the ESP32 file storage system, in order to read/write files on the device, which would be good for hosting to transmit to another device. The tutorial also included using WebSockets, but it so far has not been successful. JP is still working to update the Website with sponsor feedback. 

Another improvement we're looking into is to put an ESP32 into "Low Power Mode" to conserve energy resources, since the device likely doesn't need to be running at too high of a clock rate for our purposes.

## Plan Update
Due by the end of week 3:
- Review the control board PCB - *Mitchell*
- Finish the display board schematic / PCB - *Ryan*
- Review the display schematics / PCBs - *Mitchell, Charles, and Tyler*
- Order the PCBs *Tyler*
- Order parts for making MCU board and parts for a prototype

Due by the next Advisor Meeting with Durant:
- Set up and test WebSockets with SPIFF on the ESP32s - *Charles*
- Make updates to the website display (listed in issue on 
Gitlab) - *John Paul*
- Investigate ESP32 Low Power Mode *Mitchell*

## Conclusion
We plan to finish up schematic/pcb design this week in order to purchase them soon, and the software design is being concurrently developed.