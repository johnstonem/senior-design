# Weekly Status Report
Course: CPE 4902 002 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 20 February 2024

Term: Spring

Week: 5

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
With a functional demo, the spring update senior design presentation went well.
Additionally, the PCBs have been ordered an are on their way, and some software tasks have begun for the interim.

## Status Update
- Mitchell, Charles and Tyler reviewed the display PCB
- Everyone helped give the spring update presentation
- Ryan continued to 3D model the light isolators
- Tyler ordered the PCBs on 2/16
- Mitchell successfully integrated WebSocket functionality in the code.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|Review Display PCB||1 hrs||2 hrs|2 hrs|
|3D modeling - Light isolators|||2 hrs|
|WebSocket Code|||||4 hrs|
|Presentation|2 hr|2 hr|2 hr|2 hr|2 hrs|

### Weekly Project Effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--     |--     |--     |--     |--     |
|W1         |2 hrs  |2 hrs  |2 hrs  |8 hrs  |3 hrs  |
|W2         |2 hrs  |4 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W3         |1 hrs  |5 hrs  |3 hrs  |5 hrs  |5 hrs  |
|W4         |3 hrs  |3 hrs  |2 hrs  |6 hrs  |3 hrs  |
|W5         |2 hrs  |1 hr   |       |1 hr   |2 hr   |
|W6         |3 hrs  |3 hrs  |4 hrs  |5 hrs  |3 hrs  |
|W7         |2 hrs  |5 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W8         |2 hrs  |2 hrs  |6 hrs  |4 hrs  |4 hrs  |
|W9         |4 hrs  |5 hrs  |11 hrs |7 hrs  |3 hrs  |
|W10        |3 hrs  |5 hrs  |2 hrs  |3 hrs  |4 hrs  |
|W11        |6 hrs  |4 hrs  |7 hrs  |5 hrs  |5 hrs  |
|W12        |3 hrs  |2 hrs  |3 hrs  | 3 hrs |2 hrs  |
|W13        |<td>Thanksgiving Break</td>
|W14        |5 hrs  |5 hrs  |       |5 hrs  |5 hrs  |
|W15        |3 hrs  |3 hrs  |3 hrs  |3 hors |3 hrs  |
|W1         |4 hrs  |4 hrs  |4 hrs  |8 hrs  |3 hrs  |
|W2         |6 hrs  |4 hrs  |5 hrs  |6 hrs  |6 hrs  |
|W3         |3 hrs  |2 hrs  |5 hrs  |6 hrs  |7 hrs  |
|W4         |1 hr   |15 hrs |6 hrs  |20 hrs |6 hrs  |
|W5         |       |3 hrs  |4 hrs  |4 hrs  |8 hrs  |  
|**Total:** |55 hrs |75 hrs |80 hrs |110 hrs|71 hrs |

## Discussion
The Spring presentation took up the central part of week 5, which went well. We had practiced it before hand, and most of the delivery seemed to go okay. We need to review feedback whenever it's available.

Last week, the PCBs were finished were under review by Mitchell, Tyler, and Charles. Some slight modifications were made (see the difference in PCB / Schematic between V1 and V2.5 of the display board), but otherwise the board passed review. Both boards were ordered on Friday, 2/16/2024.

In the meantime, software development has gone well. Web sockets were introduced in the code and SPIFFs was taken full advantage of to host the website code. The Web Sockets improved the user interface with a steady connection, no manual refresh needed. Some updates to the web interface still need to be made, such as adding the set score information and adding the ability to change the player names. Otherwise, we should prepare the software for the incoming hardware, ensuring our software maps to the pins before flashing.

## Plan Update
- 3D model the light isolators
- 3D model the case to match the PCB specifications
- Collect / order missing components for the PCB assembly - Tyler
- Clean up Display Schematic - Tyler
- Add set indicators to the website
- Use web socket requests to update names and save to a file in SPIFFs.
- Draft the Marketing booklet entry.

## Conclusion
With the PCBs ordered, we have to prepare for their arrival. Software updates will need to be made to ensure the pins align with the schematics, and any missing components will need to be ordered.