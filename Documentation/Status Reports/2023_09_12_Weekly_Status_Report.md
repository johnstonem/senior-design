# Weekly Status Report
Course: CPE 4901 004

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 12 SEP 2023

Term: Fall

Week: 2

## Executive Summary
This week we met with our client for the first time, where we got a lot of specification on project requirements.
Using those updated details, we had to revise our Detailed Proposal and Development Plan to include those requirements.
Research on potential methods and components was done and the plan was formalized and written up.

## Status Update
- Detailed Proposal and Development Plan was written up and sent to Dr. Durant to look over.
- Project Deliverables were more rigorously defined by client.
- Team agreement on development plan moving forward.
- Planned ahead for Technical Concept Investigation report.
- Introductory research on some proposed technical solutions.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|Development Plan Write up|1 hr|2 hrs|2 hrs||1 hr|
|Client Meeting|1 hr|1 hr|1 hr|1 hr||
|Wednesday Team Meeting||1 hr|1 hr|1 hr|1 hr|
|Component Research||||4 hrs||
|Week 3 planning|||||1 hr|

### Cumulative Project Effort
|Week|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|W1|2 hrs|2 hrs|2 hrs|8 hrs|3 hrs|
|W2|2 hrs|4 hrs|4 hrs|6 hrs|3 hrs|

## Discussion
One of the most important times of this past week was the meeting with our client for the project. He helped us to define our project more to his neccessities.

### Core Deliverables
Our client needs tennis scoreboards that will be available at each course. These scoreboards should have visual displays to indicate to the players and fans the current state of the game. As well, the scoreboards should be able to link somehow so that the coach can view every game's state in a single location, such as on an app or website. Finally, the game's data should be recorded so it can be easily and reliably accessed and transfered to an online site for data tracking.


## Plan Update
Milestones: We all will be doing research into new avenues of completing our core deliverables. While some research into technologies has already been completed, in the next 3 weeks we will be exploring new approaches. Each member should contribute to at least one approach's development.

## Questions
- For the Technology and Concept Investigation Report, what exactly is implied by "A monolithic design is not acceptable"? Simplified?

## Conclusion
Using the updated core deliverables, our key priority for the next week is to come up with new approaches for our development and begin research on technologies to use for each approach.
