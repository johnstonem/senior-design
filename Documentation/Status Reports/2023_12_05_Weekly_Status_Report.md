# Weekly Status Report
Course: CPE 4901 004 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 5 DEC 2023

Term: Fall

Week: 15

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
We successfully developed a demo with 2 boards, using both hardware and software components, in order to simulate a minimum viable product for our product.

## Status Update
- JP, Mitchell, Charles and Tyler finished developing the [Current Code](../../jp_mesh/), used in the simple demo. 
- Tyler and Ryan are still working on the PCB models. The schematics need to be made for the LED array, as well as the buttons, as we're planning on doing those as separate boards.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|Demo Work|5 hrs|5 hrs||5 hrs|5hrs|

### Weekly Project Effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--        |--      |--     |--     |--       |
|W1         |2 hrs     |2 hrs   |2 hrs  |8 hrs  |3 hrs    |
|W2         |2 hrs     |4 hrs   |4 hrs  |6 hrs  |3 hrs    |
|W3         |1 hrs     |5 hrs   |3 hrs  |5 hrs  |5 hrs    |
|W4         |3 hrs     |3 hrs   |2 hrs  |6 hrs  |3 hrs    |
|W5         |2 hrs     |1 hr    |       |1 hr   |2 hr     |
|W6         |3 hrs     |3 hrs   |4 hrs  |5 hrs  |3 hrs    |
|W7         |2 hrs     |5 hrs   |4 hrs  |6 hrs  |3 hrs    |
|W8         |2 hrs     |2 hrs   |6 hrs  |4 hrs  |4 hrs    |
|W9         |4 hrs     |5 hrs   |11 hrs |7 hrs  |3 hrs    |
|W10        |3 hrs     |5 hrs   |2 hrs  |3 hrs  |4 hrs    |
|W11        |6 hrs     |4 hrs   |7 hrs  |5 hrs  |5 hrs    |
|W12        |3 hrs     |2 hrs   |3 hrs  | 3 hrs |2 hrs    |
|W14        |5 hrs     |5 hrs   |  |5 hrs  |5 hrs    |
|**Total:** |38 hrs    |46 hrs  |48 hrs |61 hrs |45 hrs   |

## Discussion
The demo uses two boards to communicate board state, continously updating a web server with the current info located on the mesh network's wifi. The board has simple login credentials right now, and the website is hosted on the domain name of `tennis.local`, so no need to use an IP adress.⌈

## Plan Update
- Enjoy Winter Break.

## Conclusion
We'll be focussing on studying for finals and then taking finals next week. ⌈