# Weekly Status Report
Course: CPE 4902 002 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 20 February 2024

Term: Spring

Week: 6

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
The PCBs were received and assembly begun, with more components ordered and on their way. 
Website improvements (modifying names on the courts and seeing set scores) were made following the addition of WebSockets.

## Status Update
- Tyler and Charles began assembly of the MCU board
- Mitchell updated some more of the Website to get communication of sets and names
- Tyler Ordered more components to build more board.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|Assembly||4 hrs||4hrs|
|WebSocket Code|||||4 hrs|

### Weekly Project Effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--     |--     |--     |--     |--     |
|W1         |2 hrs  |2 hrs  |2 hrs  |8 hrs  |3 hrs  |
|W2         |2 hrs  |4 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W3         |1 hrs  |5 hrs  |3 hrs  |5 hrs  |5 hrs  |
|W4         |3 hrs  |3 hrs  |2 hrs  |6 hrs  |3 hrs  |
|W5         |2 hrs  |1 hr   |       |1 hr   |2 hr   |
|W6         |3 hrs  |3 hrs  |4 hrs  |5 hrs  |3 hrs  |
|W7         |2 hrs  |5 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W8         |2 hrs  |2 hrs  |6 hrs  |4 hrs  |4 hrs  |
|W9         |4 hrs  |5 hrs  |11 hrs |7 hrs  |3 hrs  |
|W10        |3 hrs  |5 hrs  |2 hrs  |3 hrs  |4 hrs  |
|W11        |6 hrs  |4 hrs  |7 hrs  |5 hrs  |5 hrs  |
|W12        |3 hrs  |2 hrs  |3 hrs  | 3 hrs |2 hrs  |
|W13        |<td>Thanksgiving Break</td>
|W14        |5 hrs  |5 hrs  |       |5 hrs  |5 hrs  |
|W15        |3 hrs  |3 hrs  |3 hrs  |3 hors |3 hrs  |
|W1         |4 hrs  |4 hrs  |4 hrs  |8 hrs  |3 hrs  |
|W2         |6 hrs  |4 hrs  |5 hrs  |6 hrs  |6 hrs  |
|W3         |3 hrs  |2 hrs  |5 hrs  |6 hrs  |7 hrs  |
|W4         |1 hr   |15 hrs |6 hrs  |20 hrs |6 hrs  |
|W5         |Sick   |3 hrs  |4 hrs  |4 hrs  |8 hrs  |  
|W6         |Sick   |4 hrs  |       |4 hrs  |4 hrs  |  
|**Total:** |55 hrs |79 hrs |84 hrs |114 hrs|75 hrs |

## Discussion
This past week we got our order of boards (Both the MCU and the display) and began assembling the MCU board.
Hopefully, once the MCU board is running, that can be our test bench for further improvements.
We also ordered some more components that we were missing from our other orders but are necessary for the board build.

One noticed issue we had when get got all of our components is that the ESP32s were ordered without antenna.
While this does hinder us a bit, as we currently have no method to produce the wifi needed for hosting the website,
this provides us with an opportunity to test out using alternative / exterior antenna. We will be testing and possibly placing another order.

We also found an issue with a footprint on the MCU, notably that one of the components is too small. Work is still being completed to see if the 
surface mount part can still touch the pads to connect to the board.

For on the website, one task complete was to use web socket requests to update names and save to a file in SPIFFs.
Prior to this week, all of the court names are hard coded in a file, so it wasn't modular. Now, the ESP32 will send all of the names once it had
an update to one name so ensure the ESP32 gets all of the updated changes the coach makes.
Additionally, the set information has been communicated, but there is no nice display yet for showing the sets.
Also, the current score statistics for all courts can be downloaded as a csv from the web page.

## Plan Update
- 3D model the light isolators - Ryan
- 3D model the case to match the PCB specifications - Ryan
- Assembly - Ryan, Charles, Tyler
- Add set indicators to the website - Mitchell
- Draft the Marketing booklet entry. - JP

## Conclusion
we will focus on completing the assembly of the MCU board, testing alternative antennas for wifi connectivity, 3D modelling, adding more website fixes, and completing the draft for the marketing booklet entry.