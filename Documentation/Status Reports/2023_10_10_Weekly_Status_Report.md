# Weekly Status Report
Course: CPE 4901 004 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 10 OCT 2023

Term: Fall

Week: 6

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
We wrote up the first two main sections of the SyRS and set up the prototyping. Parts were ordered for the custom LED array testing and the ESP32 system had communication for a "hello world" program.

## Status Update
- JP, Tyler, Charles - Work through and set up drivers for the ESP32 development.
- Ryan, Tyler - Compiled components to test for power relays and batteries, as well as more LEDs for the array. Parts ordered.
- Mitchell - Finished first 2 major sections of the SyRS.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|LED Array Parts Research|||4 hrs|4 hrs||
|ESP32 Dev Kit interface|3 hrs|3 hrs||1 hr||
|SyRS Work|||||3 hrs|

### Weekly Project Effort
|Week       |John Paul  |Charles    |Ryan   |Tyler  |Mitchell   |
|--         |--         |--         |--     |--     |--         |
|W1         |2 hrs      |2 hrs      |2 hrs  |8 hrs  |3 hrs      |
|W2         |2 hrs      |4 hrs      |4 hrs  |6 hrs  |3 hrs      |
|W3         |1 hrs      |5 hrs      |3 hrs  |5 hrs  |5 hrs      |
|W4         |3 hrs      |3 hrs      |2 hrs  |6 hrs  |3 hrs      |
|W5         |2 hrs      |1 hr       |       |1 hr   |2 hr       |
|W6         |3 hrs      |3 hrs      |4 hrs  |5 hrs  |3 hrs      |
|**Total:** |13 hrs     |18 hrs     |15 hrs |31 hrs |19 hrs     |

## Discussion
Ryan and Tyler worked on researching the scoreboard components.Power transistors and batteries were explored, and some parts were ordered to get metrics of power. By early next week we should have a mock up of the custom LED array and an expected power output.

Charles, Tyler, and JP began working the interface to an ESP32. This involved working through an example program to produce a "hello world" program, although there was a driver setup issue.
JP set up his phone and started to set up the template for bluetooth interface.

Mitchell worked on the Systems Requirement Specification. Got most sections from 1 and 2 completed, designated remaining sections to members.

## Plan Update

- Continued improvement from last plan update:
    - Prototyping with ESP32 Communication System - JP, Mitchell & Charles
    - Developing the Custom LED board and power options - Ryan & Tyler
    - Writing up the SyRS Report - Everyone

- For Durant:
    - What : 2.4 Major system conditions
    - Plan for next week meeting? (10/17)

## Conclusion
We are finishing up the SyRS this week and continuing our prototyping and development.