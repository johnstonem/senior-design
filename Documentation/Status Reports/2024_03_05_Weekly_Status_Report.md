# Weekly Status Report
Course: CPE 4902 002 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 05 March 2024

Term: Spring

Week: 7

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
This week, the team assembled MCU and display boards, identifying and fixing issues. They also tested a decoder chip and made revisions to PCBs, while continuing work on website updates.

## Status Update
- Tyler and Charles assembled 2 MCU boards, finding places to fix
- Ryan assembled most of a display board, finding fault in one component.
- Mitchell and Tyler tested the 7 segment decoder chip.
- Tyler made revisions to the MCU PCB
- Ryan made revisions to the Display PCB
- Mitchell drafted the marketing booklet memo.
- JP worked on implementing the history in the website.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|Assembly/debugging||8 hours|6 hours|8 hours|3 hours|
|Testing||||2 hours|3 hours|
|Revisions|||2 hours|3 hours|
|Web Updates|4 hours|

### Weekly Project Effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--     |--     |--     |--     |--     |
|W1         |2 hrs  |2 hrs  |2 hrs  |8 hrs  |3 hrs  |
|W2         |2 hrs  |4 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W3         |1 hrs  |5 hrs  |3 hrs  |5 hrs  |5 hrs  |
|W4         |3 hrs  |3 hrs  |2 hrs  |6 hrs  |3 hrs  |
|W5         |2 hrs  |1 hr   |       |1 hr   |2 hr   |
|W6         |3 hrs  |3 hrs  |4 hrs  |5 hrs  |3 hrs  |
|W7         |2 hrs  |5 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W8         |2 hrs  |2 hrs  |6 hrs  |4 hrs  |4 hrs  |
|W9         |4 hrs  |5 hrs  |11 hrs |7 hrs  |3 hrs  |
|W10        |3 hrs  |5 hrs  |2 hrs  |3 hrs  |4 hrs  |
|W11        |6 hrs  |4 hrs  |7 hrs  |5 hrs  |5 hrs  |
|W12        |3 hrs  |2 hrs  |3 hrs  | 3 hrs |2 hrs  |
|W13        |<td>Thanksgiving Break</td>
|W14        |5 hrs  |5 hrs  |       |5 hrs  |5 hrs  |
|W15        |3 hrs  |3 hrs  |3 hrs  |3 hors |3 hrs  |
|W1         |4 hrs  |4 hrs  |4 hrs  |8 hrs  |3 hrs  |
|W2         |6 hrs  |4 hrs  |5 hrs  |6 hrs  |6 hrs  |
|W3         |3 hrs  |2 hrs  |5 hrs  |6 hrs  |7 hrs  |
|W4         |1 hr   |15 hrs |6 hrs  |20 hrs |6 hrs  |
|W5         |Sick   |3 hrs  |4 hrs  |4 hrs  |8 hrs  |  
|W6         |Sick   |4 hrs  |       |4 hrs  |4 hrs  |  
|W7         |4 hrs  |8 hrs  |8 hrs  |13 hrs |6 hrs  |
|**Total:** |59 hrs |87 hrs |92 hrs |127 hrs|81 hrs |

## Discussion
After getting all the boards and components, a major push was made to assemble everything to see what faults we found in the first rendition. Multiple failure points were found in both boards, and fixes are being implemented in a follow up version.
Aside from the issues with the board, the other components on the board were tested to verify the design, and from the testing we believe the rest of the board should be functional after a full assembly.

2 of the major issues was misaligned footprints. Some of the components needed manually-designed footprints to match the datasheets, but 2 were designed incorrectly. These components were fixed by remaking the footprints and recompiling the PCBs.

Additionally, we were worried about the 3.3V top plane shorting to ground through a grounded via. We tried to space out the vias better and are ordering tented vias with the next order.

After debugging the boards to find the issues and implementing the fixes, the PCB should be in a state ready to order. They just need to be reviewed and shipped.

Otherwise, we also did some testing of the decoder chip for the display board, which we were most worried about working. The testing showed promising results for the surface mount component.

Additionally, we've continued to make improvements on the website code, starting to incorporate a history of the changes of the scoreboard states.

## Plan Update
- Fix 3D light isolator models - Ryan
- Finish adding history for the display - JP
- Add set indicator 
- Review revisions of PCB - Everyone
- Order new boards before spring break - Tyler
- Find antenna to use for ESP32 - Charles

## Conclusion
We will be reviewing, finalizing, and ordering our updated PCBs. We'll also be working on the board accessories, like the light isolators and antenna, as well as software updates like the history component.