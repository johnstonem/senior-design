# Weekly Status Report
Course: CPE 4901 004

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 26 SEP 2023

Term: Fall

Week: 4

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
We compiled all of our research and decisions into the Technology and Concept Report. Our rough draft was sent to Durant and he got us back some comments / feedback.

## Status Update
- We completed research for the HUB and scoreboard
    - Mitchell added some photos and his memory research to the tech report.
    - Ryan added his research on the Display
    - Charles added the competiting product and some of the scoreboard research.
    - Tyler added to scoreboard and some LED display stats.
    - JP added to the overview/conclusion, uncertain about research.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|Competing Products||1 hr||||
|HUB Pros/Cons|||||1 hr|
|HUB Research|||2 hr||2 hrs|
|Scoreboard Pros/cons||2 hr||1 hr||
|Scoreboard Research||||3 hrs|
|Parts Procurement||||2 hrs|

### Weekly Project Effort
|Week|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|W1|2 hrs|2 hrs|2 hrs|8 hrs|3 hrs|
|W2|2 hrs|4 hrs|4 hrs|6 hrs|3 hrs|
|W3||5 hrs|3 hrs|5 hrs|5 hrs|
|W4||3 hrs|2 hrs|6 hrs|3 hrs|
|**Total:**||14 hrs|11 hrs|25 hrs|14 hrs|

## Discussion

## Plan Update
- Ryan will develop a basic schematic of his LED array board.
    - When the LEDs come in, they will be tested
- Charles will write an email to the coach and work on starting the schematic of our scoreboards.
    - When parts come in, begin developing the prototype boards.ki  
- Tyler will handle the incoming parts and then work out how the HUB will communicate with 7 different Scoreboards.
- Mitchell will finish up the Tech & Concept report, adding interconnectivity research and any updates from our advisor.
- JP will begin developing the software for the ESP32 for communicating over bluetooth to an apple phone.

## Conclusion
We plan to be done with the Tech Report by this Friday. With ordered parts, we also plan to begin some prototyping and planning for development.
