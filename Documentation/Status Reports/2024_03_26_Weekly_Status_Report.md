# Weekly Status Report
Course: CPE 4902 002 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 26 March 2024

Term: Spring

Week: 9

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
The board communication issues were identified and resolved, and the software was further tested on the boards to verify IO pins. Work is continuing on the simulation, with successful flashing of simulated ESP32.

## Status Update
- Get the WOKWI simulator to boot up the devices - JP
- the software was flashed to the MCU and debugged - Mitchell / Tyler
- Assemble duplicate MCU boards for mesh testing - Tyler / Charles
- Assembly duplicate Display board - Ryan 

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|Assemble Duplicate MCU board||5 hrs||5 hrs|
|Assemble Duplicate Display board|||6 hrs|
|Flash and debug MCU|||||4 hrs|
|Order more components||||1 hr|
|WOKWI Simulation|5 hrs|


### Weekly Project Effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--     |--     |--     |--     |--     |
|W1         |2 hrs  |2 hrs  |2 hrs  |8 hrs  |3 hrs  |
|W2         |2 hrs  |4 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W3         |1 hrs  |5 hrs  |3 hrs  |5 hrs  |5 hrs  |
|W4         |3 hrs  |3 hrs  |2 hrs  |6 hrs  |3 hrs  |
|W5         |2 hrs  |1 hr   |       |1 hr   |2 hr   |
|W6         |3 hrs  |3 hrs  |4 hrs  |5 hrs  |3 hrs  |
|W7         |2 hrs  |5 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W8         |2 hrs  |2 hrs  |6 hrs  |4 hrs  |4 hrs  |
|W9         |4 hrs  |5 hrs  |11 hrs |7 hrs  |3 hrs  |
|W10        |3 hrs  |5 hrs  |2 hrs  |3 hrs  |4 hrs  |
|W11        |6 hrs  |4 hrs  |7 hrs  |5 hrs  |5 hrs  |
|W12        |3 hrs  |2 hrs  |3 hrs  | 3 hrs |2 hrs  |
|W13        |<td>Thanksgiving Break</td>
|W14        |5 hrs  |5 hrs  |       |5 hrs  |5 hrs  |
|W15        |3 hrs  |3 hrs  |3 hrs  |3 hors |3 hrs  |
|W1         |4 hrs  |4 hrs  |4 hrs  |8 hrs  |3 hrs  |
|W2         |6 hrs  |4 hrs  |5 hrs  |6 hrs  |6 hrs  |
|W3         |3 hrs  |2 hrs  |5 hrs  |6 hrs  |7 hrs  |
|W4         |1 hr   |15 hrs |6 hrs  |20 hrs |6 hrs  |
|W5         |Sick   |3 hrs  |4 hrs  |4 hrs  |8 hrs  |  
|W6         |Sick   |4 hrs  |       |4 hrs  |4 hrs  |  
|W7         |4 hrs  |8 hrs  |8 hrs  |13 hrs |6 hrs  |
|W8         |6 hrs  |3 hrs  |2 hrs  |10 hrs |2 hrs  |
|W9         |5 hrs  |5 hrs  |6 hrs  |6 hrs  |4 hrs  |
|**Total:** |65 hrs |90 hrs |94 hrs |137 hrs|83 hrs |

## Discussion
During the senior design meeting last week, we had hooked up the MCU to a single display board. While the display board lit up, it didn't respond to the input from the buttons, even though the software indicated that the logic was correctly firing, and we could measure the correct signals at the decoder pins. 

After debugging the system, it was found that the decoder chip needed inputs that had voltages on par with the supplied voltage, $V_S$. We were supplying 12 volts, but the logic pins only had 3.3 volts, which is why the decoder didn't switch: we would've needed at least 6 volts to trigger the decoder to act. 

We had 2 paths forward without redesigning the board. We could:
- scrape off the 12V trace and wire a short to 3.3V
- lower the input voltage from the battery from 12V to 5V.

We chose to lower the battery output voltage to 5V to keep most of the functionality the same. This lowers the brightness a bit, which can be accounted for by modifying the connected resistor values. This will be done in future work.

After resolving the connection, we were able to properly get output and inputs to register on both the MCU board and the display board. The IO pins were set by mapping the circuitry pins to the ESP32 IO pins, and then verifying their functionality by hooking up interrupts to the buttons and altering the display. Because the debounce chip has internal pull ups, the IO pins only had to be set as INPUTs for the buttons and OUTPUTs for the lights.

To demonstrate the full functionality of the MVP, we assembled the board with the prior enclosure and constructing a duplicate of the display board to get a functional demo, although some pieces need updates once new parts are in, like longer ribbon cables and clicky buttons.

Lastly, work has been done to introduce our code into a simulation with the use of WOKWI. We are able to flash our program onto the simulated board (we know because we can see our programmed print statements). However, the simulated environment doesn't have a SPIFFs partition by default, so the website files are unable to be stored and served. Work will be done to add SPIFFs to the simulation.

## Plan Update
- Add SPIFFs to the ESP32 simulation - JP
- Create a 3D model of the case - Charles / Mitchell
- Create assembly instructions - Tyler
- Order last parts - Tyler
- Begin bill of Materials - Ryan

## Conclusion
We will assemble a better board with clicky buttons and longer ribbon cables, integrating the code into a simulation environment for testing with larger network sizes. Additionally, completing the 3D model, assembly instructions, and beginning the bill of materials will help wrapping up key components.