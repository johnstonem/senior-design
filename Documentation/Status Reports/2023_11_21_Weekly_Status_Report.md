# Weekly Status Report
Course: CPE 4901 004 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 21 NOV 2023

Term: Fall

Week: 12

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
This week involved consolidating the networking requirements, testing the battery we received, and drafting a 3D model of the scoreboard.

## Status Update
- Charles tested the mesh network's ability to host a web server without a separate network.
- Mitchell created a rough draft of the 3D model for the board to get an initial impression.
- Ryan and Tyler did some tests against the usability of the battery we ordered last week.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|Network Testing||2 hrs|
|3D Modeling|||||2 hrs|
|Battery testing|||2 hrs|2 hrs|
|Part Research|||1 hr|1 hr
|Server updates|

### Weekly Project Effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--        |--      |--     |--     |--       |
|W1         |2 hrs     |2 hrs   |2 hrs  |8 hrs  |3 hrs    |
|W2         |2 hrs     |4 hrs   |4 hrs  |6 hrs  |3 hrs    |
|W3         |1 hrs     |5 hrs   |3 hrs  |5 hrs  |5 hrs    |
|W4         |3 hrs     |3 hrs   |2 hrs  |6 hrs  |3 hrs    |
|W5         |2 hrs     |1 hr    |       |1 hr   |2 hr     |
|W6         |3 hrs     |3 hrs   |4 hrs  |5 hrs  |3 hrs    |
|W7         |2 hrs     |5 hrs   |4 hrs  |6 hrs  |3 hrs    |
|W8         |2 hrs     |2 hrs   |6 hrs  |4 hrs  |4 hrs    |
|W9         |4 hrs     |5 hrs   |11 hrs |7 hrs  |3 hrs    |
|W10        |3 hrs     |5 hrs   |2 hrs  |3 hrs  |4 hrs    |
|W11        |6 hrs     |4 hrs   |7 hrs  |5 hrs  |5 hrs    |
|W12        |     |2 hrs   |3 hrs  | 3 hrs |2 hrs    |
|**Total:** |30 hrs    |41 hrs  |48 hrs |56 hrs |40 hrs   |

## Discussion
Prior network tests focused on using a mesh network to communicate between nodes and a separate network to communicate to the web interface on the user's phone. 
This week, using only a mesh network was tested, hosting the web client on the mesh network itself. This was done by spacing out multiple nodes on the mesh network, connecting to a base node (which didn't have the server code directly on it), and trying to access the server. A local DNS server was also implemented, so the server was able to be accessed at `tennis.local`. The test showed that the mesh network is able to communicate the server to the entire mesh, and that the client will be able to use the local name for the server.

Parts research was done on any proprietary / Amazon sourced control / distribution technologies for power to the 7 segment display. The current system for placing the 280 diodes, which are already small, there'll be a larger chance for error. By replacing the system, such as a multiplexer, we can hopefully be more reliable.

When testing the battery, we found it had a larger output but a shorter duration of capacity.
Specifically, the battery was able to output 12V at 3.6A, which was much higher than advertised. This was done with using the 12V trigger to output maximum from the battery, and ran it against 1 Ohm power resistors. It was shorter than expected, in an hour rather than multiple hours, although another test has to be done to actually make quantitative measurements.

A basic 3D model was developed in accordance with one of our original UX drawings. The original drawing is below:
![drawing](../UX%20Drawings/Scoreboard%203.png)

This drawing was signed off by Mr. Matousek, so we decided to try to emulate the design in Onshape. This product was used because we all had experience designing models in Onshape and allowed for easy sharing of models. 
Here's a screen clip of the 3D model without LEDs, 7 segment displays, or buttons:
![3D model](../UX%20Drawings/3D%20model.png)

## Plan Update
Enjoy Thanksgiving!
Don't get too hammered.

## Conclusion
This upcoming week is Thanksgiving, so we won't be spending too much time on the project. Once we get back, we'll be attempting to print the prototype model and incorporate some software pieces together.