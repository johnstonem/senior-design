# Weekly Status Report
Course: CPE 4902 002 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 06 February 2024

Term: Spring

Week: 3

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
The final revision of the MCU board is complete, awaiting the display board's completion for ordering. Additionally, parts have been ordered to create a proof-of-concept scoreboard, utilizing both the MCU and display board schematics, with enough components for three MCU boards, and the code has been adapted to the schematics, although it needs integration with website and websocket updates.

## Status Update
- Tyler completed the [final revision of the MCU board](/Hardware/Senior_Design_MCU_Board_V7/Senior_Design_MCU_Board.kicad_sch). This board is ready for order, and is waiting for the display board to be completed. 
- Tyler also ordered parts to create a proof-of-concept scoreboard on a breadboard. This task is called the "Demo Scoreboard". This parts order also contained enough parts to do a build of three MCU boards. The scoreboard will be constructed using both the MCU schematic as well as the display board schematic (both of which are completed).
- Mitchell developed the current demo code for W5, which uses the pin values from the finished MCU board. It does not integrate the updated website code, which needs to be done this week in preparation for the presentation.
- Charles and Mitchell debugged the WebSocket and SPIFF code, which was previously showing the desired website but did not update it according to the code. Now the website properly updates without needing to refresh the page, and will get the updated values from the board.
- Ryan developed the display board to match the MCU schematic. The current board can be found [here](/Hardware/Display_Board_V1/). It currently is only configured for 1 seven-segment display, and we'll need 2.
- JP worked on the website code to implement issues that were suggested by the coach. See [here](/PO_meetings/2023_11_10_matousek.md) for the coach's feedback.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|SPIFFs / Websocket Code||1 hour|||1 hour|
|Control Board Review / Update||1 hour||4 hours|2 hours|
|Display Board Development|||RYAN HRS|
|WK 5 Demo Code|||||4 hours|
|Demo Scoreboard||||2 hours||
|Website Updates|3 hours|

### Weekly Project Effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--     |--     |--     |--     |--     |
|W1         |2 hrs  |2 hrs  |2 hrs  |8 hrs  |3 hrs  |
|W2         |2 hrs  |4 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W3         |1 hrs  |5 hrs  |3 hrs  |5 hrs  |5 hrs  |
|W4         |3 hrs  |3 hrs  |2 hrs  |6 hrs  |3 hrs  |
|W5         |2 hrs  |1 hr   |       |1 hr   |2 hr   |
|W6         |3 hrs  |3 hrs  |4 hrs  |5 hrs  |3 hrs  |
|W7         |2 hrs  |5 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W8         |2 hrs  |2 hrs  |6 hrs  |4 hrs  |4 hrs  |
|W9         |4 hrs  |5 hrs  |11 hrs |7 hrs  |3 hrs  |
|W10        |3 hrs  |5 hrs  |2 hrs  |3 hrs  |4 hrs  |
|W11        |6 hrs  |4 hrs  |7 hrs  |5 hrs  |5 hrs  |
|W12        |3 hrs  |2 hrs  |3 hrs  | 3 hrs |2 hrs  |
|W13        |<td>Thanksgiving Break</td>
|W14        |5 hrs  |5 hrs  |       |5 hrs  |5 hrs  |
|W15        |3 hrs  |3 hrs  |3 hrs  |3 hors |3 hrs  |
|W1         |4 hrs  |4 hrs  |4 hrs  |8 hrs  |3 hrs  |
|W2         |6 hrs  |       |       |6 hrs  |6 hrs  |
|W3         |3 hrs  |2 hrs  |? hrs  |6 hrs  |7 hrs  |
|**Total:** |54 hrs |53 hrs  |55 hrs |86 hrs |57 hrs   |

## Discussion
We have decided to make a working prototype scoreboard with the off-the-shelf seven segment displays we purchased in fall semester. Conveniently, these seven segment displays have very similar functionality to the ones we are developing. This scoreboard will be built this week by Tyler and Charles, and the scoreboard's functionality will be shown during the presentation in week 5. The parts order made late last week contained enough parts to make three of the MCU boards, as well as create this demo scoreboard. 

The code for it was worked on by Mitchell and contains the relevant c++ code to run the boards, but needs to be updated with the updates from JP's work on the website, as well as the incorporation of WebSockets to assist concurrency fo information.

While the test bench board is being constructed, Ryan will be finishing the Display board. Good progress was made from the prior meeting, and we are more aligned on how the board is currently planned out. After review, the boards will hopefully be ready to order, but we need to finalize the schematic / PCB.

Another item we'd like to test is putting the ESP32 into low power mode. The current hypothesis is that we can put it in lower power mode, which reduces the clock rate, but hopefully not enough to disrupt the mesh network or web socket processes. This would enable us to conserve even more power in our boards to prolong the battery.

## Plan Update
Due by the next Advisor Meeting with Durant:
- Demo Scoreboard by *Charles* and *Tyler*
- Fully incorporated Demo Code *Mitchell*
- Finish Display PCB *Ryan*
- Order PCBs *Tyler*
- Experiment results for ESP32 Low Power Mode *JP*

## Conclusion
We plan to create a working demo scoreboard to use during our week 5 presentation this week, both hardware and software, order the PCBs, and experiment with putting the ESP32 in lower power mode.