# Weekly Status Report
Course: CPE 4902 002 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 23 January 2024

Term: Spring

Week: 1

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
What are the key things that were accomplished during the last week? If there was exceptional variance or corrective action, also document it here. Be sure to include the actual results space permitting. For example, “We evaluated Java and C# as possible languages for implementation” doesn't say as much as “We selected Java as the implementation language.” The reader can read further if he/she would like to know the rationale. Approximate length: 2-3 sentences

## Status Update
- List of current and recently completed milestones - brief description and status (completed, in progress, not begun, etc.) 
- Actual effort vs. estimate for each team member on each task during current period with units of hours. 
- Cumulative project effort - previous total added to effort this period equals new total for each team member. This might be a table showing values for each week and a running total for the term. Or, it could be graphical, either as a burn-down chart or plot of earned value. 
- Specific contributions made by each team member.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|||||||

### Weekly Project Effort
|Week|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|||||||
|**Total:**||||||

## Discussion
Summarize key meetings, findings, successes, and risk updates. Approximate length: variable depending upon need: 1-6 paragraphs – use subsections if warranted

## Plan Update
Milestones: when and by whom to be completed in next 1 to 2 weeks, brief but enough detail to be clear on the scope and specifics of the work.

## Conclusion
Briefly summarize how you will you be moving forward in the next week (key priorities, etc.). Approximate length: Typically 1 sentence, but should only rarely be more than 3.