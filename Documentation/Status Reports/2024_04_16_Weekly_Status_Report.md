# Weekly Status Report
Course: CPE 4902 002 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 16 April 2024

Term: Spring

Week: 12

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
We've put a focus on wrapping up the project development of the case, and the 3D model is at a place where it can be fabricated and tested against our boards.

## Status Update
- Added updates to the 3D model - Ryan & Mitchell

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|3D modelling|||4 hrs||6 hrs|

### Weekly Project Effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--     |--     |--     |--     |--     |
|W1         |2 hrs  |2 hrs  |2 hrs  |8 hrs  |3 hrs  |
|W2         |2 hrs  |4 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W3         |1 hrs  |5 hrs  |3 hrs  |5 hrs  |5 hrs  |
|W4         |3 hrs  |3 hrs  |2 hrs  |6 hrs  |3 hrs  |
|W5         |2 hrs  |1 hr   |       |1 hr   |2 hr   |
|W6         |3 hrs  |3 hrs  |4 hrs  |5 hrs  |3 hrs  |
|W7         |2 hrs  |5 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W8         |2 hrs  |2 hrs  |6 hrs  |4 hrs  |4 hrs  |
|W9         |4 hrs  |5 hrs  |11 hrs |7 hrs  |3 hrs  |
|W10        |3 hrs  |5 hrs  |2 hrs  |3 hrs  |4 hrs  |
|W11        |6 hrs  |4 hrs  |7 hrs  |5 hrs  |5 hrs  |
|W12        |3 hrs  |2 hrs  |3 hrs  | 3 hrs |2 hrs  |
|W13        |<td>Thanksgiving Break</td>
|W14        |5 hrs  |5 hrs  |       |5 hrs  |5 hrs  |
|W15        |3 hrs  |3 hrs  |3 hrs  |3 hors |3 hrs  |
|W1         |4 hrs  |4 hrs  |4 hrs  |8 hrs  |3 hrs  |
|W2         |6 hrs  |4 hrs  |5 hrs  |6 hrs  |6 hrs  |
|W3         |3 hrs  |2 hrs  |5 hrs  |6 hrs  |7 hrs  |
|W4         |1 hr   |15 hrs |6 hrs  |20 hrs |6 hrs  |
|W5         |Sick   |3 hrs  |4 hrs  |4 hrs  |8 hrs  |  
|W6         |Sick   |4 hrs  |       |4 hrs  |4 hrs  |  
|W7         |4 hrs  |8 hrs  |8 hrs  |13 hrs |6 hrs  |
|W8         |6 hrs  |3 hrs  |2 hrs  |10 hrs |2 hrs  |
|W9         |5 hrs  |5 hrs  |6 hrs  |6 hrs  |4 hrs  |
|W10        |6 hrs  |2 hrs  |3 hrs  |8 hrs  |6 hrs  |
|W11        |3 hrs  |3 hrs  |3 hrs  |4 hrs  |3 hrs  |
|W12        |       |       |4 hrs  |       |6 hrs  |
|**Total:** |74 hrs |95 hrs |104 hrs|139 hrs|98 hrs |

## Discussion
The current model for the case can be found [here](https://cad.onshape.com/documents/e1a26b3d9c59e44e8335c20b/w/848112aac7653ba4e4257da2/e/7ffceb83b5eebe99adb6bb4b). Updates that were made to the model include:
- Making the case small enough to be printed on a standard printer at MSOE
- Reinforcing the walls, making them thicker
- Adding overhangs between pieces to join the pieces of the case together
- Rearranging the layout of the PCBs to match the expected layout
- Notches were added for placing in the acrylic front plate
- The light isolators are separate components/ models

With the updates to the model, the case should be able to be printed and joined with the PCBs, although functional testing will need to be done on some features, such as ensuring the holes line up and can grip the self-threading screws as intended.

Moving forward, we need to begin collecting all of our documentation in parallel to the construction of the boards. This includes both the poster work and the formal report writeup.

## Plan Update
- Print case & make updates - Mitchell & Ryan
- Poster Draft (Submitted Friday) - Charles & Tyler
- Finalize the Simulation - JP

## Conclusion
We play to continue wrapping up the development, including printing the case, drafting and submitting the poster, and beginning documentation cleanup.