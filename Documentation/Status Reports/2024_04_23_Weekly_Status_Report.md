# Weekly Status Report
Course: CPE 4902 002 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 23 April 2024

Term: Spring

Week: 13

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
The updated 3D modeled case's components have been printed, and there was progress on thorough documentation including reports on hardware and software, as well as the project poster.

## Status Update
- Print case & make updates - Mitchell & Ryan
- Hardware Documentation - Tyler
- Software Documentation - Charles
- Poster Draft - JP

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|3D printing|||5 hours|||
|3D modeling|||1 hour||3 hours|
|Hardware Documentation||||4 hours|1 hour|
|Software Documentation||3 hours|1 hour|
|Poster|2 hours|

### Weekly Project Effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--     |--     |--     |--     |--     |
|W1         |2 hrs  |2 hrs  |2 hrs  |8 hrs  |3 hrs  |
|W2         |2 hrs  |4 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W3         |1 hrs  |5 hrs  |3 hrs  |5 hrs  |5 hrs  |
|W4         |3 hrs  |3 hrs  |2 hrs  |6 hrs  |3 hrs  |
|W5         |2 hrs  |1 hr   |       |1 hr   |2 hr   |
|W6         |3 hrs  |3 hrs  |4 hrs  |5 hrs  |3 hrs  |
|W7         |2 hrs  |5 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W8         |2 hrs  |2 hrs  |6 hrs  |4 hrs  |4 hrs  |
|W9         |4 hrs  |5 hrs  |11 hrs |7 hrs  |3 hrs  |
|W10        |3 hrs  |5 hrs  |2 hrs  |3 hrs  |4 hrs  |
|W11        |6 hrs  |4 hrs  |7 hrs  |5 hrs  |5 hrs  |
|W12        |3 hrs  |2 hrs  |3 hrs  | 3 hrs |2 hrs  |
|W13        |<td>Thanksgiving Break</td>
|W14        |5 hrs  |5 hrs  |       |5 hrs  |5 hrs  |
|W15        |3 hrs  |3 hrs  |3 hrs  |3 hors |3 hrs  |
|W1         |4 hrs  |4 hrs  |4 hrs  |8 hrs  |3 hrs  |
|W2         |6 hrs  |4 hrs  |5 hrs  |6 hrs  |6 hrs  |
|W3         |3 hrs  |2 hrs  |5 hrs  |6 hrs  |7 hrs  |
|W4         |1 hr   |15 hrs |6 hrs  |20 hrs |6 hrs  |
|W5         |Sick   |3 hrs  |4 hrs  |4 hrs  |8 hrs  |  
|W6         |Sick   |4 hrs  |       |4 hrs  |4 hrs  |  
|W7         |4 hrs  |8 hrs  |8 hrs  |13 hrs |6 hrs  |
|W8         |6 hrs  |3 hrs  |2 hrs  |10 hrs |2 hrs  |
|W9         |5 hrs  |5 hrs  |6 hrs  |6 hrs  |4 hrs  |
|W10        |6 hrs  |2 hrs  |3 hrs  |8 hrs  |6 hrs  |
|W11        |3 hrs  |3 hrs  |3 hrs  |4 hrs  |3 hrs  |
|W12        |       |       |4 hrs  |       |6 hrs  |
|W13        |2 hrs  |3 hrs  |6 hrs  |4 hrs  |5 hrs  |
|**Total:** |76 hrs |98 hrs |110 hrs|143 hrs|103 hrs|

## Discussion
The updated model from Week 12's status update has had its components printed. The prints take over 24 hours to print, which means it's taken a while to print all of them, but we should have a fully assembled case by the status update meeting, or soon after if there are issues. Ryan has been working to print all of the components, which will allow us to put together a functional board with our custom case.

Already, a few updates have been brought up after printing, such as missing a few holes for the power switch, the charging cord, and the antenna, as well as shifting the side buttons down to prevent interaction with the PCB. These have been updated by Mitchell in the latest version of the model. The current model for the case can be found [here](https://cad.onshape.com/documents/e1a26b3d9c59e44e8335c20b/w/848112aac7653ba4e4257da2/e/7ffceb83b5eebe99adb6bb4b). 

On the documentation side, there's been work on 3 fronts: the hardware documentation, the software documentation, and the poster.

Tyler has completed a detailed report describing our approach to [hardware](../Final%20Documentation/final_doc.md). It contains details of assembly, design choices, rational for our decisions, and some statistics on the power of the boards.

Charles similarly worked on a report of the project's [software](../Engineering%20Documents/software_eng_doc.md). This one lists the libraries used, the purpose of the individual GPIO pins, and the different capabilities presented in the software solution.

JP has begun a poster for the project, which will be have a draft to look over with Dr. Durant during the meeting.

## Plan Update
- Draft the slide deck - Mitchell
- Fill in individual slides - everyone
- Finish Poster - JP
- Fully assembled scoreboard - Tyler, Ryan & Charles

## Conclusion
Working towards wrapping up the semester, we'll be focussing on a full custom assembly, with the 3D printed case and wiring setup. We'll also be working towards finalizing documentation, such as the poster we'll be presenting in the senior design show and the slide deck we'll be presenting in Week 15.