# Weekly Status Report
Course: CPE 4901 004

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 19 SEP 2023

Term: Fall

Week: 3d

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
We individually researched different aspects of our main approach. We broke our approach into 3 base components: the separate scoreboards, the central HUB, and the interconnection between the two. This week we focussed research on core issues in the HUB and the scoreboards. A 7-segment display option was ordered to do prototyping.

## Status Update
- Mitchell researched memory options for the HUB
- Ryan researched UI/Display options for the HUB data
- JP researched general powering / battery for our devices
- Tyler & Charles added to the research of different components.

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|Development Plan Update||0 hr|0 hr|0 hr|1 hr|
|HUB Research||0 hr|3 hr|0 hr|4 hrs|
|Scoreboard Research||5 hrs|0 hr|5 hrs|0 hr|

### Weekly Project Effort
|Week|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|W1|2 hrs|2 hrs|2 hrs|8 hrs|3 hrs|
|W2|2 hrs|4 hrs|4 hrs|6 hrs|3 hrs|
|W3||5 hrs|3 hrs|5 hrs|5 hrs|
|**Total:**||11 hrs|9 hrs|19 hrs|11 hrs|

## Discussion
### HUB research
JP, Mitchell, and Ryan met on 9/13 to discuss more about the HUB and it's neccesary compontents. The different avenues of development here revolved around: the power, the memory, and the UI/display. 
For the memory, the most cost-effective and reliable solution is to have a micro-SD card holder on the HUB, which provides read/write from the controller but also would allow the data to be exported onto a laptop or other device.

### Scoreboard research
Tyler and Charles met on 9/14 to discuss the hardware requirements and options for the scoreboard itself. Multiple choices for displays and batteries were reasearched and discussed. Overall display type is TBD, but we did order a couple large seven-segment displays to test and see how much power they draw. For batteries, we considered options of LiPo battery specifically RC car batteries and Milwaukee M18 batteries. The display will need at least 8 AH, and to make an accurate decision, we need to do a rough power budget based on our display type. 

## Plan Update
- ____ will research RF communication and different approaches for board-HUB connections.
- Everyone will finalize components for their respective approaches.
- ____ will compile a list of all finalized components to begin ordering for a prototype.
- Mitchell & ____ will work to incorporate all research material into a single technology report document. A draft will be done by the next status meeting (9/26).
 - Tyler and ? will meet and try out the display types and see if they meet the power requirments and the requirements of the customer. This testing will be done using equipment in the science building.  

## Conclusion
As the technology and concept report is due next week, the key priority is to complete research in the communication field and fill in the document, resulting in a list of specific parts we'll be using.