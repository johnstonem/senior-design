# Weekly Status Report
Course: CPE 4902 002 - CE Senior Design

Team: Charles Schaefer, John Paul Bunn, Mitchell Johnstone, Ryan Beatty, Tyler Togstad

Date: 04 April 2024

Term: Spring

Week: 10

## Table of contents
- [Executive Summary](#executive-summary)
- [Status Update](#status-update)
- [Task breakdown](#this-weeks-task-breakdown)
- [Weekly Project Effort](#weekly-project-effort)
- [Discussion](#discussion)
- [Plan Update](#plan-update)
- [Conclusion](#conclusion)

## Executive Summary
The team advanced with 3D modeling, assembly instructions, display board assembly, and simulation integration.

## Status Update
- JP add a SPIFFs partition to the ESP32 simulation
- Mitchell and Charles finished version 1 3D model of the case
- Tyler created assembly instructions (https://gitlab.com/johnstonem/senior-design/-/tree/main/Hardware/Senior_Design_Harness_V1?ref_type=heads)
- Ryan and Tyler assembled more display boards
- Tyler planned for 2 final builds and ordered parts
- Charles began work on a 3D case for QC trigger

### This week's Task breakdown
|Task|John Paul|Charles|Ryan|Tyler|Mitchell|
|--|--|--|--|--|--|
|3D Modelling||2 hrs|||6 hrs|
|WOKWI SPIFFs|6 hrs|
|Assembly Instructions||||3 hrs|
|Display Assembly|||3 hrs|3 hrs|
|HLA Planning||||2 hrs|

### Weekly Project Effort
|Week       |John Paul |Charles |Ryan   |Tyler  |Mitchell |
|--         |--     |--     |--     |--     |--     |
|W1         |2 hrs  |2 hrs  |2 hrs  |8 hrs  |3 hrs  |
|W2         |2 hrs  |4 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W3         |1 hrs  |5 hrs  |3 hrs  |5 hrs  |5 hrs  |
|W4         |3 hrs  |3 hrs  |2 hrs  |6 hrs  |3 hrs  |
|W5         |2 hrs  |1 hr   |       |1 hr   |2 hr   |
|W6         |3 hrs  |3 hrs  |4 hrs  |5 hrs  |3 hrs  |
|W7         |2 hrs  |5 hrs  |4 hrs  |6 hrs  |3 hrs  |
|W8         |2 hrs  |2 hrs  |6 hrs  |4 hrs  |4 hrs  |
|W9         |4 hrs  |5 hrs  |11 hrs |7 hrs  |3 hrs  |
|W10        |3 hrs  |5 hrs  |2 hrs  |3 hrs  |4 hrs  |
|W11        |6 hrs  |4 hrs  |7 hrs  |5 hrs  |5 hrs  |
|W12        |3 hrs  |2 hrs  |3 hrs  | 3 hrs |2 hrs  |
|W13        |<td>Thanksgiving Break</td>
|W14        |5 hrs  |5 hrs  |       |5 hrs  |5 hrs  |
|W15        |3 hrs  |3 hrs  |3 hrs  |3 hors |3 hrs  |
|W1         |4 hrs  |4 hrs  |4 hrs  |8 hrs  |3 hrs  |
|W2         |6 hrs  |4 hrs  |5 hrs  |6 hrs  |6 hrs  |
|W3         |3 hrs  |2 hrs  |5 hrs  |6 hrs  |7 hrs  |
|W4         |1 hr   |15 hrs |6 hrs  |20 hrs |6 hrs  |
|W5         |Sick   |3 hrs  |4 hrs  |4 hrs  |8 hrs  |  
|W6         |Sick   |4 hrs  |       |4 hrs  |4 hrs  |  
|W7         |4 hrs  |8 hrs  |8 hrs  |13 hrs |6 hrs  |
|W8         |6 hrs  |3 hrs  |2 hrs  |10 hrs |2 hrs  |
|W9         |5 hrs  |5 hrs  |6 hrs  |6 hrs  |4 hrs  |
|W10        |6 hrs  |2 hrs  |3 hrs  |8 hrs  |6 hrs  |
|**Total:** |71 hrs |92 hrs |97 hrs |135 hrs|89 hrs |

## Discussion
2 more display boards were constructed by Tyler and Ryan.
The original board circuitry was designed for 12V, so by switching to 5V we had to recalculate the appropriate resistors to use with the LEDs. This brought our resistors down to 220 Ohm instead of the 330 Ohm. While this will increase current draw, our last test found the battery would likely withstand the extra Watt hours. Ideally, the circuit should use 100 Ohm resistors, and they've been ordered in our most recent order to experiment with.

Charles began a 3D model for the enclosure, which Mitchell then picked up and worked on more. It was a first pass, so only 1 part was printed, to then get more opinions and refinements, tasks which include:
- introducing holes for the buttons
- making slots for charging ports and antenna
- increasing the gap on clips for holding the PCB
- separate the light isolators and the enclosure to make printing extra parts easier
- add room and holder for acrylic 

Tyler spent time writing up official assembly instructions. A clip of the instructions is below:
![alt text](image.png)

Tyler also did planning for the two final scoreboard builds. This was done in tandem with the assembly instructions. The final parts order, parts order 12 was placed on 4/2. The order contains all the parts we need to complete two complete HLA's, and provides the PETG filament to create the cases. 

Additionally, JP worked more on getting the simulator working. There was an issue with creating a partition for the filesystem (SPIFFs), but that was resolved. Now, there's a network error, not connecting from the browser to the simulated network. More work needs to be done to connect to the simulation.

## Plan Update
- Make Case updates - Mitchell & Ryan
- Make QC trigger 3D model - Charles
- Get Network connection to WOKWI - JP
- Print 3D cases / models - Tyler
- Work on completing builds - Tyler & Charles

## Conclusion
Next steps include refining case designs, finalizing 3D models, resolving network connectivity issues, and proceeding with 3D printing.