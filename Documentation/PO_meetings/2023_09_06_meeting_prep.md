# First Meeting with PO

## Details

Time: Wednesday, September 6, 9-9:50 AM

Location: Wednesday, September 6, 9-9:50 AM on the MSOE campus.

Attendees: Dr. Durant, Mr. Matousek, 2 senior design teams.

Our Members: Tyler, Ryan, Charles, JP (Mitchell absent)

Topics to discuss:
- requirements from players and coaches
- details on local electronic scoreboard

## Questions

Scoreboard
- How big would you like the scoreboard to be?
- Tournament scoreboard or Match scoreboard?
- What problems do you have with your existing scoreboard?
- What type of power will be powering the scoreboard? 
- Do you intend it to be mobile?
- What environment will the scoreboard be in?

Remotes
- What would you like the remote to look like? Would you like a sleeve to go over the racket?
- How many remotes would you need, at a minimum?
- Do you want them to be rechargable?
- What did you have in mind for the remotes beyond this?

App
- What features would you be interested in having in the app?

Communication
- When would you like to meet? do you want to meet regularly?


## Notes from meeting
- Analog scoring devices that hang on the side of the court
- Numbered 1-9
- A set is 6 games
- The players are responsible for recording the game
- Best of 3 sets typically
- Tennis Reporting Software that has now become the required thing the WIAA (interscholastic mandated app)
    - The home team is responsible for this
    - An email is sent to an opposing team coach to verify the results of the match
    - Right now they rely on the players to report their scores at the end of each match
    - They have to rely on one of the kids to act as a manager or scorekeeper 
    - They enter players info into the WIAA
    - When it's reported it's summarized in the app
    - As a coach, he can go in and check on a player's overall record, or team's record, etc
        - Jim coaches at Brown Deer High, and he doesn't usually get an opportunity to see/scout the opponent except through this app
    - 7 flights
        - 3 singles
        - 4 doubles
- For some time, thinking about an LED device that can be augmented by the player and keep track of the score
- Scoring device in player now have a BLACK and RED variants
    - Wants 2 different colors, specific color doesn't matter
    - Visible in DAYLIGHT
    - Visible by players is very high priority
    - Score is verifiable
- 3 or 4 times already Jim's team has lost the score due to memory

### Visuals
- Reinventing the visuals with display
    - "Johnny Hurr" is the MSOE grad that suggested this project to Jim
- These flaps are used by every school, so it's amazing that Tennis hasn't gone beyond these flap devices
- As match results are completed, it would be logged

### App
- The WIAA program is being adopted by many states now
    - Distinct one for all 50 states
- 5 or 6 states using this app specifically for player's history
- The app has helped with many problems over time
- It'd be nice to collect the score from the device itself and sends it to the WIAA applicaiton
    - It'd be nice to have it go directly to the app to be reported, but this might not be possible
    - The WIAA app doesn't catch when people simply don't report these scores
- Jim would like a way to see what is present on multiple scoreboards

### Tennis mechanics
- 6-ALL is a tiebreaker match
- Best of 3 tie-break sets
    - If they split sets (player wins the first set, loses the second set, the 3rd set is a match-point tiebreaker)
        - First one to 10 wins by 2
    - If the players have a hard time remembering what the score is in that tiebreak
- There are no more than 7 matches in a given game
- In college tennis 6 flights of singles and 6 flights of doubles (12)
    - At most there are 6 matches going on at a time

### Power requirements
- They are in the middle of a field
- They have some wall-power, but it's not ideal to use
- Battery-powered is preferred
- Jim thought about solar power
    - Not playing tennis when weather concerns are present
- The number on the flap closest to the player is meant to represent their score

### Mobility
- Jim wants to take the device to other schools
    - He wants other schools to see the practicality of the device

### Dimensionality
- The wind has previously impacted the card-flaps systems
- He wants the scoreboard to be a similar size to the flaps 
- Intended to be 2 units, but can be one multisided setting

### Remotes
- Remote that doesn't have to be worn by the player
- Giving a remote to the player to collect afterwards
    - A player usually doesn't even want to think about reporting their score after they've lost, for example

### Interactability
- As a chair-umpire, Jim is responsible for keeping track of the individual game score
- He enjoys using his phone for amassing information
