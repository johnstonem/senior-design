## Technology Report 
- For our 3 alternatives, we can experimnet on derivaties of the existing project.
- Review competing products.
- We might be looking at 2,3,4 display alternatives, processor alternatives, is there an easy path to wifi or are we talking about something integrated, etc.
- Example: we could design a block diagram with the hub to be primarily software or primarily hardware as 2 different parts/alternatives.
- Depending on what we're finding, we could ask, are we designing a PCB to keep our costs down? Could we use an existing board that can be miniaturized as necessary?
- We'll want to have multiple diagrams showing alternative implementations.

## Detailed Proposal Plan Draft Review
- We'll likely do 1-2 revisions of the proposal.
- In the development plan, it is okay to have research as part of the proposal.
    - We'll want to have research during weeks 3,4
    - If we know all the major pieces by the time of the final decision, then we can remove the decision/research process.
- We'll want to research how many high schools have tennis teams, typical player sizes, etc to determine the "market size." Additionally, we should research how wide the amateur tennis industry is, who may be interested in this product.
- Say a little more about competing products; specifically, research other solutions, and highlight what needs they don't meet.
- Towards the end of the project description, we should mention the app that the PO uses for publishing scores and how our solution will streamline the use of this app.
    - This is included in stretch goals.
- Dr. Durant likes the writing style of the Project Description.
    - Charles is now the official writer for all documention /s.
- We know that between now and week 5 of Spring is our implementation phase.
    - He wants to see the week-by-week of the implementation plan.
    - We could use both bullet points and descriptions of approaches.
- This proposal is in a good place for where we are in the semester.
- Durant wants the changes and updates back "soon". The sooner the better. Absolutely by thursday/friday.

## Research help
- Durant has PCB and power design suggestions on the wiki.
- He encourages us to look at other senior design projects and get inspiration and information based on those projects.
    - Sometimes those teams have information on work we need to ge tdone.
- For RF, students were usually looking for modules that could attach to the processor they had
