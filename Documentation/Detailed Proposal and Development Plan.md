# Detailed Proposal and Development Plan
## Automated Tennis Scoring Project
Senior Design 2023

Members: Charles Schaefer, Tyler Togstad, Ryan Beatty, John Paul Bunn, and Mitchell Johnstone

Advisor: Dr. Durant

Date Submitted: September 11th, 2023.

## Table of Contents
- [Project Description](#project-description)
- [Project Technologies and Support Facilities](#project-technologies-and-support-facilities)
    - [Anticipated Required Technologies](#anticipated-required-technologies)
    - [Support](#support)
- [Development Plan](#development-plan)

## Project Description 
Our project is designed to solve a few issues managing tennis and scoring high school tennis games. Our client, a tennis coach for Brown Deer High School, has advised us that he continually encounters the same issues each season:

1. The first issue is he has is the human error involved when athletes report the scores for their games. 
2. The second issue is he encounters is that he cannot see all the game scores at one time. 
    
The goal of our project is to attack these issues first and then reach for stretch goals/wishlist items if time allows. 

Currently, there is a website that keeps track of tennis scores after input from the coach, manually. The purpose of the website is to keep track of scores and data for multiple or all student athletes in tennis. It may be a stretch goal to interact with this website; however, it is not the main intention, as of now, to interact with this website by sending data manipulating data in any way.

Depending on the type of solution pursued, the idea could be quite valuable for high school tennis coaches. The idea allows for coaches to have consistent, accurate score reports and be able to view multiple game scores from a single point.

Similar products to ours would include any scoring system for tennis. Most current scoring systems for tennis are analog. The current system used at Brown Deer High School is multiple cards with numbers on each for current scores. The athletes are required to flip the cards on each win. The coach is also required to walk around and pay attention to most games and scores at the same time. In terms of market size of our product, there are about 20,000 high school tennis programs in the United States. This statistic comes from tennismediagroup.com and seems reasonable considering there is about 42,000 total high schools in the Unites States. As our product would be useful to individual high school teams, this is a decent market size to target.

The objective of our project is to design multiple small, user-friendly scoreboards for each court with a main HUB or application for viewing each score. This splits our project into two components. The first component would be the individual scoreboards for each court. The second component is a main HUB or application for viewing each court score. Each component directly addresses an issue from our client. The scoreboards will help ensure accurate scoring from individuals and the main HUB will act as a view for all current court scores. 
    
Our solution will need to be moderately simplistic because it is designed to be simple and easily controlled. Our intention is to create something that is easy to use. It will need to be simple because, to our understanding, there may not be support for the product after it is built and used because it is a senior design project. Having a simple design should also help control the error in score reporting and generally make it easier to view all the games at one time from the coach's perspective.

Our project suits our team members because it contains an adequate amount of electrical and software work. All students in the team have a computer engineering background. The content of our project may contain some mechanical engineering design, but as much to the extend that we would need someone with a mechanical background. Our initial scope of the project is smaller but there are stretch goals in the event that we have enough time to invest into the project. The project will serve as a full product including the designing, prototyping, building, and debugging phases which will benefit everyone in our project well.

## Project Technologies and Support Facilities 

### Anticipated Required Technologies

|Type|Details|Familiarity|Level of Assistance|
|--|--|--|--|
|Hardware|Microcontrollers|Very good|Some|
|Hardware|Lithium polymer battery|None|High|
|Hardware|Battery monitoring system|None|Some|
|Hardware|LED Display|Some|None|
|Hardware|RF interface chip|None|High|
|Hardware|Buttons|Very good|None|
|Software|Embedded development platform|High|High|
|Software|App development platform|Some|High|

### Support 

|Facility|Support required|
|--|--|
|MSOE EECS|lab equipment, PC workstation, makespace access|
|Other|equipment fabrication (e.g., machine shop, 3D printing), testing, etc|

## Development Plan

### Overall
The first main goal is design of the digital scorekeeper. This starts with rough sketches and more importantly getting the dimensions we want for the device. We need to design the scorecard around the 7 segment display we choose, so deciding on that early is necisary. (maybe purchasing a couple to test things like ease of use and brightness) We then need to choose how much additional information we want to display and how we will display that information (Leds, other segment displays, or other lights). We also need to be aware of the power draw of each device as we will have limited available and will need at least 2-3 hours of uptime. The next step in the design process is the user interface (buttons, switches, rotary encoders). Once these are all decided we can finalize the design/sketch of the scorekeeper

After the external IO is complete, we need to decide on internals. Our first hurtle is choosing how the HUB is going to communicate with the 7 scorecards. The current two options is 7 seperate bands that the HUB rotates through manually or automatically. Or have a single band and each HUB has a key assocated with it and the HUB recieves information from all scorecards simaltanuasly. Other constraints that should be tested is how often will we ping the boards, or how often will the HUB be updated. 

We need to decide on power. The coach has asked currently for a recharable battery, but we should also consider alkalines, or solar. We need to verify we can run the scorecard for at most 8 hours for some long tournaments. 

We then need to start design of the HUB. Currently ideas are a similar device to the scorekeeper that displays information from the 7 scorekeepers. Another idea was a usb dongle that would recieve the information, and displays the information through a app on a laptop. A lot of design decisions are yet to be made for the HUB. 

After the design process is complete, parts research is the next major step. Researching power consumption, interfacing, wireless technology, etc... We will then buy parts and test the independent of eachother to make sure they meet our requirements. Rig up some testing enviroments and maybe even start scratch coding to test our parts. 

After we confirmed all the parts we will be using design of the printed circuit board can begin. We can order one and get the prototype built. This is where any last part changes or design changes will happen. Once the prototype is built the bulk of our coding can be implemented. 

Once we have our prototype working properly then we can order enough parts to build the HUB and 7 scorekeepers. 

### Shortened version

Main goals
- A scorekeeper that displays current score and can be incremented easily by a user
- A HUB that has the ability to display the score from 7 scorekeepers.
Stretch goals
- App for phones and or laptops
- Storage of the game scores that is easily accesable
- A way to input player names
- A way to input the scores directly into the tennis app

### Weekly Breakdown:

__FALL__

|Week(s)|Objective|
|--|--|
|4-5|Create the Technology Concept Investigation report. Explore different options for materials and competiting products. Prototyping should be done if possible.|
|6-8|Create the systems specification report, order all components. Any free time should be dedicated towards making UI design or started on any basic coding/constrution of full prototype.|
|9|Create / practice presentations. Create/submit individual Elevator Pitches.|
|10|Deliver Presentation. Get feedback of idea from peers / viewers, finalize outstanding avenues.|
|11|Take inventory of what we have, make a plan for 15 weeks of development.|
|12-15|At this point, all parts should be ordered and we can start incremental development. By Winter Break, a basic prototyped scoreboard should be made to test our concept.|

__SPRING__

|Week(s)|Objective|
|--|--|
|1-4|Create the hub! With the prototyped scoreboard, we should be able to communicate between it and however the info is displayed (i.e. laptop, app, etc). Again, prototyping.|
|5|Give our Team Progress update presentations. Order PCB boards for the HUB / scoreboard.|
|6-10|Create final Embedded boards, one for each component. Housings made if neccessary, but hopefully at least working minimum-viable products. |
|11-14| If we've fallen behind in development, this period is project catchup. Otherwise, we should pursue our stretch goal. As well, a poster must be designed and presentation updated. |
|15|Team final presentations, clean documentation for final evaluation.|
|16|Take part in the senior design show.|