# Research for the HUB

## Content

### Power - JP

### Storage - Mitchell
While the individual scoreboard will have some memory in them to keep track of the individual scores, the HUB will have to have a much larger storage capacity. The HUB collects the information about scoring from the scoreboards, providing a way to access the current state of all courts in a centralized location.

This means the HUB has to store the following in memory:
- It's own program
- Current individual court scores
- History of past games

While the program will take a bit of memory, that'll likely compile and fit within the microcontroller easily. The history of past games will take a sizeable chunk of what's left. As a baseline, [this file](example_data.csv) is an example of some potential scoring that could happen in a match. While the file only takes up 477 bytes in memory, it only records for 1 fairly straightforward set score on a single court. 
Ideally, our product would work across at least 7 different courts, and have multiple sets each, maybe each multiple full matches. 

Worst case scenario, let's say we have 7 full courts. If each match lasts approximately 90 minutes, then in an 8 hour tournament we'd have about 5.33 matches each, and each match would have about 8 sets. 

$7 courts * 5.33 matches * 8 sets * 477 bytes = 142 kB$

#### Internal Option: Store In memory
From an initial pass, this likely could be stored internally. From the microcontrollers that we've worked with in the past (ESP32, STM32), they have enough flash memory to likely be able to store them in files. As an example, the ESP32-WROOM [Datasheet](https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32e_esp32-wroom-32ue_datasheet_en.pdf) describes it having 4, 8, or 16 MB of SPI flash.
The internal filesystem is supported through libraries such as [Arduino SPIFFS](https://esp32tutorials.com/esp32-spiffs-esp-idf/). However, the internal flash memory wears out after too many write cycles to it.

Pros:
- Easy to write/read the files
- Documented Examples
- No additional cost

Cons:
- Limited number of writes to flash memory.
    - Expected Endurance: [100,000 cycles](https://ww1.microchip.com/downloads/aemDocuments/documents/OTH/ProductDocuments/DataSheets/01357A.pdf)

If some external memory was added, then the product could be more reliable, but it would add a bit to the cost. Some FRAM chips have a much higher tollerance (trillions of write/read cycles). Chips such as [FM25L16B-GTR](https://www.digikey.com/en/products/detail/cypress-semiconductor-corp/FM25L16B-GTR/428-3578-1-ND/5977807) would add about 16 kbits to the system, but would cost ~$1.50. Sadly, this is not enough space, nor cost effective.

Helpful resource for describing different [external memory storage devices](https://www.digikey.com/en/articles/the-fundamentals-of-embedded-memory)


#### Connected Memory
One other option is to use [SD cards](https://www.digikey.com/en/products/filter/memory-connectors/pc-card-sockets/414?s=N4IgTCBcDaILYEsDGAnA9gZQCIAICyApnGigJ44DCAhigCaVoB2jBSALiQM4gC6AvkA). Writing to a USB drive seems to require a filesystem on the connected device, which an embedded system usually wouldn't need. However, arduino systems are capable of writing to SD cards using the [SD library](https://www.arduino.cc/en/Tutorial/LibraryExamples/ReadWrite). Microcontrollers like the ESP32 have [examples](https://randomnerdtutorials.com/esp32-microsd-card-arduino/) of how to work with an external SD card. This would allow the program to reliably store the data to an external location without needed to store it internally.

Pros
- Reliable. 
- Already has [Arduino Library Documentation](https://www.electronicwings.com/esp32/microsd-card-interfacing-with-esp32) for interaction.

Cons
- Would require GPIO ports for SPI communication.
- Coach would need a device for reading from the micro-SD card

#### Offloading Memory
The other simple approach to the memory storage would be to offload the memory to external devices. Our product likely would have the ability to communicate with another device, whether that be across a USB connector, a WIFI signal, or a Bluetooth signal. One example is here,[Bluetooth on ESP32 connected to smartphone](https://randomnerdtutorials.com/esp32-bluetooth-classic-arduino-ide/). Thus, the data that gets transmitted to the HUB could be relayed directly to the device, which would be in charge of storing all the information.

Pros
- No need for our product to have any memory.

Cons
- Unreliable.
    - If there's poor wifi/bluetooth connection, data may be dropped/lost.
- Would need to create app that would have that storage capability. SQL?

#### Opinion
Combing the SD card with the offloading memory over bluetooth seems like it would be a good method. The SD card would provide reliability and would be the main storage location. For continuous updates to other apps, that memory could then be read and transmitted via bluetooth to another device. Then all data would reliably be stored but also quickly communicated

### Display - Ryan
While the scoreboards are are what the players interact with the HUB not only allows communication between the different scoreboards but is what the coach will be interacting with. This means we need to be able to display all the desired information to the coach through a couple of options. 

After a long phone call I found certain things out from our sponsor. First is that the scores are entered into the tennis app almost immediatly after the game is finished, so needing to swap between different games will not be as necissary. The coach inputs the names into the tennis app well before game day, meaning we don't have to have that information inputted, just having who was home and away will be enough. While he will only need the hub to display the one game that happened on that court as typically there isn't mroe than one it would be helpful for big games days where multiple schools meet. Once the scores are reported into the tennis app there won't be much need to save that data for later use. Time stamping is very appealing, if possible a time stamp for every event/updates would be very helpful to understand how a games got the score when he checks in. Being able to tell singles and doubles court would be helpful when viewing the 7 different courts, a typcal match is 4 singles and 3 doubles. Finally it is neccissary that he can hold it in one hand so he can move around with it. 

#### HUB with onboard screen
This option would not have to conenct to any other device as all the information would be accessible from the HUB. This would make setup easy, and getting information simple. All information from the scoreboards will send information the the hub which will be able to switch between courts and get live feed from each court

Pros
- Quickly access information
- Simpler appraoch
    - Not much additional technology would be needed and opens up our options for the wireless interconnectivity between the scoreboard

Cons
- Limited information
    - Because we need to keep the size down we would only be able to display one courts scores at a time, and might be hard to easily display the timestamps
- Battery life
    - this option would consume the most power as a lot of information would have to be displayed at once

#### Application that connects to the HUB via iphone.
This option would have the HUB fit in a pocket or on a belt and would send the information from the 7 scoreboards to a phone app through most likely bluetooth. It is good to note that our sponsor used apple products (Ipad, Iphone)

Pros
- Lots of information
    - Since we would have a high quality screen we could display a lot of information even all information at once
- Sponsor's preffered approach
    - While on the phone he said he already walks around with his phone, and if this could work on his phone as well that would be nice. 

Cons
- Complicated technology
    - We would have to integrate at least 2 wireless technologies, as well as develop a application for multiple devices
- Bluetooth range
    - If the hub isn't in his pocket because of size, then the range will be a limiting factor
- Apple
    - It is known that it is hard to get anything on the applestore so this might be a hard roadblock if we go this aproach

### 8th scorebaord (NO hub)
This aproach would for go the hub entirely and instead have every scoreboard communicate with eachother so each scoreboard could act as a hub. We would build 8 so the coach can have one to see the scores.

Pros
- Less design
    - Since we wouldn't design a hub we would only have to design one fab board and in all have to work less. 
- Easy setup
    -Since there is no hub setup of the scoreboards will be easier and quicker

Cons
- Size
    - Since the coach would be walking around with a full scoreboard the size will be way bigger than what he was wanting
- Limited information
    - Since it is just a duplicated scoreboard we wouldn't be able to display additional information that the coach might want, like timestamps
- Complexity
    - This would increase the complexity of each scoreboard by a lot and may increase to a higher overall price. 