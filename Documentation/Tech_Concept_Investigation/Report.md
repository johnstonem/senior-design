# Technology and Concept Investigation Report
## Automated Tennis Scoring Project
Senior Design 2023

Members: Charles Schaefer, Tyler Togstad, Ryan Beatty, John Paul Bunn, and Mitchell Johnstone

Advisor: Dr. Durant

Date Submitted: 9/24/2023

## Table of Contents
- [Overview](#overview)
- [Competing Products](#competing-products)
    - [Analog/Physical](#analogphysical)
    - [Digital](#digital)
    - [Mobile/Phone App](#mobilephone-app)
- [Hub](#hub)
    - [UI/Display](#display)
    - [Memory](#memory-storage)
- [Score-Keeper](#scorekeeper)
- [Inter-connectivity](#interconnectivity)


## Overview
Tracking tennis scores across multiple scoreboards in a cohesive and reportable fashion provides some difficulty and inconvenience for administrators, coaches, and players. To alleviate this difficulty, we are designing an interconnected tennis scoring system to automatically track the concurrent scores across multiple courts to report information to a central hub. Then, the hub can be used to either access the scoreboard data or report the data as necessary, as well as allowing for access to the data via a phone application.

### Block Diagram
After meeting with the coach of the tennis team that'll use our product, we collectively decided on the overview of our product. Below is a block diagram of our currently planned concept. Future sections will dive into some more specifics on individual components.

![Block Diagram of the Tennis Board System](./Flow%20Diagram.png)

## Competing Products
This section discusses competing products in the market and advantages our product provides over each.

### Analog/Physical Scoreboard
<img src="https://m.media-amazon.com/images/I/61LOt9YCZnL._AC_UF1000,1000_QL80_.jpg" alt="Analog Scoreboard" width="200"/>

*Description*

An analog score-keeping method is the current method used by Brown Deer High School. This method consists of physical cards with different numbers to represent sets and games won. It is a very simple method of keeping score. 

*Pros*
- Simple
- No Technology
- Easy to Understand
- Mobility

*Cons*
- Outdated Technique
- Cannot be viewed distance, easily

*Values*

It is a simple, easy design to build and maintain; however, it does not address some of the issues our client encounters. Specifically, it does not address the issue of human error and visibility. By visibility, this indicates it is not easily viewable when someone is away from the court.

### Digital Scoreboard
<img src="https://www.vivisport.it/thumb.php?src=files/Prodotti/immagine/7585/7585-1.jpg&size=600x" alt="Digital Scoreboard" width="200"/>

*Description*

A digital display would be a small scoreboard that is quite similar to its analog counterpart. It would show the same, or similar information but in a digital form. It is quite a simple method of keeping score and provides a nice touch of technology.

*Pros*
- Simple
- Can Provide more information (Customizable)
- Better Visibility from distance
- Easy to Understand

*Cons*
- Large units may require dedicated/connected power
    - also heavier and more permanent than analog counterpart
- Small units require batteries to operate (AA or D batteries)
- More costly than physical scoreboards

*Values*

A digital scoring system consists of modern technology with a more custom interface. For example, some scoreboards show the exact scores of each set all on one display Some information is not as necessary but it can be nice to see and is generally better because it has more potential. However, the additional information causes it to usually be more costly. Simple and generic digital scoreboards can be anywhere from $50 to $100 for a small display, such as [this portable display](https://www.amazon.com/dp/B0C5RF4QPC/ref=sspa_dk_detail_3?psc=1&pd_rd_i=B0C5RF4QPC&pd_rd_w=T3HET&content-id=amzn1.sym.f734d1a2-0bf9-4a26-ad34-2e1b969a5a75&pf_rd_p=f734d1a2-0bf9-4a26-ad34-2e1b969a5a75&pf_rd_r=NJ7ZKGZCV7KTW65HEZN8&pd_rd_wg=hu3yA&pd_rd_r=201cfc6d-cb3f-4c97-9560-978ef0d157b1&s=electronics&sp_csd=d2lkZ2V0TmFtZT1zcF9kZXRhaWw), while boards that are more tennis focussed can cost around $350, such as [this tennis game board](https://www.amazon.com/Multifunctional-Professional-Scoreboard-Specialized-Scoreboards/dp/B0CHJ2J2C7/ref=sr_1_139?crid=3IS4Z6SODAZ3V&keywords=digital%2Btennis%2Bscoreboard&qid=1695963946&sprefix=digital%2Btennis%2Bscoreboard%2Caps%2C78&sr=8-139&ufe=app_do%3Aamzn1.fos.ac2169a1-b668-44b9-8bd0-5ec63b24bcb5&th=1)
There are many different choices that come into play when pursuing a digital scoreboard including the mobility, battery, and processors. Another fault this product fails to address is the ability for a user or coach to view all scores from a single point such as a central hub, since each court would have it's own scoreboard to view.

### Mobile/Phone Scoreboard App
<img src="https://yourtennisgame.com/images/003-Dashboard-G.jpg" alt="tennis app" width="200"/>

*Description*

["Your Tennis Game"](https://yourtennisgame.com/) is an app that details tennis information on mobile devices. The app is designed for phones and exists to keep track of scoring in tennis games and to communicate between coaches and their athletes. Players can set up games and keep track of game statistics form the app. When playing a game, the "Your Tennis Game" app shows sets and games won along with other additional information depending on chosen settings.

*Pros*
- Mobile
- Easy to use
- Good form of communication

*Cons*
- Not viewable to a large crowd
- Designed for individual use, not for entire tournaments
    
*Values*

Most people have a phone already, why don't they simply look at their phone for the score? This thought process requires everyone to have a phone and no physical scoreboard for the crowd. This scoring technique does not seem to be adopted by people watching games; however, this is a viable method to stat tracking. A similar example would be the baseball scoring book used to keep track of stats and other information throughout the game. This also provides for a central hub for the coach to look at multiple scores from a single point without the need for traveling to different courts. 

## HUB
### Display
While the scoreboards are are what the players interact with the HUB 
not only allows communication between the different scoreboards but is 
what the coach will be interacting with. This means we need to be able 
to display all the desired information to the coach through a couple of 
options. 

A lot of specification about the display came from a long phone call with our sponsor, Jim Matousek, who is the Brown Deer tennis team coach, on September 22nd. 
First is that the scores are entered into the tennis app almost 
immediately after the game is finished, so needing to swap between 
different games will not be as necessary. The coach inputs the names 
into the tennis app well before game day, meaning we don't have to have 
that information inputted, just having who was home and away will be 
enough. While he will only need the hub to display the one game that 
happened on that court as typically there isn't more than one it would 
be helpful for big games days where multiple schools meet. Once the 
scores are reported into the tennis app there won't be much need to 
save that data for later use. Time stamping is very appealing, if 
possible a time stamp for every event/updates would be very helpful to 
understand how a games got the score when he checks in. Being able to 
tell singles and doubles courts would be helpful when viewing the 7 
different courts, as a typical match is 4 singles and 3 doubles. Lastly, the display must be small enough and light enough so that he can hold the display in one hand and move around easily in order to visit each court while they're in progress. 

#### HUB with onboard screen
This option would not have to connect to any other device as all the 
information would be accessible from the HUB. This would make setup of the board
easy, and getting information simple. All information from the 
scoreboards would be sent to the hub, which would be able to 
switch between courts and get live feed from each court.

Pros
- Quickly access information
- Simpler approach
    - Not much additional technology would be needed and opens up our 
options for the wireless inter-connectivity between the scoreboard

Cons
- Limited information
    - Because we need to keep the size down we would only be able to 
display one courts scores at a time, and might be hard to easily 
display the timestamps
- Battery life
    - this option would consume the most power as a lot of information 
would have to be displayed at once

#### Application that connects to the HUB via iphone.
This option would have the HUB fit in a pocket or on a belt and would 
send the information from the 7 scoreboards to a phone app through most 
likely bluetooth. It is good to note that our sponsor used apple 
products (Ipad, Iphone)

Pros
- Lots of information
    - Since we would have a high quality screen we could display a lot 
of information even all information at once
- Sponsor's preferred approach
    - While on the phone he said he already walks around with his 
phone, and if this could work on his phone as well that would be 
nice. 

Cons
- Complicated technology
    - We would have to integrate at least 2 wireless technologies, as 
well as develop a application for multiple devices
- Bluetooth range
    - If the hub isn't in his pocket because of size, then the range 
will be a limiting factor
- Apple
    - It is known that it is hard to get anything on the Apple Store so 
this might be a hard roadblock if we go this approach

### 8th scoreboard (NO hub)
This approach would for go the hub entirely and instead have every 
scoreboard communicate with each other so any scoreboard could act as a 
hub. We would build 8 so the coach can have one to see the scores. 
Since each scoreboard is designed to be stationary at each court, the
coach would also likely have to be stationary when viewing the 8th 
scoreboard.

Pros
- Since we wouldn't design a hub, we would only have to design one
PCB and software for the product. 
- Easy production and replacement: replicate the same board

Cons
- More complex construction
    - PCB and software would include all components of scoreboard and hub.
- Limited information
    - Since it is just a duplicated scoreboard we wouldn't be able to 
display additional information that the coach might want, like 
timestamps
- Complexity
    - This would increase the complexity of each scoreboard by a lot 
and may increase to a higher overall price. 

### Memory Storage
While the individual scoreboard will have enough memory in them to keep 
track of the individual scores, the HUB might require a larger 
storage capacity. The HUB collects the information about scoring from 
the scoreboards, providing a way to access the current state of all 
courts in a centralized location.

This means the HUB has to store the following in memory:
- It's own program
- Current individual court scores
- (Potentially) a history of past games

While the program will take a bit of memory, that'll likely compile and 
fit within the micro controller easily. The history of past games will 
take a sizeable chunk of what's left. As a baseline, [this file](example_data.csv) 
is an example of some potential scoring that could 
happen in a match. While the file only takes up 477 bytes in memory, it 
only records for 1 fairly straightforward set score on a single court. 
Ideally, our product would work across at least 7 different courts, and 
have multiple sets each, maybe each multiple full matches. 

Worst case scenario, let's say we have 7 full courts. If each match 
lasts approximately 90 minutes, then in an 8 hour tournament we'd have 
about 5.33 matches each, and each match would have about 8 sets. 

$7 courts * 5.33 matches * 8 sets * 477 bytes = 142 kB$

This storage seems reasonable for a standard tennis tournament. 
Most microprocessors contain enough memory to support this (>2MB of Flash),
but it still worth exploring having dedicated storage.

#### Internal Option: Store In memory
From an initial pass, this likely could be stored internally. From the 
micro controllers that we've worked with in the past (ESP32, STM32), 
they have enough flash memory to likely be able to store them in files. 
As an example, the ESP32-WROOM [Data sheet](https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32e_esp32-wroom-32ue_datasheet_en.pdf) describes it having 
4, 8, or 16 MB of SPI flash.
The internal filesystem is supported through libraries such as [Arduino SPIFFS](https://esp32tutorials.com/esp32-spiffs-esp-idf/). However, the 
internal flash memory wears out after too many write cycles to it.

Pros:
- Easy to write/read the files
- Documented Examples
- No additional cost

Cons:
- Limited number of writes to flash memory.
    - Expected Endurance: [100,000 cycles](https://ww1.microchip.com/downloads/aemDocuments/documents/OTH/ProductDocuments/DataSheets/01357A.pdf). With good code, this can be workable for our product, but it's still a limitation.

If some external memory was added, then the product could be more 
reliable, but it would add a bit to the cost. Some FRAM chips have a 
much higher tolerance (trillions of write/read cycles). Chips such as 
[FM25L16B-GTR](https://www.digikey.com/en/products/detail/cypress-semiconductor-corp/FM25L16B-GTR/428-3578-1-ND/5977807) would 
add about 16 kilobits to the system, but would cost ~$1.50. Sadly, this is 
not enough space, nor cost effective.

Helpful resource for describing different [external memory storage devices](https://www.digikey.com/en/articles/the-fundamentals-of-embedded-memory)


#### Connected Memory
One other option is to use [SD cards](https://www.digikey.com/en/products/filter/memory-connectors/pc-card-sockets/414?s=N4IgTCBcDaILYEsDGAnA9gZQCIAICyApnGigJ44DCAhigCaVoB2jBSALiQM4gC6AvkA). 
Writing to a USB drive seems to require a filesystem on the connected 
device, which an embedded system usually wouldn't need. However, 
arduino systems are capable of writing to SD cards using the [SD library](https://www.arduino.cc/en/Tutorial/LibraryExamples/ReadWrite). 
Micro controllers like the ESP32 have [examples](https://randomnerdtutorials.com/esp32-microsd-card-arduino/) of how to work 
with an external SD card. This would allow the program to reliably 
store the data to an external location without needed to store it 
internally.

Pros
- Reliable. 
- Already has [Arduino Library Documentation](https://www.electronicwings.com/esp32/microsd-card-interfacing-with-esp32) for 
interaction.

Cons
- Would require GPIO ports for SPI communication.
- Coach would need a device for reading from the micro-SD card

#### Offloading Memory
The other simple approach to the memory storage would be to offload the 
memory to external devices. Our product likely would have the ability 
to communicate with another device, whether that be across a USB 
connector, a WIFI signal, or a Bluetooth signal. One example is here,
[Bluetooth on ESP32 connected to smartphone](https://randomnerdtutorials.com/esp32-bluetooth-classic-arduino-ide/). Thus, 
the data that gets transmitted to the HUB could be relayed directly to 
the device, which would be in charge of storing all the information.

Pros
- No need for our product to have any memory.

Cons
- Unreliable.
    - If there's poor wifi/bluetooth connection, data may be dropped/
lost.
- Would need to create app that would have that storage capability. SQL?

#### Opinion
When communicating with the coach, if he doesn't need the long term history
solution, the SD card can be dropped and the live statistics can be streamed 
from the input of the scoreboards to the HUB. This would greatly reduce
the need for any storage and simplify our architecture.

If the history is a viable addition, combing the SD card with the 
offloading memory over bluetooth seems like it would be a good method. 
The SD card would provide reliability and would be the main storage 
location for a history. For continuous updates to other apps, that 
memory could then be read and transmitted via bluetooth to another 
device. Then all data would reliably be stored but also quickly communicated.

## Scorekeeper
### Display
#### Off the Shelf Seven-Segment Display
*Description*

A seven-segment display is the display that would be used for the current set score only. This is the current number of games won on each side in the current set.

We purchased an off the shelf seven segment display to test. We also ordered some LEDs for testing and making our own display. Testing for the off the shelf seven segment are below.

*Testing*

Seven segment display with part number A0718RS was purchased and used for testing. The test results were as follows:

*Performance at 24V*
 - Full load amps were greater than 1 A
    - Total display power: < 24W
 - Visibility at this voltage was very high
 - Brightness was very good

*Performance at 12V*
- Power consumption was about 60 mA per segment
- Full load amps were about 500 mA
    - Total display power: 6W
- Visibility was very good
- Brightness was a little less than 24V

*Performance at 8V*
- 8V was the minimum turn on voltage for each segment
- Full load amps were about 100 mA
    - Total display power: 0.8W
- Despite this being the minimum, visibility was pretty good indoors
    - outdoor not tested yet, as we don't have a portable power source yet.
- Brightness was also decent, though it would be nice to have it higher

*Pros*
- Already Exist
- Not Expensive
- Documentation Exists

*Cons*
- Not Custom
- Looks Generic
- Unfavorable Power Intake (Amps and Voltage)

#### Custom LED Array
*Description*

A custom LED Array would display all information on one LED Array. This could display the current set score along with the 10x multiplier and the overall sets won. 

*Pros*
- All information on one LED Array
- Customize-ability and control over how it looks
    - Make the display fit our design, not the other way around.
- Controllable Power Intake

*Cons*
- Will take longer to develop
- Expensive
- Extreme power consumption (>2 Amps)

### Battery
#### Milwaukee M18 Battery
*Description*

[This battery](https://www.homedepot.com/p/Milwaukee-M18-18-Volt-Lithium-Ion-High-Output-XC-8-0-Ah-Battery-2-Pack-48-11-1880-48-11-1880/316139365) is for operating the scoreboard. It is a standard 18V, 8Ah Milwaukee Tool Electric Drill battery.

*Pros*
- Easy to Replace
- Easy to use
- Rechargeable
- Reliable
- Extremely high power output (>8AH)
- Has temperature monitoring system (TMS) built in to the battery
- Has battery monitoring system (BMS) [built in](https://www.batteryequivalents.com/milwaukee-m18-batteries.html#:~:text=Milwaukee%20M18%20batteries%20differ%20in,and%20protects%20it%20if%20required.)
- Battery plugs directly into device from the outside

*Cons*
- Expensive ($100-$200)
    - Would need to buy [charger](https://www.amazon.com/dp/B078X9YD57/ref=sspa_dk_detail_0?psc=1&pd_rd_i=B078X9YD57&pd_rd_w=11gkG&content-id=amzn1.sym.386c274b-4bfe-4421-9052-a1a56db557ab&pf_rd_p=386c274b-4bfe-4421-9052-a1a56db557ab&pf_rd_r=3MC2Q1A4JAYTV78SACHN&pd_rd_wg=N62CU&pd_rd_r=2d1bcfee-1c2a-44ec-8522-5cd8a1b8324d&s=electronics&sp_csd=d2lkZ2V0TmFtZT1zcF9kZXRhaWxfdGhlbWF0aWM) ($30)
- Slightly Bulky (3.5" x 3.5" x 5.5")
- Questionable Legality in a finished product
- Removes waterproof capabilities with battery contacts outside of the case

#### Drone Battery

*Description*
- This battery is used to power drones. It's typically in the form of LiPo cells, similar in style to how the milwaukee M18 battery works. 

*Pros*
- Readily available from multiple vendors
- Simple connection types
- Small and compact
- Chargers already exist

*Cons*
- Depending on the vendor, battery may or may not have BMS or TMS
- Most options are typically around 2AH, so some further research would need to be done to get a larger capacity battery
- Charging would need to be done directly on the battery with a separate device, so our case would need have a battery cavity

### Processor
#### ESP32 Microprocessor
*Description*

This is the processor that will be used to send information about the scorekeeper. The microprocessor will most likely be used to change information on the displays as well. (May not be required)

*Pros*
- All members are learning about the processor currently
    - Used in CPE4610 - Embedded Systems Fabrication
- Easy to use
    - Documentation Exists
    - Uses Arduino IDE

*Cons*
- Cheap
- Limited I/O

## Inter Connectivity
### WIFI/Bluetooth
This would be the easiest to use, as the ESP32-WROOM modules already come built with an antenna
that is capable of both WIFI and bluetooth. Testing will have to be done to determine
the range of connection to determine if they'll be able to communicate well.

*Pros*
- Free (already with microprocessor)
- Easy to configure
    - Plenty of Arduino library documentation for these connections

*Cons*
- Might have a smaller range (50-200 meters)

### Radio Communication (RF)
A lot of long range embedded systems use RF communication, such as RC cars and planes.
A similar idea could be used for our board, using radio antennas to send signals to each other.

*Pros*
- Longer range (1000 meters)
- Proven technology

*Cons*
- More expensive ($4 modules)

## Selections
|Component|Reasoning|
|--|--|
|ESP32-WROOM|This microprocessor will have enough GPIO pins for our needs. It is a familiar technology, as we all will be using one in our embedded systems fabrication class for a PCB already. It also includes internal Flash memory for history storage and bluetooth and WIFI capabilities.|
|Custom LED Display|Most other large 7-segment displays are going to be costly and might not be efficient enough for our solution. Designing it ourselves, from selecting the LEDs to use to the layout of the PCB board, will allow us to construct an efficient board.|
|SD Card Memory (if needed)|The SD card will provide long-term storage that is portable, infinitely writeable, and can be transferred from the board to other devices easily.|

Other notes:
- If the WiFi/Bluetooth strength has enough range, that would be ideal. Otherwise, RF will be a good fallback.
- The Milwaukee M18 battery has to be researched to see if we can legally use it. 

## Conclusion
Through the many explored (and inter-changeable) alternatives, there are many pros and cons that come with increasing the complexity and expansive capabilities of the scoreboards. For example, our type of battery will heavily dictate what the scoreboard are capable of as well as their usefulness. However, each technology alternative explores the foundational goal of successfully alleviating the stresses that the project was founded for: better displaying the scores, making the data easily accessible across multiple courts, and centralizing that data for easy access and use.